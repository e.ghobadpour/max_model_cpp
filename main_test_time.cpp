//
//  main.cpp
//  FCC_Max
//
//  Created by ghobadel on 20/06/2021.
#include <iostream> //For input/output
#include <fstream> //For streaming data to the output file
#include <sstream>
#include <iomanip>
#include <string> //For strings
#include <cmath> //For math calculation
#include <vector>
#include <set>
//#include "simulation_functions.hpp"
#include <ctime>
#include <cstdlib>
#include <random>
#include <stdlib.h>
#include <boost/filesystem.hpp>
#include "lattice_functions.hpp"
#include "model_functions.hpp"
#include "Diffusion_functions.hpp"
#include "TwoMon_functions.hpp"
#include "OneMonMove_functions.hpp"
#include "output_functions.hpp"
#include "parameters.hpp"
#include "cxxopts.hpp"
#include "arg_parse_functions.hpp"
using namespace std;


int main(int argc, char *argv[])
{
    //    for (int i=0; i<argc; i++) {
    //        cout<<sizeof(argv[i])/sizeof(char)<<"**";
    //    }
    //    cout<<"\nargc="+to_string(argc)<<endl<<"finished\n";
    
    General_Parameters param = argument_parser(argc,argv);
    
    if (param.resume) {
        //        for(int iargv=0;iargv<argc-1;iargv++)
        //            {
        //                printf("%s",argv[iargv]);   //remove * from here
        //            }
        param = import_params_for_resume(param);
        param.resume = true;
    }
    //    if (param.resume) {
    //        param = import_params_for_resume(param);
    //    } else {
    //        write_initial_parameters(param,filePath);
    //    }
    
    cout<<"Lx = "<<param.Lx<<endl;
    cout<<"timedynamic = "<<param.MC_DYN<<endl;
    //    exit(1);
    ofstream out;
    string outputfile, type;
    double time_phase_grow;
    double time_phase_equib;
    double time_phase_dyn=0.0;
    double time_phase_eq_2;
    double dt;
    int number_occupied_cells;
    float lattice_density_sample;
    float lattice_chain_length_sample;
    float cell_occupation_number;
    int number_of_tree_nodes;
    int ntime;
    int ntime_eq;
    
    vector<vector<int> > FCC_coordinates = initiate_FCC_coordinates(param.DIMENSION,
                                                                    param.N_MESH_POINTS,
                                                                    param.LATTICE_CONSTANT,
                                                                    param.Lx,
                                                                    param.Ly,
                                                                    param.Lz);
    
    if (param.LATTICE_CONSTANT!=2 )
    {
        
        string ErrorMessage="Lattice constant is not equal to 2.";
        throw runtime_error(ErrorMessage);
        
    }
    
    vector<vector<int> > nearest_neighbor_primitive = define_nearest_neighbors_vectors(param.DIMENSION,
                                                                                       param.N_NEIGHBORS);
    
    vector<vector<int> > neighbors_mesh_points = list_nearest_neighbors_each_lattice_point(param.boundary,
                                                                                           param.DIMENSION,
                                                                                           param.N_MESH_POINTS,
                                                                                           param.N_NEIGHBORS,
                                                                                           param.LATTICE_CONSTANT,
                                                                                           param.Lx,
                                                                                           param.Ly,
                                                                                           param.Lz,
                                                                                           FCC_coordinates,
                                                                                           nearest_neighbor_primitive);
    
    vector<vector<int> > bonds_backward = list_backward_bonds(param.N_NEIGHBORS,
                                                              nearest_neighbor_primitive);
    
    vector<vector<int> > bonds_forward = list_forward_bonds(param.N_NEIGHBORS,
                                                            nearest_neighbor_primitive);
    
    
    vector <vector<vector<int>> > possible_moves = reptation_hairpin_moves(param.N_NEIGHBORS,
                                                                           bonds_forward,
                                                                           bonds_backward);
    
    vector<int> opposite_primitive_vectors = list_paired_primitive_vectors(param.N_NEIGHBORS+1,
                                                                           nearest_neighbor_primitive );
    vector <vector<vector<int>> > possible_bending_moves = bending_moves (param.N_NEIGHBORS,
                                                                          bonds_forward,
                                                                          bonds_backward,
                                                                          opposite_primitive_vectors);
    
    
    for (int isamp = 0 ; isamp < param.nsamp ; isamp++)
    {
        //        srand(time(0));  // Initialize random number generator.
        
        //        srand(param.seed_sample+isamp);  // Initialize random number generator.
        std::mt19937_64 RNG(param.seed_sample+isamp); //random number generator
        string filePath = assign_dir(param,isamp);
        string resumeFilePath = filePath + "resume.bin";
        create_path(filePath);
        if (!param.resume)
        {
            write_initial_parameters(param,filePath);
        }
        
        vector<int> cell_occupation = define_cell_occupation(param.N_MESH_POINTS);
        vector<int> cell_tree_occupation(param.N_MESH_POINTS,0); // number of tree nodes on each lattice site
        vector<int> monomer_lattice_pos(param.TOTAL_N_MON+1,0); //position each mononer on the lattice
        
        vector<int> previous_mon(param.TOTAL_N_MON+1,0);// the index of the previus monomer
        
        vector<int> next_mon(param.TOTAL_N_MON+1,0);// the index of the next monomer
        
        vector<int> monomer_bond(param.TOTAL_N_MON+1,0);// bond of the  all monomers ,
        
        
        vector <vector <int> > list_monomers_on_each_lattice_site;
        list_monomers_on_each_lattice_site.resize(param.N_MESH_POINTS); // which monomers are in each lattice site
        vector<int> where_monomer_is_on_tree (param.TOTAL_N_MON+1,-1);
        vector <int> lattice_position_tree_nodes;
        vector <vector <int> > tree_nodes_info;
        vector<int> functionality_tree_nodes;
        vector<int> list_available_index_tree_nodes;
        
        vector<int> sum_functionality_1(param.N_POLYMERS+1,1);// start from one, because very first branch make two tree nodes with functionality one
        vector<int> tot_number_of_tree_nodes(param.N_POLYMERS+1,1);
        //
        //        int sum_functionality_1 =1;
        //        int tot_number_of_tree_nodes =1;
        vector<int> monomer_belong_polymer(param.TOTAL_N_MON+1,0);// this info says each monomer blong to which polymer chain
        double energy_sum =0;
        double delta_energy  = 0;
        
        //        vector <set<int> > neighbors_connected_lattice_site;
        //        neighbors_connected_lattice_site.resize(param.N_MESH_POINTS); // neighbors of each lattice site
        
        //        vector<int> lattice_site_functionality(param.N_MESH_POINTS,0);
        
        int nt0 = 0;
        //        int random_move;
        long int accept_move_monomer_eq = 0;
        //        long int accept_move_site_eq = 0;
        long int accept_move_monomer_dy = 0,accept_move_two_mon_dy = 0;
        long int accept_move_site_dy = 0;
        
        
        vector<int> length_chain(param.N_POLYMERS+1,0);
        
        vector<int> vector_functionalities(param.N_NEIGHBORS+1,0); //record functionalities for the last frame
        
        vector<int> vector_attempt_move_tree_with_functionality(param.N_NEIGHBORS+1,0);
        vector<int> vector_accept_move_tree_with_functionality(param.N_NEIGHBORS+1,0);
        
        
        //        long int accept_move_gr = 0;
        //     initialization for dynamics phase
        //        long int counter = 0;
        long int counter_one_mon_step =0;
        long int counter_two_mon_step =0;
        long int counter_site_step =0;
//        long int time_steps_int_counter = 0;
        long int iteration;
        //        int N_sites;
        //        vector <int> first_monomer_position;
        vector <vector <int>> old_position_first_monomer(param.N_POLYMERS, vector<int> (3, 0));
        vector <vector <int>> new_position_first_monomer(param.N_POLYMERS, vector<int> (3, 0));
        vector <vector <int>> record_pbc_shift(param.N_POLYMERS, vector<int> (3, 0));
//        vector<double> time_steps = logspace(param.start_time_outpout, param.end_time_output, param.time_steps_output);
        vector<long int> time_steps_log = logarithmicIntervals(param.start_time_outpout, param.end_time_output, param.time_steps_output);
//        for (int i=0;i<time_steps_log.size();i++)
//        {
//            cout<<i<<" "<<time_steps_log[i]<<endl;
//        }
//        vector<double> time_steps_dyn = time_steps;
        
        
        int counter_step;
        
        int ipm, mj;
        //        ****************************************Growth phase************************************
        tree_nodes_info.clear();
        functionality_tree_nodes.clear();
        if (!param.resume)
        {
            
            
            for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
            {
                place_trimers(ip,
                              param.N_MESH_POINTS,
                              nt0,
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              monomer_belong_polymer,
                              length_chain,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              lattice_position_tree_nodes,
                              where_monomer_is_on_tree,
                              functionality_tree_nodes,
                              RNG
                              );
                time_phase_grow = 0.0;
                dt = 1.0 ;
                while( length_chain[ip] < param.N_MONOMERS || time_phase_grow < param.MC_GROW)
                {
                    
                    time_phase_grow += dt/nt0;
                    
                    One_Monomer_Move(param.interaction_reptation,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     nt0,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_eq,
                                     1.0,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                    
                    if (((double)rand() / (double)RAND_MAX) < 0.01)
                    {
                        
                        add_new_monomer(param.N_MONOMERS,
                                        param.N_MESH_POINTS,
                                        param.N_CELL,
                                        nt0,
                                        cell_occupation,
                                        monomer_lattice_pos,
                                        previous_mon,
                                        next_mon,
                                        monomer_bond,
                                        monomer_belong_polymer,
                                        length_chain,
                                        list_monomers_on_each_lattice_site,
                                        tree_nodes_info,
                                        where_monomer_is_on_tree);
                        
                        
                        
                    }
                }
            }
            
            //        cout<<"delta_energy_gr  "<<delta_energy <<endl;
            //        Hamiltonian(tot_number_of_tree_nodes,
            //                    sum_functionality_1,
            //                    param.N_T,
            //                    param.N_MESH_POINTS,
            //                    param.BRANCH_POTENITIAL,
            //                    param.INTERACTION_POTENITIAL,
            //                    param.TREE_LENGTH_STIFFNESS,
            //                    energy_sum,
            //                    cell_tree_occupation);
            //        double energy_begin = energy_sum;
            //        double energy_end = 0;
            //        double delta_energy_hamiltoni =0;
            //        cout<<"energy "<<energy_sum<<endl;
            //        cout<<"sum_functionality_1 "<<sum_functionality_1<<endl;
            //        cout<<"tot_num_tree_nodes "<<tot_number_of_tree_nodes<<endl;
            delta_energy =0;
            //        cout<<"delta_energy "<<delta_energy<<endl;
            //    *****************************Equilibruim phase of the simulation*************************************
            time_phase_equib = 0;
            dt= 1.0/param.TOTAL_N_MON;
            iteration = 0;
            //            for (int i=0;i<=10;i++)
            //            {
            //                cout<<monomer_belong_polymer[i]<<" ";
            //            }
            //            cout<<endl;
            
            while( time_phase_equib < param.MC_EQUIB)
            {
                iteration += 1;
                if ( iteration % 1000000000 == 0)
                {
                    cout<<"ie"<<" "<<iteration <<endl;
                }
                
                
                
                for ( int count_eq_1 = 1; count_eq_1 <= param.TOTAL_N_MON; count_eq_1++)
                {
                    //                cout<<"eq phase "<<ntime<<endl;
                    time_phase_equib += dt;
                    delta_energy = 0;
                    One_Monomer_Move(param.interaction_reptation,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     param.TOTAL_N_MON,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_eq,
                                     param.BRANCH_PROB,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                    
                    //
                    
                }
                
            }
            
            
//            param.ss.str("");
//            param.ss<<param.fileName_eq;
            write_output_xyz(filePath + param.fileName_eq,
                             param,
                             monomer_lattice_pos,
                             FCC_coordinates,
                             monomer_bond,
                             nearest_neighbor_primitive,
                             record_pbc_shift,
                             0);
            write_time_steps_opt(filePath + param.fileName_time_eq,
                                 time_steps_log,
                                 0,
                                 param.TOTAL_N_MON);
            
            
            
            //  **********************************************equlibriulm second phase**********************************************************
            ntime_eq = param.TOTAL_N_MON;
            int N_sweep = ntime_eq;
            dt = 1.0/ntime_eq;
//            vector <long int> time_steps_int_eq= MultiplyVectorByScalar(time_steps , ntime_eq);
            //            for (int i=0; i<time_steps_int_eq.size(); i++) {
            //                cout<<time_steps_int_eq[i]<<endl;
            //            }
            for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
            {
                ipm = (ip-1) * param.N_MONOMERS;
                mj = monomer_lattice_pos[ipm+1];
                old_position_first_monomer[ip-1] = FCC_coordinates[mj];
            }
            
            int counter_step_eq =0;
            time_phase_eq_2 = 0.0;
            iteration = 0;
            double init_time_phase_eq =0;
            for ( time_phase_eq_2  = init_time_phase_eq;time_phase_eq_2 < param.MC_DYN; time_phase_eq_2 +=ntime_eq*dt)
                
            {
                
                //                if ( iteration % 10000000000 == 0)
                //                {
//                                    cout<<"ie"<<" "<<iteration <<endl;
                //                }
                for ( int count_eq_1 = 1; count_eq_1 <= ntime_eq; count_eq_1++)
                {
                    
                    
                    One_Monomer_Move(param.interaction_reptation,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     param.TOTAL_N_MON,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_eq,
                                     param.BRANCH_PROB,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                }
                    
                
                    
                    for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
                    {
                        ipm = (ip-1) * param.N_MONOMERS;
                        mj = monomer_lattice_pos[ipm+1];
                        new_position_first_monomer[ip-1] = FCC_coordinates[mj];
                    }
                    
                    
                    
                    record_position_first_monomer(old_position_first_monomer,
                                                  new_position_first_monomer,
                                                  record_pbc_shift,
                                                  param.LATTICE_CONSTANT,
                                                  param.Lx,
                                                  param.Ly,
                                                  param.Lz,
                                                  param.N_POLYMERS);
                    if ( iteration == time_steps_log[counter_step_eq])
                        
                    {

                        
                        counter_step_eq += 1;
//                        time_steps_int_counter +=1;
//                        param.ss.str("");
                        //                    counter += 1;
                        //                    param.ss<<param.fileName<<setw(param.maxDigits)<<setfill<char>('0')<<counter<<param.fileType;
                        write_output_xyz(filePath + param.fileName_eq,
                                         param,
                                         monomer_lattice_pos,
                                         FCC_coordinates,
                                         monomer_bond,
                                         nearest_neighbor_primitive,
                                         record_pbc_shift,
                                         counter_step_eq);
                        
//                        param.fs.str("");
//                        param.fs<<"time_ouput_eq"<<param.fileType;
                        write_time_steps_opt(filePath + param.fileName_time_eq,
                                             time_steps_log,
                                             counter_step_eq,
                                             N_sweep);
                    }
                iteration += 1;
                }
//            }

        }
        
        //**********************************************Dynamics**********************************************************
        
        //        int ntime = param.TOTAL_N_MON;
        
        //        int tot_N_t = 0;
        //        for (int i=1; i<param.N_POLYMERS+1; i++) {
        //            tot_N_t += tot_number_of_tree_nodes[i];
        ////            cout<<"tot_N_t "<< tot_number_of_tree_nodes[i] << endl;
        //        }
        ////        ntime = tot_N_t;
        //        ntime = param.TOTAL_N_MON;
        //        cout<< "ntime "<<ntime<<" tot_N_t "<< tot_N_t<< endl;
        //        vector <long int> time_steps_int= MultiplyVectorByScalar(time_steps_dyn , ntime);
        counter_step =0;
        time_phase_dyn = 0.0;
        //        dt = 1.0/ntime;
        iteration = 0;
        if (!param.resume) {
            param.ss.str("");
            param.ss<<param.fileName;
            write_output_xyz(filePath + param.ss.str(),
                             param,
                             monomer_lattice_pos,
                             FCC_coordinates,
                             monomer_bond,
                             nearest_neighbor_primitive,
                             record_pbc_shift,
                             counter_step);
            write_time_steps_opt(filePath + param.fileName_time_dy,
                                 time_steps_log,
                                 counter_step,
                                 param.TOTAL_N_MON);
        }
        //
        
        long int num_steps_without_resume;
        
        if (param.resume_save_freq!=0)
        {
            num_steps_without_resume =param.MC_DYN/param.resume_save_freq;
        } else {
            num_steps_without_resume =param.MC_DYN + 0.5;
        }
        long int resume_save_time = num_steps_without_resume;
        
        double init_time_phase_dyn =0;
        if (!param.resume) {
            export_for_resume(resumeFilePath,
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              where_monomer_is_on_tree,
                              lattice_position_tree_nodes,
                              functionality_tree_nodes,
                              list_available_index_tree_nodes,
                              sum_functionality_1,
                              tot_number_of_tree_nodes,
                              monomer_belong_polymer,
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              old_position_first_monomer,
                              new_position_first_monomer,
                              record_pbc_shift,
                              time_phase_dyn,
                              delta_energy,
                              iteration,
                              counter_step,
                              accept_move_site_dy,
                              accept_move_two_mon_dy,
                              accept_move_monomer_dy,
                              RNG
                              );
            
            export_for_resume(resumeFilePath + "_backup",
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              where_monomer_is_on_tree,
                              lattice_position_tree_nodes,
                              functionality_tree_nodes,
                              list_available_index_tree_nodes,
                              sum_functionality_1,
                              tot_number_of_tree_nodes,
                              monomer_belong_polymer,
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              old_position_first_monomer,
                              new_position_first_monomer,
                              record_pbc_shift,
                              time_phase_dyn,
                              delta_energy,
                              iteration,
                              counter_step,
                              accept_move_site_dy,
                              accept_move_two_mon_dy,
                              accept_move_monomer_dy,
                              RNG
                              );
        } else {
            import_for_resume(resumeFilePath,
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              where_monomer_is_on_tree,
                              lattice_position_tree_nodes,
                              functionality_tree_nodes,
                              list_available_index_tree_nodes,
                              sum_functionality_1,
                              tot_number_of_tree_nodes,
                              monomer_belong_polymer,
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              old_position_first_monomer,
                              new_position_first_monomer,
                              record_pbc_shift,
                              init_time_phase_dyn,
                              delta_energy,
                              iteration,
                              counter_step,
                              accept_move_site_dy,
                              accept_move_two_mon_dy,
                              accept_move_monomer_dy,
                              RNG
                              );
            resume_save_time = init_time_phase_dyn - 1 + num_steps_without_resume;
            cout<<"resumed time_phase_dyn = "<<init_time_phase_dyn<<endl;
            cout<<"resumed resume_save_time = "<<resume_save_time<<endl;
            cout<<flush;
        }

        int tot_N_t;
        int N_sweep = 0;
        for ( time_phase_dyn =init_time_phase_dyn;time_phase_dyn < param.MC_DYN; time_phase_dyn +=1)
        {
            if (time_phase_dyn > resume_save_time) {
                //                cout<<time_phase_dyn<<endl;
                copy_over_resume_backup(resumeFilePath);
                copy_over_RNGstate_backup(resumeFilePath);
                export_for_resume(resumeFilePath,
                                  cell_occupation,
                                  cell_tree_occupation,
                                  monomer_lattice_pos,
                                  previous_mon,
                                  next_mon,
                                  monomer_bond,
                                  where_monomer_is_on_tree,
                                  lattice_position_tree_nodes,
                                  functionality_tree_nodes,
                                  list_available_index_tree_nodes,
                                  sum_functionality_1,
                                  tot_number_of_tree_nodes,
                                  monomer_belong_polymer,
                                  vector_functionalities,
                                  vector_attempt_move_tree_with_functionality,
                                  vector_accept_move_tree_with_functionality,
                                  list_monomers_on_each_lattice_site,
                                  tree_nodes_info,
                                  old_position_first_monomer,
                                  new_position_first_monomer,
                                  record_pbc_shift,
                                  time_phase_dyn,
                                  delta_energy,
                                  iteration,
                                  counter_step,
                                  accept_move_site_dy,
                                  accept_move_two_mon_dy,
                                  accept_move_monomer_dy,
                                  RNG
                                  );
                resume_save_time += num_steps_without_resume;
                
            }
            tot_N_t=0;
            for (int i=1; i<param.N_POLYMERS+1; i++) {
                tot_N_t += tot_number_of_tree_nodes[i];
            }
            if (param.move == "tree_node")
            {
                
            for ( int count_dy_1 = 1; count_dy_1 <= tot_N_t; count_dy_1++)
            {
                N_sweep = tot_N_t;
                Tree_Node_Move(param.connectivity,
                               param.interaction_diffusion,
                               param.INTERACTION_POTENITIAL,
                               param.N_MESH_POINTS,
                               param.N_CELL,
                               cell_occupation,
                               cell_tree_occupation,
                               monomer_lattice_pos,
                               previous_mon,
                               next_mon,
                               monomer_bond,
                               neighbors_mesh_points,
                               bonds_forward,
                               bonds_backward,
                               list_monomers_on_each_lattice_site,
                               tree_nodes_info,
                               lattice_position_tree_nodes,
                               where_monomer_is_on_tree,
                               functionality_tree_nodes,
                               accept_move_site_dy,
                               delta_energy,
                               vector_attempt_move_tree_with_functionality,
                               vector_accept_move_tree_with_functionality,
                               RNG);
                counter_site_step +=1;
 
            }
            }
            
            if (param.move == "one_mon")
            {
                for ( int count_dy_1 = 1; count_dy_1 <= param.TOTAL_N_MON; count_dy_1++)
                {
                    //                time_phase_dyn += dt;
                    N_sweep = param.TOTAL_N_MON;
                    delta_energy =0;
                    One_Monomer_Move(param.interaction_reptation,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     param.TOTAL_N_MON,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_dy,
                                     param.BRANCH_PROB,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                    counter_one_mon_step+=1;
                }
            }
            
            if (param.move == "mixed")
            {
                N_sweep = tot_N_t + param.TOTAL_N_MON;
                for ( int count_dy_1 = 1; count_dy_1 <= tot_N_t; count_dy_1++)
                {
                    Tree_Node_Move(param.connectivity,
                                   param.interaction_diffusion,
                                   param.INTERACTION_POTENITIAL,
                                   param.N_MESH_POINTS,
                                   param.N_CELL,
                                   cell_occupation,
                                   cell_tree_occupation,
                                   monomer_lattice_pos,
                                   previous_mon,
                                   next_mon,
                                   monomer_bond,
                                   neighbors_mesh_points,
                                   bonds_forward,
                                   bonds_backward,
                                   list_monomers_on_each_lattice_site,
                                   tree_nodes_info,
                                   lattice_position_tree_nodes,
                                   where_monomer_is_on_tree,
                                   functionality_tree_nodes,
                                   accept_move_site_dy,
                                   delta_energy,
                                   vector_attempt_move_tree_with_functionality,
                                   vector_accept_move_tree_with_functionality,
                                   RNG);
                    counter_site_step +=1;

                }
                for ( int count_dy_1 = 1; count_dy_1 <= param.TOTAL_N_MON; count_dy_1++)
                {
                    //                time_phase_dyn += dt;
                    delta_energy =0;
                    One_Monomer_Move(param.interaction_reptation,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     param.TOTAL_N_MON,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_dy,
                                     param.BRANCH_PROB,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                    counter_one_mon_step+=1;
                }
            }
            
            for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
            {
                ipm = (ip-1) * param.N_MONOMERS;
                mj = monomer_lattice_pos[ipm+1];
                new_position_first_monomer[ip-1] = FCC_coordinates[mj];
            }
            record_position_first_monomer(old_position_first_monomer,
                                          new_position_first_monomer,
                                          record_pbc_shift,
                                          param.LATTICE_CONSTANT,
                                          param.Lx,
                                          param.Ly,
                                          param.Lz,
                                          param.N_POLYMERS);
        
            if ( iteration == time_steps_log[counter_step])
                
            {
                counter_step += 1;
                param.ss.str("");
                param.ss<<param.fileName;
                write_output_xyz(filePath + param.ss.str(),
                                 param,
                                 monomer_lattice_pos,
                                 FCC_coordinates,
                                 monomer_bond,
                                 nearest_neighbor_primitive,
                                 record_pbc_shift,
                                 counter_step);
                write_time_steps_opt(filePath + param.fileName_time_dy,
                                     time_steps_log,
                                     counter_step,
                                     N_sweep);
                
            }
            iteration += 1;
        }
        Hamiltonian(tot_number_of_tree_nodes,
                    sum_functionality_1,
                    param.N_POLYMERS,
                    param.N_T,
                    param.N_MESH_POINTS,
                    param.BRANCH_POTENITIAL,
                    param.INTERACTION_POTENITIAL,
                    param.TREE_LENGTH_STIFFNESS,
                    energy_sum,
                    cell_tree_occupation);
        
        number_occupied_cells = N_occupided_cell(param.N_MESH_POINTS,
                                                 param.N_CELL,
                                                 cell_occupation);
        lattice_density_sample = lattice_density (number_occupied_cells,
                                                  param.N_MESH_POINTS);
        
        
        lattice_chain_length_sample = lattice_chain_length (number_occupied_cells,
                                                            param.N_POLYMERS);
        
        cell_occupation_number = cell_occupation_density (number_occupied_cells,
                                                          param.TOTAL_N_MON);
        
        //        number_of_tree_nodes = tree_nodes_info.size();
        int n_f;
        number_of_tree_nodes =0;
        for (int i=0; i<tree_nodes_info.size();i++)
        {
            
            n_f = functionality_tree_nodes[i];
            if (n_f > 0)
            {
                number_of_tree_nodes +=1;
                vector_functionalities[n_f] += 1;
            }
            
        }
        
        
        param.ss.str("");
        param.ss<<param.fileName;
        write_output_xyz(filePath + param.ss.str(),
                         param,
                         monomer_lattice_pos,
                         FCC_coordinates,
                         monomer_bond,
                         nearest_neighbor_primitive,
                         record_pbc_shift,
                         counter_step);
        param.fs.str("");
        
        param.fs<<param.fileName_param<<param.fileType;
        write_simulation_info(filePath + param.fs.str(),
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              number_occupied_cells,
                              lattice_density_sample,
                              cell_occupation_number,
                              lattice_chain_length_sample,
                              accept_move_monomer_dy,
                              accept_move_two_mon_dy,
                              accept_move_site_dy,
                              counter_one_mon_step,
                              counter_two_mon_step,
                              counter_site_step,
                              number_of_tree_nodes,
                              param);
        
//        param.fs.str("");
//        param.fs<<"time_ouput"<<param.fileType;
        //        write_time_steps(filePath + param.fs.str(),
        //                         time_steps_int,
        //                         param.time_steps_output,
        //                         ntime);
        
        param.fs.str("");
        param.fs<<"tree_ouput"<<param.fileType;
        write_tree_output (filePath + param.fs.str(),
                           param,
                           functionality_tree_nodes,
                           tree_nodes_info
                           );
        //        param.fs.str("");
        //        param.fs<<"mesh_geometry"<<param.fileType;
        //        write_lattice_geometry(filePath + param.fs.str(),
        //                               FCC_coordinates,
        //                               param);
        
        copy_over_resume_backup(resumeFilePath);
        copy_over_RNGstate_backup(resumeFilePath);
        export_for_resume(resumeFilePath,
                          cell_occupation,
                          cell_tree_occupation,
                          monomer_lattice_pos,
                          previous_mon,
                          next_mon,
                          monomer_bond,
                          
                          where_monomer_is_on_tree,
                          lattice_position_tree_nodes,
                          
                          functionality_tree_nodes,
                          list_available_index_tree_nodes,
                          sum_functionality_1,
                          tot_number_of_tree_nodes,
                          monomer_belong_polymer,
                          
                          vector_functionalities,
                          vector_attempt_move_tree_with_functionality,
                          vector_accept_move_tree_with_functionality,
                          list_monomers_on_each_lattice_site,
                          tree_nodes_info,
                          old_position_first_monomer,
                          new_position_first_monomer,
                          record_pbc_shift,
                          time_phase_dyn,
                          delta_energy,
                          iteration,
                          counter_step,
                          accept_move_site_dy,
                          accept_move_two_mon_dy,
                          accept_move_monomer_dy,
                          RNG
                          );
        
        
        
        //                Two_Monomer_Move (param.interaction_diffusion,
        //                                  param.BRANCH_POTENITIAL,
        //                                  param.INTERACTION_POTENITIAL,
        //                                  param.TREE_LENGTH_STIFFNESS,
        //                                  param.N_T,
        //                                  param.BRANCH_PROB,
        //                                  param.FUNCTIONALITY,
        //                                  param.N_MONOMERS,
        //                                  param.N_MESH_POINTS,
        //                                  param.N_CELL,
        //                                  param.TOTAL_N_MON,
        //                                  sum_functionality_1,
        //                                  tot_number_of_tree_nodes,
        //                                  previous_mon,
        //                                  next_mon,
        //                                  monomer_lattice_pos,
        //                                  lattice_position_tree_nodes,
        //                                  cell_occupation,
        //                                  cell_tree_occupation,
        //                                  monomer_bond,
        //                                  possible_bending_moves,
        //                                  neighbors_mesh_points,
        //                                  bonds_forward,
        //                                  bonds_backward,
        //                                  list_monomers_on_each_lattice_site,
        //                                  opposite_primitive_vectors,
        //                                  where_monomer_is_on_tree,
        //                                  tree_nodes_info,
        //                                  functionality_tree_nodes,
        //                                  list_available_index_tree_nodes,
        //                                  accept_move_two_mon_dy,
        //                                  delta_energy,
        //                                  monomer_belong_polymer,
        //                                  RNG
        //                                  );
        


        
    }
    return 0;
}

