import os
import time

os.system("tmux kill-server")
time.sleep(1)
n_nodes = 64
N_samples = 4
for i in range(n_nodes):
    command = ""
    for bound in ["pbc" ]:
        for mix in ["mixed"]:
            for branch in [3]:
                for interaction in [30.0]:
                    for chemical in [0.0]:
                        for m in [0.01,0.99,0.10,0.90]:
                            for ring in [512]:
                                seed = 1 + i*N_samples
                                command +=f"./FCC_Max -a {seed} -s {N_samples} -b {bound} -w {mix} -p 1 -m {ring} -f {branch} -u {chemical} -k {interaction} -v 0.0 -q 2.3 -x 35 -y 35 -z 35  -g 100000 -e 1000000000 -t 1000000000 -R 0 -j {m} -d 1.0; "
                    
    print(command)
    os.system('tmux new -s {} -d'.format(i)) 
    os.system("tmux send -t {} '{}' C-m".format(i, command))

