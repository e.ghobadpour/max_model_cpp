//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef arg_parse_functions_h
#define arg_parse_functions_h
#include "parameters.hpp"
#include <vector>
#include <iostream> //For input/output
using namespace std;


General_Parameters argument_parser(int argc, char** argv);
 

#endif /* arg_parse_functions_h */
