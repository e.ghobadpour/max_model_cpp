//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef TwoMon_functions_h
#define TwoMon_functions_h
#include <vector>
#include <iostream> //For input/output
#include <array>
#include <set>
#include <list>
#include <initializer_list>
#include <random>
using namespace std;


//std::tuple <int,vector<int>,vector<int>,vector<int>, vector<int>, vector<int>, vector<int>> initialisation(int nt,int np);




void  diffusion_sites ( int mesh_size,
                       int ncell,
                       int &nt0,
                       vector<int> &icl,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       vector <vector<vector<int> > > &bending_move_table,
                       vector <vector<int> > &list_nn,
                       vector< vector <int> > &forward_bonds,
                       vector< vector <int> > &backward_bonds,
                       vector <int> &opposite_primitive_vectors,
                       vector<vector<int>> &sites_info,
                       vector<int> &functionality,
                       vector<set<int>> &sites_neighbors,
                       vector<int> &paired_vectors,
                       int &acceptance_rate,
                       float branch_prob);

int Find_Paired_Monomer (int chain_size,
                         int monomer_1,
                         int random_tree_node,
                         vector<int> &previous_mon,
                         vector<int> &next_mon,
                         vector<int> &monomer_lattice_pos,
                         vector<int> &monomer_bond,
                         vector <int> &monomer_tree_position,
                         vector <vector <int> >  &tree_node_info,
                         mt19937_64 &RNG);


int Find_Bond_Direction( int bond1_1,
                        int bond1_2,
                        int bond2_1,
                        int bond2_2,
                        vector<int> &paired_vectors);


void Update_Sites_Two_Monomer_Move(int monomer_1,
                                  int monomer_2,
                                  int random_lattice_site,
                                  int new_position_random_monomers,
                                  int zero_bond,
                                  vector<int> &previous_mon,
                                  vector<int> &next_mon,
                                  vector<int> &monomer_lattice_pos,
                                  vector<int> &icl,
//                                  vector<int> &functionality,
                                  vector<vector<int>> &sites_info);


void Update_Tree_Nodes_Two_Monomer_Move(int monomer_1,
                                       int monomer_2,
                                       int random_lattice_site,
                                       int new_position_random_monomers,
                                       int zero_bond,
                                       int random_tree_node,
                                       vector<int> &sum_f_1,
                                       vector<int> &sum_N_t,
                                       vector<int> &icl,
                                       vector<int> &N_tree_node_on_lattice_sites,
                                       vector<int> &previous_mon,
                                       vector<int> &next_mon,
                                       vector<int> &monomer_lattice_pos,
                                       vector<int> &monomer_bond,
                                       vector <int>   &lattice_sites_nodes,
                                       vector <int> &monomer_tree_position,
                                       vector<int> &functionality_tree_node,
                                       vector<int> &list_available_index,
                                       vector <vector <int> >  &tree_node_info,
                                       vector<int> &monomer_belong_polymer
                                       );

bool Check_Accept_Condition_Two_Mons_Move(string interaction,
                                          double potenitial_branch,
                                          double potential_interaction,
                                          double tree_stiffness,
                                          int equib_N_tree,
                                          int max_branch,
                                          int zero_bond,
                                          int monomer_1,
                                          int monomer_2,
                                          int new_position_random_monomers,
                                          int random_lattice_site,
                                          int random_tree_node,
                                          int random_neighbor_1,
                                          int bond_direction,
                                          int monomer_first,
                                          vector<int> sum_f_1,
                                          vector<int> sum_N_t,
                                          vector<int> &icl,
                                          vector<int> &N_tree_node_on_lattice_sites,
                                          vector<int> &previous_mon,
                                          vector<int> &next_mon,
                                          vector<int> &monomer_bond,
                                          vector<int> &monomer_lattice_pos,
                                          vector<int> &paired_vectors,
                                          vector <int> &monomer_tree_position,
                                          vector <int>   &lattice_sites_nodes,
                                          vector<int> &functionality_tree_node,
                                          vector <vector <int> >  &tree_node_info,
                                          double &delta_E,
                                          vector<int> &monomer_belong_polymer,
                                          mt19937_64 &RNG
                                          );



void  Two_Monomer_Move (string interaction,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        double branch_prob,
                        int max_branch,
                        int chain_size,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_lattice_pos,
                        vector <int>   &lattice_sites_nodes,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &bending_move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector<vector<int>> &sites_info,
//                        vector<int> &functionality,
                        vector<int> &paired_vectors,
                        vector <int> &monomer_tree_position,
                        vector <vector <int> >  &tree_node_info,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG
                        );

bool Check_branch_probability_f_2(int zero_bond,
                              int random_tree_node,
                              double branch_prob,
                              vector<int> &functionality_tree_node
                              );
#endif /* TwoMon_functions_h */
