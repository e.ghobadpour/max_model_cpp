//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef model_functions_h
#define model_functions_h
#include <vector>
#include <iostream> //For input/output
#include <array>
#include <set>
#include <list>
#include <initializer_list>
#include <random>
using namespace std;


//std::tuple <int,vector<int>,vector<int>,vector<int>, vector<int>, vector<int>, vector<int>> initialisation(int nt,int np);

void  place_trimers ( int ip,
                     int mesh_size,
                     int &nt0,
                     vector<int> &icl,
                     vector<int> &N_tree_node_on_lattice_sites,
                     vector<int> &monomer_lattice_pos,
                     vector<int> &previous_mon,
                     vector<int> &next_mon,
                     vector<int> &monomer_bond,
                     vector<int> &monomer_belong_polymer,
                     vector<int> &length_chain,
                     vector<vector<int>> &sites_info,
//                     vector<int> &functionality,
                     vector <vector <int> >  &tree_node,
                     vector <int>   &lattice_sites_nodes,
                     vector <int> &monomer_tree_position,
                     vector<int> &functionality_tree_node,
                     mt19937_64 &RNG);







void  add_new_monomer(int nm,
                      int mesh_size,
                      int ncell,
                      int &nt0,
                      vector<int> &icl,
                      vector<int> &monomer_lattice_pos,
                      vector<int> &previous_mon,
                      vector<int> &next_mon,
                      vector<int> &monomer_bond,
                      vector<int> &monomer_belong_polymer,
                      vector<int> &length_chain,
                      vector<vector<int>> &sites_info,
                      vector <vector <int> >  &nodes_tree,
                      vector <int> & monomer_tree_position);
//                      vector<int> &previous_mon_prune,
//                      vector<vector<int>> &non_zero_mons_tree_node);


vector<double> Hamiltonian_function(vector<int> &total_number_tree_nodes,
                  vector<int> &total_functionality_1,
                  int number_polymers,
                    int equib_length_tree,
                    int tot_mesh_points,
                    double potenitial_branch,
                    double potential_interaction,
                    double stiffness_tree_length,
                    double &energy,
                    vector<int> &N_tree_node_on_lattice_sites);
#endif /* model_functions_h */
