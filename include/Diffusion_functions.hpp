//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef diffusion_functions_h
#define diffusion_functions_h
#include <vector>
#include <iostream> //For input/output
#include <array>
#include <set>
#include <list>
#include <initializer_list>
#include <random>
using namespace std;


//std::tuple <int,vector<int>,vector<int>,vector<int>, vector<int>, vector<int>, vector<int>> initialisation(int nt,int np);

void  Tree_Node_Move (string interaction,
                      double potential_interaction,
                      int mesh_size,
                      int ncell,
                      vector<int> &icl,
                      vector<int> &N_tree_node_on_lattice_sites,
                      vector<int> &monomer_lattice_pos,
                      vector<int> &previous_mon,
                      vector<int> &next_mon,
                      vector<int> &monomer_bond,
                      vector <vector<int> > &list_nn,
                      vector< vector <int> > &forward_bonds,
                      vector< vector <int> > &backward_bonds,
                      vector<vector<int>> &sites_info,
                      vector <vector <int> >  &nodes_tree,
                      vector <int>   &lattice_sites_nodes,
                      vector<int> &tree_node_monomer_belongs,
                      vector<int> &functionality_tree_node,
                      long int &acceptance_rate_site,
                      double &delta_E,
                      vector<long int> &vector_attempt_move_tree_with_functionality,
                      vector<long int> &vector_accept_move_tree_with_functionality,
                      mt19937_64 &RNG
                      );


bool Check_Interaction_Condition_Diffusion(string TreeConnectivity,
                                           string interaction,
                                           double potential_interaction,
                                           int ncell,
                                           int random_tree_node,
                                           int new_position_random_site,
                                           int random_lattice_site,
                                           vector<int> &icl,
                                           vector<int> &N_tree_node_on_lattice_sites,
                                           vector<int> &previous_mon,
                                           vector<int> &monomer_lattice_pos,
                                           vector <vector <int> >  &tree_node_info,
                                           double &delta_E
                                           );


bool Check_Accept_Condition_Bonds_Tree_Node(int random_tree_node,
                                            int random_direction,
                                            vector<int> &monomer_bond,
                                            vector<int> &previous_mon,
                                            vector< vector <int> > &forward_bonds,
                                            vector< vector <int> > &backward_bonds,
                                            vector <vector <int> >  &tree_node_info);




void Update_Tree_Node_Move(int random_tree_node,
                           int random_lattice_site,
                           int random_direction,
                           int new_position_random_site,
                           vector<int> &icl,
                           vector<int> &N_tree_node_on_lattice_sites,
                           vector<int> &monomer_lattice_pos,
                           vector<int> &previous_mon,
                           vector<int> &next_mon,
                           vector<int> &monomer_bond,
                           vector< vector <int> > &forward_bonds,
                           vector< vector <int> > &backward_bonds,
                           vector <int>   &lattice_sites_nodes,
                           vector<int> &tree_node_monomer_belongs,
                           vector <vector <int> >  &tree_node_info);



#endif /* diffusion_functions_h */
