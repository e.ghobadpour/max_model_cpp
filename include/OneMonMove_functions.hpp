//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef OneMonMove_functions_h
#define OneMonMove_functions_h
#include <vector>
#include <iostream> //For input/output
#include <array>
#include <set>
#include <list>
#include <initializer_list>
#include <random>
using namespace std;


//std::tuple <int,vector<int>,vector<int>,vector<int>, vector<int>, vector<int>, vector<int>> initialisation(int nt,int np);


bool Check_Interaction_Condition_Reptation_repton(string interaction,
                                           int ncell,
                                           int max_branch,
                                           int lattice_pos_random_monomer,
                                           int new_position_random_monomer,
                                           int random_monomer,
                                           vector<int> &icl,
                                           vector<int> &previous_mon,
                                           vector<int> &next_mon,
                                           vector<int> &monomer_lattice_pos,
//                                           vector<int> &functionality,
                                           vector<int> &monomer_tree_position,
                                           vector<int> &functionality_tree_node,
                                           double repton_mixing,
                                           int nt0,
                                           mt19937_64 &RNG
                                           );


bool Check_Interaction_Condition_Reptation(string interaction,
                                           int ncell,
                                           int max_branch,
                                           int lattice_pos_random_monomer,
                                           int new_position_random_monomer,
                                           int random_monomer,
                                           vector<int> &icl,
                                           vector<int> &previous_mon,
                                           vector<int> &next_mon,
                                           vector<int> &monomer_lattice_pos,
//                                           vector<int> &functionality,
                                           vector<int> &monomer_tree_position,
                                           vector<int> &functionality_tree_node
                                           );
void Update_Functionality(int new_position_random_monomer,
                          int lattice_pos_random_monomer,
                          int &bond_prev,
                          int &bond_mon,
                          int &bond_prev_new,
                          int &bond_mon_new,
                          vector<int> &functionality,
                          vector<set<int>> &sites_neighbors
                          );

void update_tree_nodes(int random_monomer,
                       int lattice_pos_random_monomer,
                       int new_position_random_monomer,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       vector<int> &N_tree_node_on_lattice_sites,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       vector<int> &monomer_tree_position,
                       vector <int>   &lattice_sites_nodes,
                       vector<vector<int>> &tree_node,
                       vector<int> &functionality_tree_node,
                       vector<int> &list_available_index,
                       vector<int> &monomer_belong_polymer);


void  One_Monomer_Move (string interaction,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_lattice_pos,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector <int> &opposite_primitive_vectors,
                        vector<vector<int>> &sites_info,
//                        vector<int> &functionality,
                        vector<int> &monomer_tree_position,
                        vector <int>   &lattice_sites_nodes,
                        vector<vector<int>> &tree_node,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        int max_branch,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG);
void  One_Monomer_Move_repton (string interaction,
                        double repton_mixing,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_lattice_pos,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector <int> &opposite_primitive_vectors,
                        vector<vector<int>> &sites_info,
//                        vector<int> &functionality,
                        vector<int> &monomer_tree_position,
                        vector <int>   &lattice_sites_nodes,
                        vector<vector<int>> &tree_node,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        int max_branch,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG);
void  One_Monomer_Move_grow (string interaction,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_lattice_pos,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector <int> &opposite_primitive_vectors,
                        vector<vector<int>> &sites_info,
//                        vector<int> &functionality,
                        vector<int> &monomer_tree_position,
                        vector <int>   &lattice_sites_nodes,
                        vector<vector<int>> &tree_node,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        double branch_prob,
                        int max_branch,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG);



bool Check_branch_probability(int random_monomer,
                              int random_neighbor,
                              int &bond1,
                              int &bond2,
                              double branch_prob,
                              vector<int> &previous_mon,
                              vector<int> &next_mon,
                              vector<int> &monomer_bond,
                              vector <int> &opposite_primitive_vectors,
                              mt19937_64 &RNG
                              );

void Update_Monomer_Move(int random_monomer,
                         int lattice_pos_random_monomer,
                         int random_neighbor,
                         int new_position_random_monomer,
                         int &bond1,
                         int &bond2,
                         vector<int> &icl,
                         vector<int> &monomer_lattice_pos,
                         vector<int> &previous_mon,
                         vector< vector <int> > &forward_bonds,
                         vector< vector <int> > &backward_bonds,
                         vector<int> &monomer_bond,
                         vector<vector<int>> &sites_info,
                         vector <int> &opposite_primitive_vectors
                         );

bool Check_Hamiltonian(string interaction,
                       double potenitial_branch,
                       double potential_interaction,
                       double tree_stiffness,
                       int equib_N_tree,
                       int &bond1,
                       int &bond2,
                       int random_monomer,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       int new_position_random_monomer,
                       int lattice_pos_random_monomer,
                       vector<int> &N_tree_node_on_lattice_sites,
                       vector<int> &previous_mon,
                       vector<int> &functionality_tree_node,
                       vector<int> &monomer_tree_position,
                       double &delta_E,
                       vector<int> &monomer_belong_polymer,
                       mt19937_64 &RNG
                       );

#endif /* OneMonMove_functions_h */
