//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef output_functions_h
#define output_functions_h
#include <vector>
#include <iostream> //For input/output
#include "parameters.hpp"
#include <random>
using namespace std;
vector<long int> logarithmicIntervals(double start, double end, int step);
vector<double> logspace(double start, double end, int step);
vector<long int> MultiplyVectorByScalar(vector<double> &time, int sweep);

void create_path(string path);


string assign_dir(General_Parameters &param,int isamp);


void    record_position_first_monomer(vector<vector<int> > &old_position,
                                      vector<vector<int> > &current_position,
                                      vector<vector<int> > &pbc_shift,
                                      int lattice_constant,
                                      int lx,
                                      int ly,
                                      int lz,
                                      int np);

void write_output_xyz (string outputfile,
                       General_Parameters &param,
                       vector<int> monomer_lattice_pos,
                       vector<vector<int> >  FCC_coordinates,
                       vector<int> monomer_bond,
                       vector<vector<int> > nearest_neighbor_primitive,
//                       vector <int> first_monomer
                       vector<vector<int> > &pbc_shift,
                       long int time);



void write_tree_output (string outputfile,
                       General_Parameters &param,
                        vector<int> functionality_tree_nodes,
                        vector <vector <int> >  tree_nodes_info
                        );


int N_occupided_cell (int tot_mesh_points,
                      int N_CELL,
                      vector<int> &icl);


float lattice_density (int N_cell_occupided,
                       int tot_mesh_points);


float lattice_chain_length (int N_cell_occupided,
                            int Np);

float cell_occupation_density (int N_cell_occupided,
                               int TOTAL_N_MON);

void write_simulation_info (string outputfile,
                            vector<int> vector_functionalities,
                            vector<int> vector_attempt_move_tree_with_functionality,
                            vector<int> vector_accept_move_tree_with_functionality,
                            int n_occupied_cells,
                            float rho_lattice,
                            float rho_cell,
                            float chain_length,
                            long int accept_move_monomer_dy,
                            long int accept_move_two_mon_dy,
                            long int accept_move_site_dy,
                            long int counter_number_one_mon_move,
                            long int counter_number_two_mons_move,
                            long int counter_site_step,
                            int number_of_tree_nodes,
                            General_Parameters &param);


//void write_time_steps (string outputfile,
//                       vector<long int> time_steps,
//                       int number_steps,
//                        int sweep)
//                       );
void write_time_steps_opt (string outputfile,
                           vector<long int> time_steps,
                           int step_number,
                           int sweep);

void write_tree_connectivity(string outputfile,
                             vector <vector <int> >  &nodes_tree);

void write_lattice_geometry(string outputfile,
                            vector<vector<int> > FCC_coordinates,
                            General_Parameters &param);

void write_initial_parameters(General_Parameters &param,
                              string seed_path);




void export_for_resume(string resumeFilePath,
                       vector<int> &icl,
                       vector<int> &Number_Nt_on_lattice_sites,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       
                       vector<int> &monomer_tree_position,
                       vector <int> &lattice_sites_nodes,
                       
                       vector<int> &functionality_tree_node,
                       vector<int> &list_available_index,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       vector<int> &monomer_belong_polymer,
                       
                       vector<int> &vector_functionalities,
                       vector<int> &vector_attempt_move_tree_with_functionality,
                       vector<int> &vector_accept_move_tree_with_functionality,
                       vector<vector<int>> &sites_info,
                       vector<vector<int>> &tree_info,
                       vector<vector<int> >  &old_position,
                       vector<vector<int> > &current_position,
                       vector<vector<int> > &pbc_shift,
                       double &time_phase_dyn,
                       double &delta_E,
                       long int &iteration,
                       int &counter_step,
                       long int &accept_tree_node_dy,
                       long int &accept_pair_move_dy,
                       long int &accept_one_mon_dy,
                       mt19937_64 &RNG                       
                       );

void import_for_resume(string resumeFilePath,
                       vector<int> &icl,
                       vector<int> &Number_Nt_on_lattice_sites,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       
                       vector<int> &monomer_tree_position,
                       vector <int> &lattice_sites_nodes,
                       
                       vector<int> &functionality_tree_node,
                       vector<int> &list_available_index,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       vector<int> &monomer_belong_polymer,
                       
                       vector<int> &vector_functionalities,
                       vector<int> &vector_attempt_move_tree_with_functionality,
                       vector<int> &vector_accept_move_tree_with_functionality,
                       vector<vector<int>> &sites_info,
                       vector<vector<int>> &tree_info,
                       vector<vector<int> > &old_position,
                       vector<vector<int> > &current_position,
                       vector<vector<int> > &pbc_shift,
                       double &time_phase_dyn,
                       double &delta_E,
                       long int &iteration,
                       int &counter_step,
                       long int &accept_tree_node_dy,
                       long int &accept_pair_move_dy,
                       long int &accept_one_mon_dy,
                       mt19937_64 &RNG
                       );


void copy_over_resume_backup(string resumeFilePath);
void copy_over_RNGstate_backup(string resumeFilePath);

General_Parameters import_params_for_resume(General_Parameters &param);
string check_if_file_exists(string filename);

#endif /* output_functions_h */
