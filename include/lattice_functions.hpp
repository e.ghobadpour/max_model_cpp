//
//  lattice_functions.hpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//

#ifndef lattice_functions_h
#define lattice_functions_h
#include <vector>
#include <iostream> //For input/output
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <fstream> // For streaming data to the output file
#include <string> // For strings
#include <random>
using namespace std;

//boost::mt19937 gen;
//
//int random_number(int min,
//                  int max) {
//    boost::uniform_int<> dist(min, max);
//    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > die(gen, dist);
//    return die();
//}



vector<vector<int> > define_nearest_neighbors_vectors ( int dimension,
                                                        int number_nn);

vector<vector<int> > initiate_FCC_coordinates ( int dimension,
                                                int mesh_size,
                                                int constant,
                                                int ly,
                                                int lx,
                                                int lz);

vector<int> define_cell_occupation ( int tot_mesh_points);

vector<vector<int> > list_nearest_neighbors_each_lattice_point( string boundry,
                                                                int dimension,
                                                                int tot_mesh_points,
                                                                int number_neighbors,
                                                                int constant,
                                                                int ly,
                                                                int lx,
                                                                int lz,
                                                                vector<  vector <int>> fcc_mesh_pionts,
                                                                vector< vector <int>> list_primery_vectors);

vector<vector<int> > list_backward_bonds( int number_neighbors,
                                          vector< vector <int>> primary_vectors);

vector<vector<int> > list_forward_bonds( int number_neighbors,
                                         vector< vector <int>> primary_vectors);

vector<vector<int> > list_bending_bonds ( int number_neighbors,
                                         vector< vector <int>> primary_vectors);

vector <vector<vector<int>> > reptation_hairpin_moves( int number_neighbors,
                                                       vector< vector <int>> forward_bonds,
                                                       vector< vector <int>> backward_bonds
                                                       );

vector <vector<vector<int>> > bending_moves ( int number_neighbors,
                                              vector< vector <int>> forward_bonds,
                                              vector< vector <int>> backward_bonds,
                                             vector <int> opposite_primitive_vectors);


//vector<vector<int> > shared_neighbors_primitive_bonds ( int number_neighbors,
//                                                       vector< vector <int>> primary_vectors,
//                                                       vector< vector <int>> backward_bonds);


vector<double> initialise_time_measurment( int mc_sweep,
                                           int ntim);


vector<int> list_paired_primitive_vectors(int number_nearest_neighbors,
                                          vector< vector <int> > list_primary_vectors);


bool accept_move(std::mt19937_64 &RNG, double Hamiltonian);

#endif /* lattice_functions_h */
