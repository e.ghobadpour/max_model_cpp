//
//  geometry.cpp
//  FCC_Max
//  Created by ghobadel on 28/07/2021.
//
#ifndef parameters_hpp
#define parameters_hpp


#include<sstream>
using namespace std;

// Global variable declaration:
struct General_Parameters{
    int DIMENSION = 3;
    int N_NEIGHBORS = DIMENSION * (DIMENSION+1); //   number of nearest neighbors for each site
    int Lx ; //    rows of atoms
    int Ly ; //    columns of atoms
    int Lz ; //    stacks of mesh sheets :
    int LATTICE_CONSTANT ; //    the lattice constant for your structur:
    int N_MESH_POINTS;
    int N_MONOMERS ; //   number of monomers
    int N_POLYMERS ; //    number of polymers
    int TOTAL_N_MON ; //     total number of monomers in the simulation
    int N_CELL ; //     total number of possible monomer in one cell
    int FUNCTIONALITY ; //     Max number of branches from one tree node
    int MC_GROW ; // Monte Cralo sweep at grow phase
    int MC_EQUIB ;
    long int MC_DYN ;
    double DENSITY_T ;
    double mixing ;
    double mixingRepton;
    int N_T ; //equilibrium number of tree nodes
    double TREE_LENGTH_STIFFNESS;
    double BRANCH_POTENITIAL ;
    double INTERACTION_POTENITIAL ;
//    double BRANCH_PROB ;
    int resume_save_freq;
    int nsamp ; // number of independt sample for each node
    int seed_sample ;
    int time_steps_output = 100;
    double start_time_outpout = 1;
    double end_time_output;
    double end_time_output_eq;
    //    string directory = "D:/";
    string fileName = "trajectory.xyz";
    string fileName_eq = "eq_trajectory.xyz";
    string fileName_param = "parameters";
    string fileType = ".txt";
    string fileName_time_eq= "time_ouput_eq.txt";
    string fileName_time_dy= "time_ouput.txt";
    string move ;
    int maxDigits = 3;
    stringstream ss,fs;
    string boundary;
    string interaction_type;
//    string interaction_reptation ;
    string output_directory = "Results";
    string project_name;
    string connectivity;
    bool resume;
    string resume_file_path;
    
};
#endif /* parameters_hpp */
