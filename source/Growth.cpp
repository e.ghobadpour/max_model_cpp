//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "model_functions.hpp"
#include "lattice_functions.hpp"
#include "OneMonMove_functions.hpp"
#include <stdlib.h>
#include <random>



void  place_trimers ( int ip,
                     int mesh_size,
                     int &nt0,
                     vector<int> &icl,
                     vector<int> &N_tree_node_on_lattice_sites,
                     vector<int> &monomer_lattice_pos,
                     vector<int> &previous_mon,
                     vector<int> &next_mon,
                     vector<int> &monomer_bond,
                     vector<int> &monomer_belong_polymer,
                     vector<int> &length_chain,
                     vector<vector<int>> &sites_info,
                     vector <vector <int> >  &tree_node,
                     vector <int>   &lattice_sites_nodes,
                     vector <int> &monomer_tree_position,
                     vector<int> &functionality_tree_node,
                     mt19937_64 &RNG
                     )
{
    vector <int> temp;
    int n;
    //This function randomely choose one lattice poin and put a trimer on it.
    int trimer_lattice_point;
    // choose randomly one lattice site :
label:
    trimer_lattice_point = RNG() % mesh_size ;
    if(trimer_lattice_point==0)
    {
        trimer_lattice_point=20;
    }
    if (icl[trimer_lattice_point] != 0)
    {
        goto label;
    }
    // update trees information.
    
    n = tree_node.size();
    
    vector<int> empty;
//    tree_node.push_back(empty);
    functionality_tree_node.resize(n+1,0);
    lattice_sites_nodes.resize(n+1,0);
//    cout<<"tree_node "<<tree_node.size()<<endl;
    
    // put the trimer :

    for (int is=1; is<4; is++)
    {
        nt0 += 1;
        monomer_lattice_pos[nt0] = trimer_lattice_point;
        monomer_bond[nt0] = 0;
        previous_mon[nt0] = nt0-1;
        next_mon[nt0] = nt0 + 1;
        monomer_belong_polymer[nt0] = ip;
        length_chain[ip] += 1;
        sites_info[trimer_lattice_point].push_back(nt0);
        monomer_tree_position[nt0] = n;
    }
    previous_mon[nt0-2] = nt0;
    next_mon[nt0] = nt0-2;
    //    info sites:
    icl[trimer_lattice_point] = 3;
    N_tree_node_on_lattice_sites[trimer_lattice_point] = 1;
//    functionality[trimer_lattice_point] = 0;
    
    tree_node.push_back(sites_info[trimer_lattice_point]);
    lattice_sites_nodes[n]=trimer_lattice_point;
    functionality_tree_node[n]=0;
    
}



void  add_new_monomer(int nm,
                      int mesh_size,
                      int ncell,
                      int &nt0,
                      vector<int> &icl,
                      vector<int> &monomer_lattice_pos,
                      vector<int> &previous_mon,
                      vector<int> &next_mon,
                      vector<int> &monomer_bond,
                      vector<int> &monomer_belong_polymer,
                      vector<int> &length_chain,
                      vector<vector<int>> &sites_info,
                      vector <vector <int> >  &nodes_tree,
                      vector <int> &monomer_tree_position
                      )
{
    
    int last_monomer;
    int lattice_pos_last_monomer;
    int polymer_mn;
    int ix;
    int tree_node;
    
    // add new monomer to the last monomer.
    //    nm_random=min((rand() % nt0)+1,nt0);
    last_monomer = nt0;
    lattice_pos_last_monomer = monomer_lattice_pos[last_monomer];
    polymer_mn = monomer_belong_polymer[last_monomer];
    if ( icl[lattice_pos_last_monomer] < ncell && length_chain[polymer_mn] < nm)
    {
        nt0+=1; //update the total number of monomers
        //        update monomers(ring) and lattice
        monomer_lattice_pos[nt0] = lattice_pos_last_monomer; // define the position of the new monomer on the lattice
        monomer_bond[nt0] = monomer_bond[last_monomer];//define bond new monomer
        monomer_bond[last_monomer] = 0; // update bond chosen monomer (bond=0, loop or repton)
        // umpdate prevois and next monomers
        previous_mon[nt0] = last_monomer;
        ix = next_mon[last_monomer];
        next_mon[nt0] = ix;
        previous_mon[ix] = nt0;
        next_mon[last_monomer] = nt0;
        icl[lattice_pos_last_monomer] += 1;// update the number of monomer on that lattice site
        sites_info[lattice_pos_last_monomer].push_back(nt0);
        monomer_belong_polymer[nt0] = polymer_mn;
        length_chain[polymer_mn] += 1;
        
        //      update tree_node
        tree_node  = monomer_tree_position[last_monomer];
        monomer_tree_position[nt0] = tree_node;
        nodes_tree[tree_node].push_back(nt0);
        
        
    }
    
}
vector<double>  Hamiltonian_function(vector<int> &total_number_tree_nodes,
                  vector<int> &total_functionality_1,
                  int number_polymers,
                    int equib_length_tree,
                    int tot_mesh_points,
                    double potenitial_branch,
                    double potential_interaction,
                    double stiffness_tree_length,
                    double &energy,
                    vector<int> &N_tree_node_on_lattice_sites)
{
    
    vector<double> energy_output(4,-1);
    double H_br=0;
    double H_int=0;
    double H_cl=0;
    double sum=0;
    
    for (int ip = 1; ip <= number_polymers ; ip++)
    {
        H_br += potenitial_branch * total_functionality_1[ip];
        H_cl = stiffness_tree_length * pow((total_number_tree_nodes[ip] - equib_length_tree),2);
    }
//    cout<<" H_br= "<< H_br<<endl;
//    cout<<" H_cl= "<< H_cl<<endl;
//    H_br = potenitial_branch * total_functionality_1;
//    H_cl = stiffness_tree_length * pow((total_number_tree_nodes - equib_length_tree),2);
    for (int i=0; i< tot_mesh_points; i++)
    {
        if (N_tree_node_on_lattice_sites[i]!=0)
        {
//            cout<<"N_tree_node_on_lattice_sites[i] "<<N_tree_node_on_lattice_sites[i]<<endl;
//            sum +=  pow(N_tree_node_on_lattice_sites[i]-1, 2);
            sum += 0.5*double(N_tree_node_on_lattice_sites[i])* (N_tree_node_on_lattice_sites[i]-1);

        }

    }
//    cout<<"sum "<<sum<<endl;
    H_int = potential_interaction * sum;
//    cout<<"H_br "<<H_br<<endl;
//    cout<<"total_number_tree_nodes "<<total_number_tree_nodes<<endl;
//    cout<<"H_int "<<H_int<<endl;
//    cout<<"H_cl "<<H_cl<<endl;
    energy = H_br +H_int + H_cl;
    energy_output[0] = H_br;
    energy_output[1] = H_int;
    energy_output[2] = H_cl;
    energy_output[3] = energy;
    return energy_output;

}
