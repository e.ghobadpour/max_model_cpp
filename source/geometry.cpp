//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "lattice_functions.hpp"
#include <iostream>  // For input/output
#include <fstream> // For streaming data to the output file
#include <string> // For strings

vector<vector<int> > initiate_FCC_coordinates( int dimensions,
                                              int tot_mesh_points,
                                              int lattice_constant,
                                              int lx,
                                              int ly,
                                              int lz)
{
    //This function returns the coordinates [x,y,z] of all mesh points in the FCC lattice
    
    vector<vector<int> > FCC_mesh_points_coordinates;
    FCC_mesh_points_coordinates.resize(tot_mesh_points);
    for (int i = 0; i<tot_mesh_points; i++)
    {
        FCC_mesh_points_coordinates[i].resize(dimensions,0);
    }
    
    int c0_x = -lattice_constant, c0_y = -lattice_constant, c0_z = -lattice_constant;
    //    int c0_x = -lattice_constant-5, c0_y = -lattice_constant+3, c0_z = -lattice_constant+3;
    //    int c0_x = -lattice_constant-3, c0_y = -lattice_constant+1, c0_z = -lattice_constant+1;
    
    int c1_x, c1_y, c1_z = -lattice_constant/2;
    //    int c1_x, c1_y, c1_z = -lattice_constant/2+1
    //    int c1_x, c1_y, c1_z = -lattice_constant/2+3
    
    
    ;
    int counter = -1;
    int x0_values[lx], y0_values[ly], x1_values[lx], y1_values[ly], z0_values[lz], z1_values[lz];
    for (int j = 0; j < lz; j ++) // for loops to print the atoms coordinates
    {
        c0_z += lattice_constant;
        c1_z += lattice_constant;
        
        z0_values[j] = c0_z;
        z1_values[j] = c1_z;
        
        c0_x = -lattice_constant;
        //        c0_x = -lattice_constant-3;
        //        c0_x = -lattice_constant-5;
        for (int i = 0; i < lx; i ++)
        {
            c0_x += lattice_constant;
            c1_x = c0_x + lattice_constant/2;
            x0_values[i] = c0_x;
            x1_values[i] = c1_x;
            
            //            c0_y = -lattice_constant+1;
            //            c0_y = -lattice_constant+3;
            c0_y = -lattice_constant;
            for (int n = 0; n < ly; n ++)
            {
                c0_y += lattice_constant;
                c1_y = c0_y + lattice_constant/2;
                y0_values[n] = c0_y;
                y1_values[n] = c1_y;
                counter += 1;
                FCC_mesh_points_coordinates[counter][0] = x0_values[i];
                FCC_mesh_points_coordinates[counter][1] = y0_values[n];
                FCC_mesh_points_coordinates[counter][2] = z0_values[j];
                
                counter += 1;
                FCC_mesh_points_coordinates[counter][0] = x1_values[i];
                FCC_mesh_points_coordinates[counter][1] = y1_values[n];
                FCC_mesh_points_coordinates[counter][2] = z0_values[j];
                
                counter += 1;
                FCC_mesh_points_coordinates[counter][0] = x0_values[i];
                FCC_mesh_points_coordinates[counter][1] = y1_values[n];
                FCC_mesh_points_coordinates[counter][2] = z1_values[j];
                
                counter += 1;
                FCC_mesh_points_coordinates[counter][0] = x1_values[i];
                FCC_mesh_points_coordinates[counter][1] = y0_values[n];
                FCC_mesh_points_coordinates[counter][2] = z1_values[j];
                
            }
        }
    }
    string outputfile, type;
    outputfile = "data.xyz";
    ofstream out;
    out.open(outputfile.c_str());
    for (int i=0; i< tot_mesh_points; i++) {
        
        out<<FCC_mesh_points_coordinates[i][0]<< "\t"<<FCC_mesh_points_coordinates[i][1]<< "\t"<<FCC_mesh_points_coordinates[i][2]<< "\t"<<endl;
    }
    
    out.close();
    return  FCC_mesh_points_coordinates;
}




vector<vector<int> > define_nearest_neighbors_vectors( int dimensions,
                                                      int number_nearest_neighbors)
{
    // This function returns 12 vectors pointing into all nearest neighbors of the primitive cell(site, [0,0,0])
    // vector (0,0,0) is primitive cell(site)
    
    
    vector<vector<int> > nearest_neighbor_primitive;
    nearest_neighbor_primitive.resize(number_nearest_neighbors+1);
    for (int i = 0 ; i < number_nearest_neighbors+1; i++)
    {
        nearest_neighbor_primitive[i].resize(dimensions,0);
    }
    int neighbor_counter = 0;
    vector<int> one_N_one = {1,-1};
    
    for (int i=0; i<dimensions-1; i++)
    {
        for (int j=i+1;j<dimensions;j++)
        {
            for (auto const& k : one_N_one)
            {
                for (auto const& l : one_N_one)
                {
                    neighbor_counter += 1;
                    nearest_neighbor_primitive[neighbor_counter][i] = k;
                    nearest_neighbor_primitive[neighbor_counter][j] = l;
                }
            }
        }
    }
    
    //    for (int i=0; i<number_nearest_neighbors+1; i++)
    //    {
    //        cout<<nearest_neighbor_primitive[i][0]<<" "<<nearest_neighbor_primitive[i][1]<<" "<<nearest_neighbor_primitive[i][2]<<endl;
    //    }
    return  nearest_neighbor_primitive;
}

// This function returns all paired vectores (vectores with oposite directions) for instance vectors (1,1,1) and (1,1,-1) are paired.
vector<int> list_paired_primitive_vectors(int number_nearest_neighbors,
                                          vector< vector <int>> list_primary_vectors)
{
    vector<int> opposite_primitive_vectors(number_nearest_neighbors, 0);
    int x,y,z;
    for (int i = 1 ; i<number_nearest_neighbors ; i++)
    {
        
        x = list_primary_vectors[i][0];
        y = list_primary_vectors[i][1];
        z = list_primary_vectors[i][2];
        for (int k=i+1; k<number_nearest_neighbors;k++)
        {
            if ( list_primary_vectors[k][0] == -x && list_primary_vectors[k][1] == -y && list_primary_vectors[k][2] == -z)
                
            {
                opposite_primitive_vectors[i] = k;
                opposite_primitive_vectors[k] = i;
                break;
            }
        }
    }
    
    
    return opposite_primitive_vectors;
}



//Function which gives initial monomers per cell equel to zero. set cell occupation equel to zero at the begining of the simulation
vector<int> define_cell_occupation(int tot_mesh_points){
    vector<int> icl;
    icl.resize(tot_mesh_points);
    for (int i=0; i<tot_mesh_points; i++) {
        icl[i]=0;
    }
    return icl;
}


vector<vector<int> > list_nearest_neighbors_each_lattice_point ( string boundry,
                                                                int dimensions,
                                                                int tot_mesh_points,
                                                                int number_neighbors,
                                                                int lattice_constant,
                                                                int lx,
                                                                int ly,
                                                                int lz,
                                                                vector<  vector <int>> fcc_mesh_pionts,
                                                                vector< vector <int>> list_primary_vectors)
{
    // This function returns list of all nearest neighbors of each lattice site (mesh point) in the FCC lattice
    
    // If the neighbor number is -1 : no neighbor for that lattice point in that direction
    int n_x,n_y,n_z;
    vector<vector<int>> list_nn(tot_mesh_points, vector<int> (number_neighbors+1, -1));
    // periodic boundry condition
    if (boundry=="pbc")
    {
        for (int i=0; i<tot_mesh_points;i++)
        {
            for (int j=1; j<number_neighbors+1; j++)
            {
                n_x = fcc_mesh_pionts[i][0] + list_primary_vectors[j][0];
                n_x = (n_x + lattice_constant*lx) % (lattice_constant*lx);
                
                n_y = fcc_mesh_pionts[i][1] + list_primary_vectors[j][1];
                n_y = (n_y + lattice_constant*ly) % (lattice_constant*ly);
                
                n_z = fcc_mesh_pionts[i][2] + list_primary_vectors[j][2];
                n_z = (n_z + lattice_constant*lz) % (lattice_constant*lz);
                
                for (int k=0; k<tot_mesh_points;k++)
                {
                    if ( fcc_mesh_pionts[k][0] == n_x && fcc_mesh_pionts[k][1] == n_y && fcc_mesh_pionts[k][2] == n_z)
                    {
                        list_nn[i][j] = k;
                        break;
                    }
                }
                
            }
        }
    }
    // fixed box,
    // If the neighbor number is -1 : no neighbor for that lattice point in that direction
    if (boundry=="fixed")
    {
        for (int i=0; i<tot_mesh_points;i++)
        {
            for (int j=1; j<number_neighbors+1; j++)
            {
                n_x = fcc_mesh_pionts[i][0] + list_primary_vectors[j][0];
                n_y = fcc_mesh_pionts[i][1] + list_primary_vectors[j][1];
                n_z = fcc_mesh_pionts[i][2] + list_primary_vectors[j][2];
                for (int k=0; k<tot_mesh_points;k++)
                {
                    if ( fcc_mesh_pionts[k][0] == n_x && fcc_mesh_pionts[k][1] == n_y && fcc_mesh_pionts[k][2] == n_z)
                    {
                        list_nn[i][j] = k;
                        break;
                    }
                }
                
            }
            
        }
        
        
    }
    return  list_nn;
}


bool accept_move(std::mt19937_64 &RNG, double Hamiltonian){
    if (Hamiltonian <=0)
    {
        return true;
    }
    else
    {
        if (((double)RNG() / (double)RNG.max()) < exp(-Hamiltonian))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
