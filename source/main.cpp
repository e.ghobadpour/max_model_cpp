//
//  main.cpp
//  FCC_Max
//
//  Created by ghobadel on 20/06/2021.
#include <iostream> //For input/output
#include <fstream> //For streaming data to the output file
#include <sstream>
#include <iomanip>
#include <string> //For strings
#include <cmath> //For math calculation
#include <vector>
#include <set>
//#include "simulation_functions.hpp"
#include <ctime>
#include <cstdlib>
#include <random>
#include <stdlib.h>
#include <boost/filesystem.hpp>
#include "lattice_functions.hpp"
#include "model_functions.hpp"
#include "Diffusion_functions.hpp"
#include "TwoMon_functions.hpp"
#include "OneMonMove_functions.hpp"
#include "output_functions.hpp"
#include "parameters.hpp"
#include "cxxopts.hpp"
#include "arg_parse_functions.hpp"
#include <chrono>
#include <ctime>

using namespace std;


int main(int argc, char *argv[])
{
    General_Parameters param = argument_parser(argc,argv);
    
    if (param.resume) {
        param = import_params_for_resume(param);
        param.resume = true;
    }
    
    cout<<"Lx = "<<param.Lx<<endl;
    cout<<"timedynamic = "<<param.MC_DYN<<endl;
    //    exit(1);
    ofstream out;
    string outputfile, type;
    double time_phase_grow;
    double time_phase_equib;
    double time_phase_dyn=0.0;
    double time_phase_eq;
    double dt;
    int number_occupied_cells;
    float lattice_density_sample;
    float lattice_chain_length_sample;
    float cell_occupation_number;
    int number_of_tree_nodes;
    //    int ntime;
    int ntime_eq;
    //********************************************************************************
    //                   lattice and tables  initialization
    //********************************************************************************
    vector<vector<int> > FCC_coordinates = initiate_FCC_coordinates(param.DIMENSION,
                                                                    param.N_MESH_POINTS,
                                                                    param.LATTICE_CONSTANT,
                                                                    param.Lx,
                                                                    param.Ly,
                                                                    param.Lz);
    
    if (param.LATTICE_CONSTANT!=2 )
    {
        
        string ErrorMessage="Lattice constant is not equal to 2.";
        throw runtime_error(ErrorMessage);
    }
    
    vector<vector<int> > nearest_neighbor_primitive = define_nearest_neighbors_vectors(param.DIMENSION,
                                                                                       param.N_NEIGHBORS);
    
    vector<vector<int> > neighbors_mesh_points = list_nearest_neighbors_each_lattice_point(param.boundary,
                                                                                           param.DIMENSION,
                                                                                           param.N_MESH_POINTS,
                                                                                           param.N_NEIGHBORS,
                                                                                           param.LATTICE_CONSTANT,
                                                                                           param.Lx,
                                                                                           param.Ly,
                                                                                           param.Lz,
                                                                                           FCC_coordinates,
                                                                                           nearest_neighbor_primitive);
    
    vector<vector<int> > bonds_backward = list_backward_bonds(param.N_NEIGHBORS,
                                                              nearest_neighbor_primitive);
    
    vector<vector<int> > bonds_forward = list_forward_bonds(param.N_NEIGHBORS,
                                                            nearest_neighbor_primitive);
    
    
    vector <vector<vector<int>> > possible_moves = reptation_hairpin_moves(param.N_NEIGHBORS,
                                                                           bonds_forward,
                                                                           bonds_backward);
    
    vector<int> opposite_primitive_vectors = list_paired_primitive_vectors(param.N_NEIGHBORS+1,
                                                                           nearest_neighbor_primitive );
    vector <vector<vector<int>> > possible_bending_moves = bending_moves (param.N_NEIGHBORS,
                                                                          bonds_forward,
                                                                          bonds_backward,
                                                                          opposite_primitive_vectors);
    //********************************************************************************
    //                   vectors initialization for each sample
    //********************************************************************************

    for (int isamp = 0 ; isamp < param.nsamp ; isamp++)
        
    {
        std::mt19937_64 RNG(param.seed_sample+isamp); //random number generator
        clock_t tStart = clock();
//        srand( (unsigned)time( NULL ) );
        
        auto chronoSteadyClockStart = chrono::steady_clock:: now();
        auto chronoSystemClockStart = chrono:: system_clock:: now();
        
        string filePath = assign_dir(param,isamp);
        string resumeFilePath = filePath + "resume.bin";
        create_path(filePath);
        if (!param.resume)
        {
            write_initial_parameters(param,filePath);
        }
        
        vector<int> cell_occupation = define_cell_occupation(param.N_MESH_POINTS);
        vector<int> cell_tree_occupation(param.N_MESH_POINTS,0); // number of tree nodes on each lattice site
        vector<int> monomer_lattice_pos(param.TOTAL_N_MON+1,0); //position each mononer on the lattice
        
        vector<int> previous_mon(param.TOTAL_N_MON+1,0);// the index of the previus monomer
        
        vector<int> next_mon(param.TOTAL_N_MON+1,0);// the index of the next monomer
        
        vector<int> monomer_bond(param.TOTAL_N_MON+1,0);// bond of the  all monomers ,
        
        vector <vector <int> > list_monomers_on_each_lattice_site;
        list_monomers_on_each_lattice_site.resize(param.N_MESH_POINTS); // which monomers are in each lattice site
        vector<int> where_monomer_is_on_tree (param.TOTAL_N_MON+1,-1);
        vector <int> lattice_position_tree_nodes;
        vector <vector <int> > tree_nodes_info;
        vector<int> functionality_tree_nodes;
        vector<int> list_available_index_tree_nodes;
        
        vector<int> sum_functionality_1(param.N_POLYMERS+1,1);// start from one, because very first branch make two tree nodes with functionality one
        vector<int> tot_number_of_tree_nodes(param.N_POLYMERS+1,1);
        vector<int> monomer_belong_polymer(param.TOTAL_N_MON+1,0);// this info says each monomer blong to which polymer chain
        double energy_sum =0;
        double delta_energy  = 0;
        vector<double> energy_each_term_hamiltonian(4,-1);
        
        int nt0 = 0;
        long int accept_move_monomer_eq = 0;
        long int accept_move_monomer_dy = 0,accept_move_two_mon_dy = 0;
        long int accept_move_tree_node_dy = 0;
        long int accept_move_tree_node_eq = 0;
        
        
        vector<int> length_chain(param.N_POLYMERS+1,0);
        
        vector<int> vector_functionalities(param.N_NEIGHBORS+1,0); //record functionalities for the last frame
        
        vector<long int> vector_attempt_move_tree_with_functionality(param.N_NEIGHBORS+1,0);
        vector<long int> vector_accept_move_tree_with_functionality(param.N_NEIGHBORS+1,0);
        vector<long int> vector_attempt_move_tree_with_functionality_eq(param.N_NEIGHBORS+1,0);
        vector<long int> vector_accept_move_tree_with_functionality_eq(param.N_NEIGHBORS+1,0);
        long int counter_one_mon_step =0;
        long int counter_two_mon_step =0;
        long int counter_tree_node_step =0;
        long int iteration;
        
        vector <vector <int>> old_position_first_monomer(param.N_POLYMERS, vector<int> (3, 0));
        vector <vector <int>> new_position_first_monomer(param.N_POLYMERS, vector<int> (3, 0));
        vector <vector <int>> record_pbc_shift(param.N_POLYMERS, vector<int> (3, 0));
        vector<long int> time_steps_log = logspace(param.start_time_outpout, param.end_time_output, param.time_steps_output);
        vector<long int> time_steps_log_eq = logspace(param.start_time_outpout, param.end_time_output_eq, param.time_steps_output);
        //        vector<long int> time_steps_log = logarithmicIntervals(param.start_time_outpout, param.end_time_output, param.time_steps_output);
        
        //        for (int i=0; i<time_steps_log.size();i++)
        //        {
        //            cout<<time_steps_log[i]<<endl;
        //        }
        int counter_step;
        
        int ipm, mj;
        //********************************************************************************
        //
        //                                         Growth phase
        //
        //********************************************************************************
      
        tree_nodes_info.clear();
        functionality_tree_nodes.clear();
        if (!param.resume)
        {
            
            for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
            {
                place_trimers(ip,
                              param.N_MESH_POINTS,
                              nt0,
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              monomer_belong_polymer,
                              length_chain,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              lattice_position_tree_nodes,
                              where_monomer_is_on_tree,
                              functionality_tree_nodes,
                              RNG
                              );
                time_phase_grow = 0.0;
                dt = 1.0 ;
                while( length_chain[ip] < param.N_MONOMERS || time_phase_grow < param.MC_GROW)
                {
                    
                    time_phase_grow += dt/nt0;
                    One_Monomer_Move(param.interaction_type,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     nt0,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_eq,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                    
                    if (((double)rand() / (double)RAND_MAX) < 0.01)
                    {
                        add_new_monomer(param.N_MONOMERS,
                                        param.N_MESH_POINTS,
                                        param.N_CELL,
                                        nt0,
                                        cell_occupation,
                                        monomer_lattice_pos,
                                        previous_mon,
                                        next_mon,
                                        monomer_bond,
                                        monomer_belong_polymer,
                                        length_chain,
                                        list_monomers_on_each_lattice_site,
                                        tree_nodes_info,
                                        where_monomer_is_on_tree);
                        
                        
                        
                    }
                }
            }
            //********************************************************************************
            //
            //                                         Equilibruim phase of the simulation
            //
            //********************************************************************************
         
            write_output_xyz(filePath + param.fileName_eq,
                             param,
                             monomer_lattice_pos,
                             FCC_coordinates,
                             monomer_bond,
                             nearest_neighbor_primitive,
                             record_pbc_shift,
                             0);
            write_time_steps_opt(filePath + param.fileName_time_eq,
                                 time_steps_log_eq,
                                 0,
                                 param.TOTAL_N_MON);
            
            
            delta_energy =0;
            time_phase_equib = 0.0;
            ntime_eq = param.TOTAL_N_MON;
            dt = 1.0/ntime_eq;
            iteration = 0;
            int N_sweep = ntime_eq;
            int counter_step_eq =0;
            double init_time_phase_eq =0;
            long int random_move_eq;
            int tot_N_t_eq;
            float mixing_eq;
            
            for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
            {
                ipm = (ip-1) * param.N_MONOMERS;
                mj = monomer_lattice_pos[ipm+1];
                old_position_first_monomer[ip-1] = FCC_coordinates[mj];
            }
            
            
            for ( time_phase_eq  = init_time_phase_eq;time_phase_eq < param.MC_EQUIB; time_phase_eq +=ntime_eq*dt)
                
            {
                
                tot_N_t_eq=0;
                for (int i=1; i<param.N_POLYMERS+1; i++) {
                    tot_N_t_eq += tot_number_of_tree_nodes[i];
                }
                if (param.move == "mixed")
                {
                    N_sweep = tot_N_t_eq + param.TOTAL_N_MON;
                    int trial_moves =0;
                    mixing_eq =param.mixing;
                    if (mixing_eq<0.5)
                    {
                        mixing_eq = 0.5;
                    }
                    trial_moves=mixing_eq*param.TOTAL_N_MON+(1-mixing_eq)*tot_N_t_eq;
                    for ( int count_eq_1 = 1; count_eq_1 <= trial_moves; count_eq_1++)
                    {
                        random_move_eq  = RNG() % trial_moves ;
                        if ( random_move_eq < (mixing_eq)*param.TOTAL_N_MON)
                        {
                            One_Monomer_Move(param.interaction_type,
                                             param.BRANCH_POTENITIAL,
                                             param.INTERACTION_POTENITIAL,
                                             param.TREE_LENGTH_STIFFNESS,
                                             param.N_T,
                                             param.N_MESH_POINTS,
                                             param.N_CELL,
                                             param.TOTAL_N_MON,
                                             cell_occupation,
                                             cell_tree_occupation,
                                             monomer_lattice_pos,
                                             previous_mon,
                                             next_mon,
                                             monomer_bond,
                                             possible_moves,
                                             neighbors_mesh_points,
                                             bonds_forward,
                                             bonds_backward,
                                             opposite_primitive_vectors,
                                             list_monomers_on_each_lattice_site,
                                             where_monomer_is_on_tree,
                                             lattice_position_tree_nodes,
                                             tree_nodes_info,
                                             functionality_tree_nodes,
                                             list_available_index_tree_nodes,
                                             accept_move_monomer_eq,
                                             param.FUNCTIONALITY,
                                             sum_functionality_1,
                                             tot_number_of_tree_nodes,
                                             delta_energy,
                                             monomer_belong_polymer,
                                             RNG);
                        }
                        else
                        {
                            
                            Tree_Node_Move(param.interaction_type,
                                           param.INTERACTION_POTENITIAL,
                                           param.N_MESH_POINTS,
                                           param.N_CELL,
                                           cell_occupation,
                                           cell_tree_occupation,
                                           monomer_lattice_pos,
                                           previous_mon,
                                           next_mon,
                                           monomer_bond,
                                           neighbors_mesh_points,
                                           bonds_forward,
                                           bonds_backward,
                                           list_monomers_on_each_lattice_site,
                                           tree_nodes_info,
                                           lattice_position_tree_nodes,
                                           where_monomer_is_on_tree,
                                           functionality_tree_nodes,
                                           accept_move_tree_node_eq,
                                           delta_energy,
                                           vector_attempt_move_tree_with_functionality_eq,
                                           vector_accept_move_tree_with_functionality_eq,
                                           RNG);
                            
                            
                        }
                    }
                    
                }
                else
                {
                    for ( int count_eq_1 = 1; count_eq_1 <= ntime_eq; count_eq_1++)
                    {
                        
                        One_Monomer_Move(param.interaction_type,
                                         param.BRANCH_POTENITIAL,
                                         param.INTERACTION_POTENITIAL,
                                         param.TREE_LENGTH_STIFFNESS,
                                         param.N_T,
                                         param.N_MESH_POINTS,
                                         param.N_CELL,
                                         param.TOTAL_N_MON,
                                         cell_occupation,
                                         cell_tree_occupation,
                                         monomer_lattice_pos,
                                         previous_mon,
                                         next_mon,
                                         monomer_bond,
                                         possible_moves,
                                         neighbors_mesh_points,
                                         bonds_forward,
                                         bonds_backward,
                                         opposite_primitive_vectors,
                                         list_monomers_on_each_lattice_site,
                                         where_monomer_is_on_tree,
                                         lattice_position_tree_nodes,
                                         tree_nodes_info,
                                         functionality_tree_nodes,
                                         list_available_index_tree_nodes,
                                         accept_move_monomer_eq,
                                         param.FUNCTIONALITY,
                                         sum_functionality_1,
                                         tot_number_of_tree_nodes,
                                         delta_energy,
                                         monomer_belong_polymer,
                                         RNG);
                        
                        Tree_Node_Move(param.interaction_type,
                                       param.INTERACTION_POTENITIAL,
                                       param.N_MESH_POINTS,
                                       param.N_CELL,
                                       cell_occupation,
                                       cell_tree_occupation,
                                       monomer_lattice_pos,
                                       previous_mon,
                                       next_mon,
                                       monomer_bond,
                                       neighbors_mesh_points,
                                       bonds_forward,
                                       bonds_backward,
                                       list_monomers_on_each_lattice_site,
                                       tree_nodes_info,
                                       lattice_position_tree_nodes,
                                       where_monomer_is_on_tree,
                                       functionality_tree_nodes,
                                       accept_move_tree_node_eq,
                                       delta_energy,
                                       vector_attempt_move_tree_with_functionality_eq,
                                       vector_accept_move_tree_with_functionality_eq,
                                       RNG);
                    }
                    
                }
                
                for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
                {
                    ipm = (ip-1) * param.N_MONOMERS;
                    mj = monomer_lattice_pos[ipm+1];
                    new_position_first_monomer[ip-1] = FCC_coordinates[mj];
                }
                
                
                
                record_position_first_monomer(old_position_first_monomer,
                                              new_position_first_monomer,
                                              record_pbc_shift,
                                              param.LATTICE_CONSTANT,
                                              param.Lx,
                                              param.Ly,
                                              param.Lz,
                                              param.N_POLYMERS);
                if ( iteration == time_steps_log_eq[counter_step_eq])
                    
                {
                    counter_step_eq += 1;
                    
                    write_output_xyz(filePath + param.fileName_eq,
                                     param,
                                     monomer_lattice_pos,
                                     FCC_coordinates,
                                     monomer_bond,
                                     nearest_neighbor_primitive,
                                     record_pbc_shift,
                                     counter_step_eq);
                    write_time_steps_opt(filePath + param.fileName_time_eq,
                                         time_steps_log_eq,
                                         counter_step_eq,
                                         N_sweep);
                }
                iteration += 1;
            }
        }
        
        //********************************************************************************
        //
        //                                         Dynamics
        //
        //********************************************************************************
        
        
        counter_step =0;
        time_phase_dyn = 0.0;
        iteration = 0;
        bool order = false;
        int tot_N_t;
        long int count_dy_1 ;
        long int random_move;
        
//        if (!param.resume) {
//            param.ss.str("");
//            param.ss<<param.fileName;
            //            write_output_xyz(filePath + param.ss.str(),
            //                             param,
            //                             monomer_lattice_pos,
            //                             FCC_coordinates,
            //                             monomer_bond,
            //                             nearest_neighbor_primitive,
            //                             record_pbc_shift,
            //                             counter_step);
            //            write_time_steps_opt(filePath + param.fileName_time_dy,
            //                                 time_steps_log,
            //                                 counter_step,
            //                                 param.TOTAL_N_MON);
            //            counter_step+=1;
//        }
        long int num_steps_without_resume;
        
        if (param.resume_save_freq!=0)
        {
            num_steps_without_resume =param.MC_DYN/param.resume_save_freq;
        } else {
            num_steps_without_resume =param.MC_DYN + 0.5;
        }
        long int resume_save_time = num_steps_without_resume;
        
        double init_time_phase_dyn =0;
        if (!param.resume) {
            export_for_resume(resumeFilePath,
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              where_monomer_is_on_tree,
                              lattice_position_tree_nodes,
                              functionality_tree_nodes,
                              list_available_index_tree_nodes,
                              sum_functionality_1,
                              tot_number_of_tree_nodes,
                              monomer_belong_polymer,
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              old_position_first_monomer,
                              new_position_first_monomer,
                              record_pbc_shift,
                              time_phase_dyn,
                              delta_energy,
                              iteration,
                              counter_step,
                              accept_move_tree_node_dy,
                              accept_move_two_mon_dy,
                              accept_move_monomer_dy,
                              RNG
                              );
            
            export_for_resume(resumeFilePath + "_backup",
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              where_monomer_is_on_tree,
                              lattice_position_tree_nodes,
                              functionality_tree_nodes,
                              list_available_index_tree_nodes,
                              sum_functionality_1,
                              tot_number_of_tree_nodes,
                              monomer_belong_polymer,
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              old_position_first_monomer,
                              new_position_first_monomer,
                              record_pbc_shift,
                              time_phase_dyn,
                              delta_energy,
                              iteration,
                              counter_step,
                              accept_move_tree_node_dy,
                              accept_move_two_mon_dy,
                              accept_move_monomer_dy,
                              RNG
                              );
        } else {
            import_for_resume(resumeFilePath,
                              cell_occupation,
                              cell_tree_occupation,
                              monomer_lattice_pos,
                              previous_mon,
                              next_mon,
                              monomer_bond,
                              where_monomer_is_on_tree,
                              lattice_position_tree_nodes,
                              functionality_tree_nodes,
                              list_available_index_tree_nodes,
                              sum_functionality_1,
                              tot_number_of_tree_nodes,
                              monomer_belong_polymer,
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              list_monomers_on_each_lattice_site,
                              tree_nodes_info,
                              old_position_first_monomer,
                              new_position_first_monomer,
                              record_pbc_shift,
                              init_time_phase_dyn,
                              delta_energy,
                              iteration,
                              counter_step,
                              accept_move_tree_node_dy,
                              accept_move_two_mon_dy,
                              accept_move_monomer_dy,
                              RNG
                              );
            resume_save_time = init_time_phase_dyn - 1 + num_steps_without_resume;
            cout<<"resumed time_phase_dyn = "<<init_time_phase_dyn<<endl;
            cout<<"resumed resume_save_time = "<<resume_save_time<<endl;
            cout<<flush;
        }
        
        
        int N_sweep = 0;
        for ( time_phase_dyn =init_time_phase_dyn;time_phase_dyn <= param.MC_DYN; time_phase_dyn +=1)
        {
            if (time_phase_dyn > resume_save_time) {
                
                copy_over_resume_backup(resumeFilePath);
                copy_over_RNGstate_backup(resumeFilePath);
                export_for_resume(resumeFilePath,
                                  cell_occupation,
                                  cell_tree_occupation,
                                  monomer_lattice_pos,
                                  previous_mon,
                                  next_mon,
                                  monomer_bond,
                                  where_monomer_is_on_tree,
                                  lattice_position_tree_nodes,
                                  functionality_tree_nodes,
                                  list_available_index_tree_nodes,
                                  sum_functionality_1,
                                  tot_number_of_tree_nodes,
                                  monomer_belong_polymer,
                                  vector_functionalities,
                                  vector_attempt_move_tree_with_functionality,
                                  vector_accept_move_tree_with_functionality,
                                  list_monomers_on_each_lattice_site,
                                  tree_nodes_info,
                                  old_position_first_monomer,
                                  new_position_first_monomer,
                                  record_pbc_shift,
                                  time_phase_dyn,
                                  delta_energy,
                                  iteration,
                                  counter_step,
                                  accept_move_tree_node_dy,
                                  accept_move_two_mon_dy,
                                  accept_move_monomer_dy,
                                  RNG
                                  );
                resume_save_time += num_steps_without_resume;
                
            }
            tot_N_t=0;
            for (int i=1; i<param.N_POLYMERS+1; i++) {
                tot_N_t += tot_number_of_tree_nodes[i];
            }
            
            if (param.move == "tree_node")
            {
                
                for ( int count_dy_1 = 1; count_dy_1 <= tot_N_t; count_dy_1++)
                {
                    
                    N_sweep = tot_N_t;
                    
                    Tree_Node_Move(param.interaction_type,
                                   param.INTERACTION_POTENITIAL,
                                   param.N_MESH_POINTS,
                                   param.N_CELL,
                                   cell_occupation,
                                   cell_tree_occupation,
                                   monomer_lattice_pos,
                                   previous_mon,
                                   next_mon,
                                   monomer_bond,
                                   neighbors_mesh_points,
                                   bonds_forward,
                                   bonds_backward,
                                   list_monomers_on_each_lattice_site,
                                   tree_nodes_info,
                                   lattice_position_tree_nodes,
                                   where_monomer_is_on_tree,
                                   functionality_tree_nodes,
                                   accept_move_tree_node_dy,
                                   delta_energy,
                                   vector_attempt_move_tree_with_functionality,
                                   vector_accept_move_tree_with_functionality,
                                   RNG);
                    counter_tree_node_step +=1;
                    
                    
                }
                
            }
            
            if (param.move == "one_mon")
            {
                for ( int count_dy_1 = 1; count_dy_1 <= param.TOTAL_N_MON; count_dy_1++)
                {
                    N_sweep = param.TOTAL_N_MON;
                    delta_energy =0;
                    One_Monomer_Move(param.interaction_type,
                                     param.BRANCH_POTENITIAL,
                                     param.INTERACTION_POTENITIAL,
                                     param.TREE_LENGTH_STIFFNESS,
                                     param.N_T,
                                     param.N_MESH_POINTS,
                                     param.N_CELL,
                                     param.TOTAL_N_MON,
                                     cell_occupation,
                                     cell_tree_occupation,
                                     monomer_lattice_pos,
                                     previous_mon,
                                     next_mon,
                                     monomer_bond,
                                     possible_moves,
                                     neighbors_mesh_points,
                                     bonds_forward,
                                     bonds_backward,
                                     opposite_primitive_vectors,
                                     list_monomers_on_each_lattice_site,
                                     where_monomer_is_on_tree,
                                     lattice_position_tree_nodes,
                                     tree_nodes_info,
                                     functionality_tree_nodes,
                                     list_available_index_tree_nodes,
                                     accept_move_monomer_dy,
                                     param.FUNCTIONALITY,
                                     sum_functionality_1,
                                     tot_number_of_tree_nodes,
                                     delta_energy,
                                     monomer_belong_polymer,
                                     RNG);
                    counter_one_mon_step+=1;
                }
            }
            
            if (param.move == "mixed")
            {
                
                N_sweep = tot_N_t + param.TOTAL_N_MON;
                if (order==true)
                {
                    // the tree node move and one monomer move will come one after the other one.
                    
                    for ( int count_dy_1 = 1; count_dy_1 <= tot_N_t; count_dy_1++)
                    {
                        Tree_Node_Move(param.interaction_type,
                                       param.INTERACTION_POTENITIAL,
                                       param.N_MESH_POINTS,
                                       param.N_CELL,
                                       cell_occupation,
                                       cell_tree_occupation,
                                       monomer_lattice_pos,
                                       previous_mon,
                                       next_mon,
                                       monomer_bond,
                                       neighbors_mesh_points,
                                       bonds_forward,
                                       bonds_backward,
                                       list_monomers_on_each_lattice_site,
                                       tree_nodes_info,
                                       lattice_position_tree_nodes,
                                       where_monomer_is_on_tree,
                                       functionality_tree_nodes,
                                       accept_move_tree_node_dy,
                                       delta_energy,
                                       vector_attempt_move_tree_with_functionality,
                                       vector_accept_move_tree_with_functionality,
                                       RNG);
                        counter_tree_node_step +=1;

                    }
//                    for ( int count_dy_1 = 1; count_dy_1 <= param.TOTAL_N_MON; count_dy_1++)
//                    {
//                        delta_energy =0;
//                        One_Monomer_Move_repton(param.interaction_type,
//                                         param.mixingRepton,
//                                         param.BRANCH_POTENITIAL,
//                                         param.INTERACTION_POTENITIAL,
//                                         param.TREE_LENGTH_STIFFNESS,
//                                         param.N_T,
//                                         param.N_MESH_POINTS,
//                                         param.N_CELL,
//                                         param.TOTAL_N_MON,
//                                         cell_occupation,
//                                         cell_tree_occupation,
//                                         monomer_lattice_pos,
//                                         previous_mon,
//                                         next_mon,
//                                         monomer_bond,
//                                         possible_moves,
//                                         neighbors_mesh_points,
//                                         bonds_forward,
//                                         bonds_backward,
//                                         opposite_primitive_vectors,
//                                         list_monomers_on_each_lattice_site,
//                                         where_monomer_is_on_tree,
//                                         lattice_position_tree_nodes,
//                                         tree_nodes_info,
//                                         functionality_tree_nodes,
//                                         list_available_index_tree_nodes,
//                                         accept_move_monomer_dy,
//                                         param.FUNCTIONALITY,
//                                         sum_functionality_1,
//                                         tot_number_of_tree_nodes,
//                                         delta_energy,
//                                         monomer_belong_polymer,
//                                         RNG);
//                        counter_one_mon_step+=1;
//                    }
                }
                if (order ==false)
                {
                    int trial_moves =0;
                    trial_moves=param.mixing*param.TOTAL_N_MON+(1-param.mixing)*tot_N_t;
                    for ( int count_dy_1 = 1; count_dy_1 <= trial_moves; count_dy_1++)
                    {
                        random_move  = RNG() % trial_moves ;
                        if ( random_move < (param.mixing)*param.TOTAL_N_MON)
                        {
                            One_Monomer_Move(param.interaction_type,
                                             param.BRANCH_POTENITIAL,
                                             param.INTERACTION_POTENITIAL,
                                             param.TREE_LENGTH_STIFFNESS,
                                             param.N_T,
                                             param.N_MESH_POINTS,
                                             param.N_CELL,
                                             param.TOTAL_N_MON,
                                             cell_occupation,
                                             cell_tree_occupation,
                                             monomer_lattice_pos,
                                             previous_mon,
                                             next_mon,
                                             monomer_bond,
                                             possible_moves,
                                             neighbors_mesh_points,
                                             bonds_forward,
                                             bonds_backward,
                                             opposite_primitive_vectors,
                                             list_monomers_on_each_lattice_site,
                                             where_monomer_is_on_tree,
                                             lattice_position_tree_nodes,
                                             tree_nodes_info,
                                             functionality_tree_nodes,
                                             list_available_index_tree_nodes,
                                             accept_move_monomer_dy,
                                             param.FUNCTIONALITY,
                                             sum_functionality_1,
                                             tot_number_of_tree_nodes,
                                             delta_energy,
                                             monomer_belong_polymer,
                                             RNG);
                            
                            
                            counter_one_mon_step+=1;
                            //                            count_dy_1 +=1;
                        }
                        else
                        {
                            
                            Tree_Node_Move(param.interaction_type,
                                           param.INTERACTION_POTENITIAL,
                                           param.N_MESH_POINTS,
                                           param.N_CELL,
                                           cell_occupation,
                                           cell_tree_occupation,
                                           monomer_lattice_pos,
                                           previous_mon,
                                           next_mon,
                                           monomer_bond,
                                           neighbors_mesh_points,
                                           bonds_forward,
                                           bonds_backward,
                                           list_monomers_on_each_lattice_site,
                                           tree_nodes_info,
                                           lattice_position_tree_nodes,
                                           where_monomer_is_on_tree,
                                           functionality_tree_nodes,
                                           accept_move_tree_node_dy,
                                           delta_energy,
                                           vector_attempt_move_tree_with_functionality,
                                           vector_accept_move_tree_with_functionality,
                                           RNG);
                            counter_tree_node_step +=1;
                            //                            count_dy_1 +=3;
                            
                            
                        }
                    }
                }
            }
            
            for (int ip = 1; ip <= param.N_POLYMERS ; ip++)
            {
                ipm = (ip-1) * param.N_MONOMERS;
                mj = monomer_lattice_pos[ipm+1];
                new_position_first_monomer[ip-1] = FCC_coordinates[mj];
            }
            record_position_first_monomer(old_position_first_monomer,
                                          new_position_first_monomer,
                                          record_pbc_shift,
                                          param.LATTICE_CONSTANT,
                                          param.Lx,
                                          param.Ly,
                                          param.Lz,
                                          param.N_POLYMERS);
            
            
            
            if ( iteration == time_steps_log[counter_step])
                
            {
                
                param.ss.str("");
                param.ss<<param.fileName;
                write_output_xyz(filePath + param.ss.str(),
                                 param,
                                 monomer_lattice_pos,
                                 FCC_coordinates,
                                 monomer_bond,
                                 nearest_neighbor_primitive,
                                 record_pbc_shift,
                                 counter_step);
                //                write_time_steps_opt(filePath + param.fileName_time_dy,
                //                                     time_steps_log,
                //                                     counter_step,
                //                                     N_sweep);
                param.ss.str("");
                param.ss<<"tree_output_dynamics"<<param.fileType;
                write_tree_output_dynamics (filePath + param.ss.str(),
                                            param,
                                            functionality_tree_nodes,
                                            tree_nodes_info,
                                            counter_step);
                
                
                energy_each_term_hamiltonian = Hamiltonian_function(tot_number_of_tree_nodes,
                            sum_functionality_1,
                            param.N_POLYMERS,
                            param.N_T,
                            param.N_MESH_POINTS,
                            param.BRANCH_POTENITIAL,
                            param.INTERACTION_POTENITIAL,
                            param.TREE_LENGTH_STIFFNESS,
                            energy_sum,
                            cell_tree_occupation);
                
                param.ss.str("");
                param.ss<<"Energy_dynamics"<<param.fileType;
                write_energy (filePath + param.ss.str(),
                                            energy_each_term_hamiltonian,
                                            counter_step);
                counter_step += 1;
                
            }
            iteration += 1;
            
            
        }

        //*************************************************write timing *********************************************
        double sim_duration_per_sec = (double)((clock() - tStart)/CLOCKS_PER_SEC);
        std::chrono::time_point<std::chrono::steady_clock> chronoSteadyClockEnd = chrono::steady_clock::now();
        std::chrono::time_point<std::chrono::system_clock> chronoSystemClockEnd = chrono::system_clock::now();
        
        
        auto chronoSteadyClockDiff = chronoSteadyClockEnd - chronoSteadyClockStart;
        int SteadyClockSecs  = std::chrono::duration_cast<std::chrono::seconds>(chronoSteadyClockDiff).count();
        
        auto chronoSystemClockDiff = chronoSystemClockEnd - chronoSystemClockStart;
        int SystemClockSecs = std::chrono::duration_cast<std::chrono::seconds>(chronoSystemClockDiff).count();
        
        cout<<"sim_duration_per_sec "<<sim_duration_per_sec<<endl;
        cout<<"SteadyClockSecs "<<SteadyClockSecs<<endl;
        cout<<"SystemClockSecs "<<SystemClockSecs<<endl;
        //*************************************************writing output files *********************************************
        number_occupied_cells = N_occupided_cell(param.N_MESH_POINTS,
                                                 param.N_CELL,
                                                 cell_occupation);
        lattice_density_sample = lattice_density (number_occupied_cells,
                                                  param.N_MESH_POINTS);
        
        
        lattice_chain_length_sample = lattice_chain_length (number_occupied_cells,
                                                            param.N_POLYMERS);
        
        cell_occupation_number = cell_occupation_density (number_occupied_cells,
                                                          param.TOTAL_N_MON);
        
        //        number_of_tree_nodes = tree_nodes_info.size();
        int n_f;
        number_of_tree_nodes =0;
        for (int i=0; i<tree_nodes_info.size();i++)
        {
            
            n_f = functionality_tree_nodes[i];
            if (n_f > 0)
            {
                number_of_tree_nodes +=1;
                vector_functionalities[n_f] += 1;
            }
            
        }
        
        
        param.ss.str("");
        param.ss<<param.fileName;
        //        write_output_xyz(filePath + param.ss.str(),
        //                         param,
        //                         monomer_lattice_pos,
        //                         FCC_coordinates,
        //                         monomer_bond,
        //                         nearest_neighbor_primitive,
        //                         record_pbc_shift,
        //                         counter_step);
        //        write_time_steps_opt(filePath + param.fileName_time_dy,
        //                             time_steps_log,
        //                             counter_step,
        //                             N_sweep);
        param.fs.str("");
        
        param.fs<<param.fileName_param<<param.fileType;
        write_simulation_info(filePath + param.fs.str(),
                              vector_functionalities,
                              vector_attempt_move_tree_with_functionality,
                              vector_accept_move_tree_with_functionality,
                              number_occupied_cells,
                              lattice_density_sample,
                              cell_occupation_number,
                              lattice_chain_length_sample,
                              accept_move_monomer_dy,
                              accept_move_two_mon_dy,
                              accept_move_tree_node_dy,
                              counter_one_mon_step,
                              counter_two_mon_step,
                              counter_tree_node_step,
                              number_of_tree_nodes,
                              param);
        
        param.fs.str("");
        param.fs<<"time_output"<<param.fileType;
        write_time_steps(filePath + param.fs.str(),
                         time_steps_log,
                         param.time_steps_output,
                         N_sweep);
        
        param.fs.str("");
        param.fs<<"tree_output"<<param.fileType;
        write_tree_output (filePath + param.fs.str(),
                           param,
                           functionality_tree_nodes,
                           tree_nodes_info
                           );
        //        param.fs.str("");
        //        param.fs<<"mesh_geometry"<<param.fileType;
        //        write_lattice_geometry(filePath + param.fs.str(),
        //                               FCC_coordinates,
        //                               param);
        param.fs.str("");
        param.fs<<"CPU_time"<<param.fileType;
        write_CPU_time (filePath + param.fs.str(),
                             sim_duration_per_sec,
                             SteadyClockSecs,
                             SystemClockSecs);
        
        copy_over_resume_backup(resumeFilePath);
        copy_over_RNGstate_backup(resumeFilePath);
        export_for_resume(resumeFilePath,
                          cell_occupation,
                          cell_tree_occupation,
                          monomer_lattice_pos,
                          previous_mon,
                          next_mon,
                          monomer_bond,
                          
                          where_monomer_is_on_tree,
                          lattice_position_tree_nodes,
                          
                          functionality_tree_nodes,
                          list_available_index_tree_nodes,
                          sum_functionality_1,
                          tot_number_of_tree_nodes,
                          monomer_belong_polymer,
                          
                          vector_functionalities,
                          vector_attempt_move_tree_with_functionality,
                          vector_accept_move_tree_with_functionality,
                          list_monomers_on_each_lattice_site,
                          tree_nodes_info,
                          old_position_first_monomer,
                          new_position_first_monomer,
                          record_pbc_shift,
                          time_phase_dyn,
                          delta_energy,
                          iteration,
                          counter_step,
                          accept_move_tree_node_dy,
                          accept_move_two_mon_dy,
                          accept_move_monomer_dy,
                          RNG
                          );
        
        
        
        //                Two_Monomer_Move (param.interaction_type,
        //                                  param.BRANCH_POTENITIAL,
        //                                  param.INTERACTION_POTENITIAL,
        //                                  param.TREE_LENGTH_STIFFNESS,
        //                                  param.N_T,
        //                                  param.FUNCTIONALITY,
        //                                  param.N_MONOMERS,
        //                                  param.N_MESH_POINTS,
        //                                  param.N_CELL,
        //                                  param.TOTAL_N_MON,
        //                                  sum_functionality_1,
        //                                  tot_number_of_tree_nodes,
        //                                  previous_mon,
        //                                  next_mon,
        //                                  monomer_lattice_pos,
        //                                  lattice_position_tree_nodes,
        //                                  cell_occupation,
        //                                  cell_tree_occupation,
        //                                  monomer_bond,
        //                                  possible_bending_moves,
        //                                  neighbors_mesh_points,
        //                                  bonds_forward,
        //                                  bonds_backward,
        //                                  list_monomers_on_each_lattice_site,
        //                                  opposite_primitive_vectors,
        //                                  where_monomer_is_on_tree,
        //                                  tree_nodes_info,
        //                                  functionality_tree_nodes,
        //                                  list_available_index_tree_nodes,
        //                                  accept_move_two_mon_dy,
        //                                  delta_energy,
        //                                  monomer_belong_polymer,
        //                                  RNG
        //                                  );
        
        
        
        
    }
    return 0;
}

