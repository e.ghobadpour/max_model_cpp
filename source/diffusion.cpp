//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "model_functions.hpp"
#include "Diffusion_functions.hpp"
#include "lattice_functions.hpp"

bool Check_Accept_Condition_Bonds_Tree_Node(int random_tree_node,
                                            int random_direction,
                                            vector<int> &monomer_bond,
                                            vector<int> &previous_mon,
                                            vector< vector <int> > &forward_bonds,
                                            vector< vector <int> > &backward_bonds,
                                            vector <vector <int> >  &tree_node_info)
{
    
    int counter_accept_bonds;
    int counter_bonds;
    int site_monomer;
    int bond_monomer;
    //    int previous_site_monomer;
    //    int previous_bond_monomer;
    counter_bonds = 0;
    counter_accept_bonds =0;
    for (int j = 0; j<tree_node_info[random_tree_node].size(); j++)
    {
        
        site_monomer = tree_node_info[random_tree_node][j];
        bond_monomer = monomer_bond[site_monomer];
        
        if (bond_monomer!=0)
        {
            counter_bonds +=1;
            
            if(forward_bonds[bond_monomer][random_direction] >= 0)
            {
                counter_accept_bonds +=1;
            }
        }
    }
    if (counter_accept_bonds == counter_bonds)
    {
        
        return true;
    }
    
    else
    {
        return false;
    }
    
}

bool Check_Interaction_Condition_Diffusion(string interaction,
                                           double potential_interaction,
                                           int ncell,
                                           int random_tree_node,
                                           int new_position_random_site,
                                           int random_lattice_site,
                                           vector<int> &icl,
                                           vector<int> &N_tree_node_on_lattice_sites,
                                           vector<int> &previous_mon,
                                           vector<int> &monomer_lattice_pos,
                                           vector <vector <int> >  &tree_node_info,
                                           double &delta_E,
                                           mt19937_64 &RNG
                                           )

{
    int site_monomer;
    int previous_monomer;
    int counter_neighbor;
    int H_int =0;
    int n_site_new;
    int n_site_old;
    int delta_initial = 0;
    int delta_final = 0;
    //    double Hamiltonian =0.0;
    
    if (interaction == "Soft_Excluded")
    {
        
        for (int i = 0; i<tree_node_info[random_tree_node].size(); i++)
        {
            
            site_monomer = tree_node_info[random_tree_node][i];
            previous_monomer = previous_mon[site_monomer];
            if (monomer_lattice_pos[previous_monomer] == new_position_random_site) {
                return false;
            }
        }

        n_site_new = N_tree_node_on_lattice_sites[new_position_random_site];
        n_site_old = N_tree_node_on_lattice_sites[random_lattice_site];
        
        if (n_site_new == 0)
        {
            delta_initial = (n_site_old-1)*(n_site_old-1);
        }
        else
        {
            delta_initial = (n_site_new-1)*(n_site_new-1) + (n_site_old-1)*(n_site_old-1);
        }
        
        if (n_site_old-1 == 0)
        {
            delta_final = (n_site_new)*(n_site_new);
        }
        else
        {
            delta_final = n_site_new*n_site_new + (n_site_old-2)*(n_site_old-2);
        }
        
        
        H_int = potential_interaction *((delta_final - delta_initial)/(double)2);
        //        Hamiltonian = exp(-H_int);
        return accept_move(RNG,H_int);
    }
    
    
    else if (interaction == "SAT")
    {
        counter_neighbor = 0;
        if (icl[new_position_random_site] == 0)
        {
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    else if (interaction == "Ideal")
    {
        if (icl[new_position_random_site] == 0)
        {
            return true;
            
        }
        else if (icl[new_position_random_site]>0 && icl[new_position_random_site]<ncell)
        {
            vector<int> temp;
            
            for (int i = 0; i<tree_node_info[random_tree_node].size(); i++)
            {
                
                site_monomer = tree_node_info[random_tree_node][i];
                previous_monomer = previous_mon[site_monomer];
                temp.push_back(monomer_lattice_pos[previous_monomer]);
            }
            if(find(temp.begin(), temp.end(), new_position_random_site) != temp.end())
            {
                return false;
            }
            else
            {
                
                return true;
            }
        }
        
        else
        {
            return false;
        }
    }
    else
    {
        string ErrorMessage="Input interaction is not defined. Available interactions: Ideal or SAT or Soft_Excluded";
        throw runtime_error(ErrorMessage);
    }
}

void Update_Tree_Node_Move(int random_tree_node,
                           int random_lattice_site,
                           int random_direction,
                           int new_position_random_site,
                           vector<int> &icl,
                           vector<int> &N_tree_node_on_lattice_sites,
                           vector<int> &monomer_lattice_pos,
                           vector<int> &previous_mon,
                           vector<int> &next_mon,
                           vector<int> &monomer_bond,
                           vector< vector <int> > &forward_bonds,
                           vector< vector <int> > &backward_bonds,
                           vector <int>   &lattice_sites_nodes,
                           vector<int> &tree_node_monomer_belongs,
                           vector <vector <int> >  &tree_node_info)
{
    int site_monomer;
    int bond_site_monomer;
    int previous_monomer;
    int previous_bond;
    
    for (int j = 0; j<tree_node_info[random_tree_node].size(); j++)
    {
        site_monomer = tree_node_info[random_tree_node][j];
        bond_site_monomer = monomer_bond[site_monomer];
        //        cout<<site_monomer<<" bond "<<bond_site_monomer<<" random_direction "<<random_direction<<endl;
        if (tree_node_monomer_belongs[site_monomer] != tree_node_monomer_belongs[next_mon[site_monomer]])
        {
            monomer_bond[site_monomer] = forward_bonds[bond_site_monomer][random_direction];
            
        }
        previous_monomer = previous_mon[site_monomer];
        if (tree_node_monomer_belongs[previous_monomer] != tree_node_monomer_belongs[site_monomer])
        {
            previous_bond = monomer_bond[previous_monomer];
            
            monomer_bond[previous_monomer] = backward_bonds[random_direction][previous_bond];
            
        }
    }
    for (int j = 0; j<tree_node_info[random_tree_node].size(); j++)
    {
        site_monomer = tree_node_info[random_tree_node][j];
        monomer_lattice_pos[site_monomer] = new_position_random_site;
    }
    icl[new_position_random_site] += tree_node_info[random_tree_node].size();
    icl[random_lattice_site] -= tree_node_info[random_tree_node].size();
    lattice_sites_nodes[random_tree_node]= new_position_random_site;
    N_tree_node_on_lattice_sites[new_position_random_site] +=1;
    N_tree_node_on_lattice_sites[random_lattice_site] -=1;
    
}

void  Tree_Node_Move (string interaction,
                      double potential_interaction,
                      int mesh_size,
                      int ncell,
                      vector<int> &icl,
                      vector<int> &N_tree_node_on_lattice_sites,
                      vector<int> &monomer_lattice_pos,
                      vector<int> &previous_mon,
                      vector<int> &next_mon,
                      vector<int> &monomer_bond,
                      vector <vector<int> > &list_nn,
                      vector< vector <int> > &forward_bonds,
                      vector< vector <int> > &backward_bonds,
                      vector<vector<int>> &sites_info,
                      vector <vector <int> >  &tree_node_info,
                      vector <int>   &lattice_sites_nodes,
                      vector<int> &tree_node_monomer_belongs,
                      vector<int> &functionality_tree_node,
                      long int &acceptance_rate_Nt,
                      double &delta_E,
                      vector<long int> &vector_attempt_move_tree_with_functionality,
                      vector<long int> &vector_accept_move_tree_with_functionality,
                      mt19937_64 &RNG)
{
    int random_tree_node;
    int random_lattice_site;
    int random_direction;
    int new_position_random_site;
    int functionality_randome_tree_node;
    
    random_tree_node = RNG() % tree_node_info.size();
    while (tree_node_info[random_tree_node].size()==0)
    {
        random_tree_node = RNG() % tree_node_info.size();
    }
    
    
    functionality_randome_tree_node = functionality_tree_node[random_tree_node];
    vector_attempt_move_tree_with_functionality[functionality_randome_tree_node]+=1;
    
    
    random_lattice_site = lattice_sites_nodes[random_tree_node];
    random_direction = (RNG() % 12) + 1;
    new_position_random_site = list_nn[random_lattice_site][random_direction];
    if (new_position_random_site >= 0)
    {
        
        if (Check_Accept_Condition_Bonds_Tree_Node(random_tree_node,
                                                   random_direction,
                                                   monomer_bond,
                                                   previous_mon,
                                                   forward_bonds,
                                                   backward_bonds,
                                                   tree_node_info)
            )
        {
            
            
            if (Check_Interaction_Condition_Diffusion(interaction,
                                                      potential_interaction,
                                                      ncell,
                                                      random_tree_node,
                                                      new_position_random_site,
                                                      random_lattice_site,
                                                      icl,
                                                      N_tree_node_on_lattice_sites,
                                                      previous_mon,
                                                      monomer_lattice_pos,
                                                      tree_node_info,
                                                      delta_E,
                                                      RNG)
                )
            {
                
                Update_Tree_Node_Move(random_tree_node,
                                      random_lattice_site,
                                      random_direction,
                                      new_position_random_site,
                                      icl,
                                      N_tree_node_on_lattice_sites,
                                      monomer_lattice_pos,
                                      previous_mon,
                                      next_mon,
                                      monomer_bond,
                                      forward_bonds,
                                      backward_bonds,
                                      lattice_sites_nodes,
                                      tree_node_monomer_belongs,
                                      tree_node_info);
                
                acceptance_rate_Nt+=1;
                vector_accept_move_tree_with_functionality[functionality_randome_tree_node]+=1;
            }
            
        }
    }
    
}

