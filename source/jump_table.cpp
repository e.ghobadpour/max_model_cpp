//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "lattice_functions.hpp"




vector<vector<int> > list_backward_bonds ( int number_neighbors,
                                          vector< vector <int>> primary_vectors)
{
    // This function returns the direction of the bond from the primary_vector
    vector<vector<int>> backward_bonds(number_neighbors+1, vector<int> (number_neighbors+1, -1));
    int n_x,n_y,n_z;
    
    for (int nc=0; nc<number_neighbors+1;nc++)
    {
        for (int nn=0; nn<number_neighbors+1;nn++)
        {
            n_x = primary_vectors[nc][0] + primary_vectors[nn][0];
            n_y = primary_vectors[nc][1] + primary_vectors[nn][1];
            n_z = primary_vectors[nc][2] + primary_vectors[nn][2];
            for (int k=0; k < number_neighbors+1;k++)
            {
                if ( primary_vectors[k][0] == n_x && primary_vectors[k][1] == n_y && primary_vectors[k][2] == n_z)
                {
                    backward_bonds[nc][nn] = k;
                    break;
                    
                }
            }
            
        }
        
    }
    
    
    //    for (int nn = 0 ; nn < number_neighbors+1 ; nn++)
    //    {
    //        for (int nc = 0 ;nc < number_neighbors+1 ; nc++)
    //        {
    //                if (backward_bonds[nc][nn]>-1)
    //                {
    //                    cout<<"nn "<<nn<<" nc "<<nc<<" backward_move "<<backward_bonds[nc][nn]<<endl;
    //                }
    //        }
    //    }
    
    
    
    return  backward_bonds;
}



vector<vector<int> > list_forward_bonds ( int number_neighbors,
                                         vector< vector <int>> primary_vectors)
{
    // This function returns the direction of the bond into the primary_vector
    vector<vector<int>> forward_bonds(number_neighbors+1, vector<int> (number_neighbors+1, -1));
    int n_x,n_y,n_z;
    
    for (int nc=0; nc<number_neighbors+1;nc++)
    {
        for (int nn=0; nn<number_neighbors+1;nn++)
        {
            n_x = primary_vectors[nc][0] - primary_vectors[nn][0];
            n_y = primary_vectors[nc][1] - primary_vectors[nn][1];
            n_z = primary_vectors[nc][2] - primary_vectors[nn][2];
            for (int k=0; k < number_neighbors+1;k++)
            {
                if ( primary_vectors[k][0] == n_x && primary_vectors[k][1] == n_y && primary_vectors[k][2] == n_z){
                    forward_bonds[nc][nn] = k;
                    break;
                    
                }
            }
        }
        
    }
    
    return  forward_bonds;
}








vector <vector<vector<int>> > reptation_hairpin_moves ( int number_neighbors,
                                                       vector< vector <int>> forward_bonds,
                                                       vector< vector <int>> backward_bonds
                                                       )
{
    // This function retruns a table of all possible moves for each pair of backward and forward bonds.
    int nnef;
    //    int nn;
    vector<vector<vector<int>>> move_table(number_neighbors+1,vector<vector<int>>(number_neighbors+1,vector<int>(number_neighbors+1,-1)));
    for (int nc = 0 ; nc < number_neighbors+1 ; nc++)
    {
        for (int nc2 = 0 ; nc2<number_neighbors+1 ; nc2++)
        {
            nnef=0;
            for (int nn = 1 ;nn < number_neighbors+1 ; nn++)
            {
                if ((nc == 0 && nc2 == 0) ||
                    (backward_bonds[nc][nn] == 0 && nn == nc2) ||
                    (nc == 0 && forward_bonds[nc2][nn] == 0) ||
                    (nc2 == 0 && backward_bonds[nc][nn] == 0))
                {
                    nnef += 1;
                    move_table[nc][nnef][nc2] = nn;
                }
            }
        }
    }
    
    
    //    for (int nc = 0 ; nc < number_neighbors+1 ; nc++)
    //    {
    //        for (int nc2 = 0 ;nc2 < number_neighbors+1 ; nc2++)
    //        {
    //            for (int nn = 0 ;nn < number_neighbors+1 ; nn++)
    //            {
    //                if (move_table[nc][nn][nc2]>-1)
    //                {
    //
    //                    cout<<"nc "<<nc<<" nc2 "<<nc2<<" nn "<<nn<<" bending_move "<<move_table[nc][nn][nc2]<<endl;
    //                }
    //            }
    //        }
    //    }
    
    return  move_table;
}





vector <vector<vector<int>> > bending_moves ( int number_neighbors,
                                             vector< vector <int>> forward_bonds,
                                             vector< vector <int>> backward_bonds,
                                             vector <int> opposite_primitive_vectors)
{
    // This function retruns a table of all possible moves for each pair of backward and forward bonds.
    //    int nnef;
    vector<vector<vector<int>>> move_table(number_neighbors+1,vector<vector<int>>(number_neighbors+1,vector<int>(number_neighbors+1,-1)));
    for (int b1 = 0 ; b1 < number_neighbors+1 ; b1++)
    {
        for (int b2 = 0 ; b2<number_neighbors+1 ; b2++)
        {
            //            nnef=0;
            for (int nn = 1 ;nn < number_neighbors+1 ; nn++)
            {
                //                if ((b1 == 0 && b2!=0 && forward_bonds[b2][nn] != -1) ||
                //                    (b2 == 0 && b1!=0 && backward_bonds[b1][nn] != -1) ||
                //                  (b1!=0 && b2!=0 && b1!=b2 && backward_bonds[b1][nn] != -1 && nn == b2) ||
                //                    (b1!=0 && b2!=0 && b1!=b2 && forward_bonds[b2][nn] != -1 && nn == opposite_primitive_vectors[b1]) ||
                //                    (b1!=0 && b2!=0 && forward_bonds[b2][b1] == nn)
                //                     )
                if ((b1 == 0 && b2!=0 && forward_bonds[b2][nn] != -1) ||
                    (b2 == 0 && b1!=0 && backward_bonds[b1][nn] != -1) ||
                    (b1!=0 && b2!=0 && backward_bonds[b1][nn] != -1 && forward_bonds[b2][nn] != -1)
                    )
                    
                {
                    //                    nnef += 1;
                    move_table[b1][nn][b2] = nn;
                    
                }
            }
        }
    }
    //    for (int nc = 0 ; nc < number_neighbors+1 ; nc++)
    //    {
    //        for (int nc2 = 0 ;nc2 < number_neighbors+1 ; nc2++)
    //        {
    //            for (int nn = 1 ;nn < number_neighbors+1 ; nn++)
    //            {
    //                if (move_table[nc][nn][nc2] != -1)
    //                {
    //
    //                    cout<<"b1 "<<nc<<" b2 "<<nc2<<" nn "<<nn<<" bending_move "<<move_table[nc][nn][nc2]<<endl;
    //                }
    //            }
    //        }
    //    }
    return  move_table;
}




