//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "lattice_functions.hpp"
#include <fstream> //For streaming data to the output file
#include <sstream>
#include <boost/filesystem.hpp>
#include "parameters.hpp"
#include <math.h>
#include <vector>
//#include<algorithm>
#include "arg_parse_functions.hpp"
#include <random>
#include "output_functions.hpp"



void create_path(string path)
{
    boost::filesystem::path pTrajectory(path);
    if(!boost::filesystem::exists(pTrajectory))
    {
        boost::filesystem::create_directories(pTrajectory);
    }
}

string assign_dir(General_Parameters &param,int isamp)

{
    
    string path = param.output_directory;
    create_path(path);
    path += "/" + param.boundary;
    create_path(path);
//    path += "/l_ij_dyn";
//    create_path(path);
    path += "/move_" + param.move;
    create_path(path);
    path += "/tree_functionality_" + to_string(param.FUNCTIONALITY);
    create_path(path);
    path += "/Box_size_" + to_string(param.Lx)+"_"+to_string(param.Ly)+"_"+to_string(param.Lz);
    create_path(path);
    path += "/Intercation_potential_" + to_string(param.INTERACTION_POTENITIAL);
    create_path(path);
    path += "/Branch_potential_" + to_string(param.BRANCH_POTENITIAL);
    create_path(path);
    path += "/chain_l_stiffness_" + to_string(param.TREE_LENGTH_STIFFNESS);
    create_path(path);
    path += "/alpha_" + to_string(param.DENSITY_T);
    create_path(path);
    path += "/np_" + to_string(param.N_POLYMERS);
    create_path(path);
    path += "/nm_" + to_string(param.N_MONOMERS);
    create_path(path);
    path += "/mixing_" + to_string(param.mixing);
    create_path(path);
    path += "/seed_" + to_string(param.seed_sample+isamp);
    return path + "/" ;
}


void    record_position_first_monomer(vector<vector<int> > &old_position,
                                      vector<vector<int> > &current_position,
                                      vector<vector<int> > &pbc_shift,
                                      int lattice_constant,
                                      int lx,
                                      int ly,
                                      int lz,
                                      int np)
{
    int dr;
    vector<int> size_box = {lx*lattice_constant,ly*lattice_constant,lz*lattice_constant};
    for (int ip = 1 ; ip <= np ; ip++)
    {
        for (int i = 0; i<3; i++)
        {
            dr = current_position[ip-1][i] - old_position[ip-1][i];
            if (abs(dr) > float(size_box[i])/2)
            {
                if (dr>0)
                {
                    pbc_shift[ip-1][i] -=1;
                }
                else
                {
                    pbc_shift[ip-1][i] +=1;
                }
            }
        }
    }
    
    
    old_position = current_position;
}

void write_tree_output (string outputfile,
                        General_Parameters &param,
                        vector <int> functionality_tree_nodes,
                        vector <vector <int> > tree_nodes_info
                        )
{
    ofstream out;
    out.open(outputfile.c_str(),ios_base::app);
    out<<"functionality monomers"<<endl;
    for (int i=0; i<tree_nodes_info.size();i++)
    {
        
        if(functionality_tree_nodes[i]>0  && functionality_tree_nodes[i]<= param.FUNCTIONALITY)
        {
            out<<functionality_tree_nodes[i]<<" ";
            for (int j=0; j<tree_nodes_info[i].size(); j++)
            {
                if (tree_nodes_info[i][j]>param.TOTAL_N_MON)
                {
                    cout<<"the tree node output has a problem";
                    exit(2);
                }
                out<<tree_nodes_info[i][j]<<" ";
            }
            out<<endl;
        }
    }
    
    out.close();
}

void write_tree_output_dynamics (string outputfile,
                        General_Parameters &param,
                        vector <int> functionality_tree_nodes,
                        vector <vector <int> > tree_nodes_info,
                        long int time
                        )
{
    ofstream out;
    out.open(outputfile.c_str(),ios_base::app);
    out<<"# "<<time<<endl;
    out<<"functionality monomers"<<endl;
    for (int i=0; i<tree_nodes_info.size();i++)
    {
        if(functionality_tree_nodes[i]>0  && functionality_tree_nodes[i]<= param.FUNCTIONALITY)
        {
            out<<functionality_tree_nodes[i]<<" ";
            for (int j=0; j<tree_nodes_info[i].size(); j++)
            {
                if (tree_nodes_info[i][j]>param.TOTAL_N_MON)
                {
                    cout<<"the tree node output has a problem";
                    exit(2);
                }
                out<<tree_nodes_info[i][j]<<" ";
            }
            out<<endl;
        }
    }
    
    out.close();
}

void write_energy(string outputfile,
                  vector<double> &energy_output,
                        long int time
                        )
{
    ofstream out;
    out.open(outputfile.c_str(),ios_base::app);
    out<<"time_step  "<<time<<endl;
    out<<"H_br       "<< energy_output[0]<<endl;
    out<<"H_int      "<< energy_output[1]<<endl;
    out<<"H_cl       "<< energy_output[2]<<endl;
    out<<"energy     "<< energy_output[3]<<endl;
//    for (int i=0; i<energy_output.size();i++)
//    {
//
//            out<<energy_output[i]<<" ";
//            out<<endl;
//    }
    out.close();
}



void write_output_xyz (string outputfile,
                       General_Parameters &param,
                       vector<int> monomer_lattice_pos,
                       vector<vector<int> >  FCC_coordinates,
                       vector<int> monomer_bond,
                       vector<vector<int> > nearest_neighbor_primitive,
                       vector<vector<int> > &pbc_shift,
                       long int time)
{
    //save  monomers's coordinates as .xyz output file
    vector<vector<int>> real_pos(param.TOTAL_N_MON+1, vector<int> (param.DIMENSION, 0));
    int mj,bond,ipm;
    //    int dx,dy,dz;
    ofstream out;
    out.open(outputfile.c_str(),ios_base::app);
    out<<param.TOTAL_N_MON<<endl;
    out<<"# "<<time<<endl;
    for (int ip = 1 ; ip <= param.N_POLYMERS ; ip++)
    {
        ipm = (ip-1) * param.N_MONOMERS;
        mj = monomer_lattice_pos[ipm+1];
        real_pos[ipm+1][0] = FCC_coordinates[mj][0] + pbc_shift[ip-1][0]*param.Lx*param.LATTICE_CONSTANT;
        real_pos[ipm+1][1] = FCC_coordinates[mj][1] + pbc_shift[ip-1][1]*param.Ly*param.LATTICE_CONSTANT;
        real_pos[ipm+1][2] = FCC_coordinates[mj][2] + pbc_shift[ip-1][2]*param.Lz*param.LATTICE_CONSTANT;
        
        out <<"poly"<< ip << "\t" << real_pos[ipm+1][0] << "\t" << real_pos[ipm+1][1]<< "\t" << real_pos[ipm+1][2] << endl;
        for (int i = 2 ; i < param.N_MONOMERS+1 ; i++)
        {
            bond=monomer_bond[ipm+i-1];
            if (bond==-1)
            {
                cout<<ipm+i-1<<endl;
            }
            real_pos[ipm+i][0] = real_pos[ipm+i-1][0] + nearest_neighbor_primitive[bond][0];
            real_pos[ipm+i][1] = real_pos[ipm+i-1][1] + nearest_neighbor_primitive[bond][1];
            real_pos[ipm+i][2] = real_pos[ipm+i-1][2] + nearest_neighbor_primitive[bond][2];
            out <<"poly"<< ip << "\t" << real_pos[ipm+i][0] << "\t" << real_pos[ipm+i][1]<< "\t" << real_pos[ipm+i][2] << "\t" << endl;
        }
    }
    out.close();
}


int N_occupided_cell (int tot_mesh_points,
                      int N_CELL,
                      vector<int> &icl)
{
    int counter_occupided = 0;
    for (int i=0 ; i < tot_mesh_points ; i++){
        if ( icl[i]> 0 && icl[i]<= N_CELL)
        {
            counter_occupided +=1;
        }
        
    }
    return counter_occupided;
}

float lattice_density (int N_cell_occupided,
                       int tot_mesh_points)
{
    
    return  (float)N_cell_occupided/(float)tot_mesh_points ;
}

float lattice_chain_length (int N_cell_occupided,
                            int Np)
{
    
    return  (float)N_cell_occupided/(float)Np ;
}
float cell_occupation_density (int N_cell_occupided,
                               int TOTAL_N_MON)
{
    //    cout<<"N_cell_occupided"<<" "<<N_cell_occupided<<" "<<"TOTAL_N_MON"<<" "<<TOTAL_N_MON<<endl;
    return (float)TOTAL_N_MON/(float)N_cell_occupided ;
}



void write_simulation_info (string outputfile,
                            vector<int> vector_functionalities,
                            vector<long int> vector_attempt_move_tree_with_functionality,
                            vector<long int> vector_accept_move_tree_with_functionality,
                            int n_occupied_cells,
                            float rho_lattice,
                            float rho_cell,
                            float chain_length,
                            long int accept_move_monomer_dy,
                            long int accept_move_two_mon_dy,
                            long int accept_move_site_dy,
                            long int counter_number_one_mon_move,
                            long int counter_number_two_mons_move,
                            long int counter_site_step,
                            int number_of_tree_nodes,
                            General_Parameters &param)
{
    
    ofstream out;
    out.open(outputfile.c_str());
    
    out <<"FCC geometry"<< endl;
    out << string( 2, '\n' );
    
    out <<"parameters:"<< endl;
    out << string( 1, '\n' );
    out <<"monomers per polymer"<<"             " <<param.N_MONOMERS << endl;
    out <<"Number of polymers"<<"               " <<param.N_POLYMERS << endl;
    out <<"Total number of monomers"<<"         " <<param.TOTAL_N_MON << endl;
    out <<"Max. monomers per cell"<<"           " <<param.N_CELL << endl;
    out<<"Max functionality of tree nodes"<<"   " <<param.FUNCTIONALITY<<endl;
    out<<"move type"<<"                         "<<param.move<<endl;
    if (param.move =="mixed")
    {
        out<<"probability mixing move types"<<"    "<<param.mixing<<endl;
    }
    else
    {
        out<<"probability mixing move types"<<"    "<<"not defined"<<endl;
    }
    out << string( 3, '\n' );
    
    out <<"geometry:"<< endl;
    out << string( 1, '\n' );
    out <<"boundary:"<<"                        " <<param.boundary << endl;
    out <<"dimensions:"<<"                      " <<param.DIMENSION << endl;
    out <<"BOX size: Lx,Ly,Lz"<<"               " <<param.Lx <<" "<<param.Ly <<" "<<param.Lz <<endl;
    out <<"Lattice constant"<<"                 " <<param.LATTICE_CONSTANT << endl;
    out <<"# mesh points,Lx*Ly*Lz*4"<<"         " <<param.N_MESH_POINTS << endl;
    out <<"Number of nearest neighbors"<<"      " <<param.N_NEIGHBORS<< endl;
    out << string( 3, '\n' );
    
    out <<"Hamiltonian:"<< endl;
    out << string( 1, '\n' );
    out <<"chemical potential branching"<<"               " <<param.BRANCH_POTENITIAL << endl;
    out <<"chemical potential interaction"<<"             " <<param.INTERACTION_POTENITIAL << endl;
    out <<"chemical potential tree length(stiffness)"<<"  " <<param.TREE_LENGTH_STIFFNESS << endl;
    out <<"density tree nodes"<<"                          " <<param.DENSITY_T  << endl;
    out << string( 3, '\n' );
    
    
    out <<"simulation times:"<< endl;
    out << string( 1, '\n' );
    out <<"t: growth"<<"                        " <<param.MC_GROW << endl;
    out <<"t: equilibrium"<<"                   " <<param.MC_EQUIB << endl;
    out <<"t: dynamics"<<"                      " <<param.MC_DYN << endl;
    
    out << string( 3, '\n' );
    out <<"number of occupied cells"<<"         " << n_occupied_cells<< endl;
    out <<"lattice density"<<"                  " << rho_lattice<< endl;
    out <<"cell occupation"<<"                  " << rho_cell<< endl;
    out <<"lattice chain length"<<"             " <<chain_length<< endl;
    out <<"tree size"<<"                        " <<number_of_tree_nodes<<endl;
    for (int i=1; i<=param.N_NEIGHBORS; i++)
    {
        out <<"tree nodes with functionality "<<i<<"  " <<vector_functionalities[i]<<endl;
    }
    
    out << string( 3, '\n' );
    out <<"accept_number_one_mon_move"<<"      " <<accept_move_monomer_dy<< endl;
    out <<"accept_number_two_mons_move"<<"     " <<accept_move_two_mon_dy<< endl;
    out <<"accept_number_tree_node_move"<<"    " <<accept_move_site_dy<< endl;
    out <<"counter_number_one_mon_move"<<"     " <<counter_number_one_mon_move<< endl;
    out <<"counter_number_two_mons_move"<<"    " <<counter_number_two_mons_move<< endl;
    out <<"counter_number_tree_node_move"<<"   " <<counter_site_step<< endl;
    out <<"acceptance_rate_one_monomer"<<"     " <<double(accept_move_monomer_dy)/counter_number_one_mon_move<< endl;
    out <<"acceptance_rate_two_monomers"<<"    " <<double(accept_move_two_mon_dy)/counter_number_two_mons_move<< endl;
    out <<"acceptance_rate_tree_nodes"<<"      " <<double(accept_move_site_dy)/counter_site_step<<endl;
    for (int i=1; i<=param.N_NEIGHBORS; i++)
    {
        out<<"accetance_rate  functionality "<<i<<" " <<double(vector_accept_move_tree_with_functionality[i])/vector_attempt_move_tree_with_functionality[i]<<endl;
    }
    
}

void write_CPU_time (string outputfile,
                     double sim_duration_per_second,
                     int Steady_Clock_Second,
                     int System_Clock_Second)
{
    ofstream out;
    out.open(outputfile.c_str(),ios_base::app);
    out<<"sim_duration in second      "<<sim_duration_per_second<<endl;
    out<<"Steady Clock in Second      "<<Steady_Clock_Second<<endl;
    out<<"System Clock in Second      "<<System_Clock_Second<<endl;
    out.close();
}

void write_time_steps (string outputfile,
                       vector<long int> time_steps,
                       int number_steps,
                       int sweep)
{
    ofstream out;
    out.open(outputfile.c_str());
    
    out <<"count time_sweep time_counter"<< endl;
    for (int i=0; i< time_steps.size(); ++i)
    {
        //        out<<i+1<<" "<<time_steps[i]<<" "<<float(time_steps[i])/sweep<<endl;
        out<<i<<" "<<sweep<<" "<<time_steps[i]<<endl;
    }
}

void write_time_steps_opt (string outputfile,
                           vector<long int> time_steps,
                           int step_number,
                           int sweep)
{
    ofstream out;
    out.open(outputfile.c_str(),ios_base::app);
    out<<step_number<<" "<<sweep<<" "<<time_steps[step_number]<<endl;
    out.close();
}




void write_tree_connectivity(string outputfile,
                             vector <vector <int> >  &nodes_tree)
{
    ofstream out;
    out.open(outputfile.c_str());
    out<<"nodes_tree.size() "<<nodes_tree.size()<<endl;
    for (int i=0; i<nodes_tree.size(); i++) {
        out<<"tree_nod_info_in thefunction "<<i<<endl;
        for (int j=0; j<nodes_tree[i].size(); j++) {
            out<<nodes_tree[i][j]<<" ";
        }
        out<<endl;
    }
    out.close();
}



void write_lattice_geometry(string outputfile,
                            vector<vector<int> > FCC_coordinates,
                            General_Parameters &param)
{
    ofstream out;
    out.open(outputfile.c_str());
    for (int i = 0; i<param.N_MESH_POINTS ; i++)
    {
        out << "\t" << FCC_coordinates[i][0] << "\t" << FCC_coordinates[i][1]<< "\t" << FCC_coordinates[i][2] << "\t" << endl;
    }
    
}


void write_initial_parameters(General_Parameters &param,
                              string seed_path)
{
    
    //    string project_path = assign_dir(param,-1);
    //    project_path.erase(project_path.end()-8,project_path.end());
    //    create_path(project_path);
    //    project_path += "_init.txt";
    
    string outputfile = seed_path +"resume_init.txt";
    ofstream out;
    out.open(outputfile.c_str());
    
    
    out<<" --constant "+to_string(param.LATTICE_CONSTANT);
    out<<" --lx "+to_string(param.Lx);
    out<<" --ly "+to_string(param.Ly);
    out<<" --lz "+to_string(param.Lz);
    out<<" --boundary "+ param.boundary;
    out<<" --functionality "+to_string(param.FUNCTIONALITY);
    out<<" --ncell "+to_string(param.N_CELL);
    out<<" --interactionType "+ param.interaction_type;
    out<<" --branchpotenitial "+to_string(param.BRANCH_POTENITIAL);
    out<<" --interactionpotenitial "+to_string(param.INTERACTION_POTENITIAL);
    out<<" --stiffnesslengthtree "+to_string(param.TREE_LENGTH_STIFFNESS);
    out<<" --densitytreenode "+to_string(param.DENSITY_T);
    
    out<<" --MCmove "+param.move;
    out<<" --mixing "+to_string(param.mixing);
    
    //    out<<" --interactionReptation "+ param.interaction_reptation;
//    out<<" --treeConnectivity "+ param.connectivity;
    //    out<<" --branchprob "+to_string(param.BRANCH_PROB);
    
    
    out<<" --monomer "+to_string(param.N_MONOMERS);
    out<<" --polymer "+to_string(param.N_POLYMERS);
    out<<" --timgrow "+to_string(param.MC_GROW);
    out<<" --timequib "+to_string(param.MC_EQUIB);
    out<<" --timdyn "+to_string(param.MC_DYN);
    out<<" --resumefreq "+to_string(param.resume_save_freq);
    out<<" --sample "+to_string(param.nsamp);
    out<<" --seed "+to_string(param.seed_sample);
    out<<" --projectName "+   param.project_name;
    out.close();
    
}
void write_bin_vec_int(ofstream &writer,
                       vector<int> &vec
                       )
{
    int size_of_vec = vec.size();
    writer.write(reinterpret_cast<const char*>(&size_of_vec), sizeof size_of_vec);
    writer.write(reinterpret_cast<const char*>(&vec[0]), size_of_vec*sizeof(int));
    
}

void write_bin_vec_long_int(ofstream &writer,
                       vector<long int> &vec
                       )
{
    int size_of_vec = vec.size();
    writer.write(reinterpret_cast<const char*>(&size_of_vec), sizeof size_of_vec);
    writer.write(reinterpret_cast<const char*>(&vec[0]), size_of_vec*sizeof(long int));
    
}


void write_bin_vec_vec_int(ofstream &writer,
                           vector<vector<int>> &vec
                           )
{
    int size_of_vec_dim_0 = vec.size();
    writer.write(reinterpret_cast<const char*>(&size_of_vec_dim_0), sizeof size_of_vec_dim_0);
    for (int i=0; i<size_of_vec_dim_0; i++) {
        write_bin_vec_int(writer, vec[i]);
    }
}



vector<int> read_bin_vec_int(ifstream &reader )
{
    
    int size_of_vec;
    int read_temp;
    reader.read(reinterpret_cast<char*>(&read_temp), sizeof(int));
    size_of_vec = read_temp;
    vector<int> vec(size_of_vec);
    reader.read(reinterpret_cast<char*>(&vec[0]), size_of_vec*sizeof(int));
    return vec;
}

vector<long int> read_bin_vec_long_int(ifstream &reader )
{
    
    int size_of_vec;
    int read_temp;
    reader.read(reinterpret_cast<char*>(&read_temp), sizeof(long int));
    size_of_vec = read_temp;
    vector<long int> vec(size_of_vec);
    reader.read(reinterpret_cast<char*>(&vec[0]), size_of_vec*sizeof(long int));
    return vec;
}

vector<vector<int>> read_bin_vec_vec_int(ifstream &reader)
{
    vector<vector<int>> return_vec;
    int size_of_vec_dim_0;
    int read_temp;
    reader.read(reinterpret_cast<char*>(&read_temp), sizeof(int));
    size_of_vec_dim_0 = read_temp;
    for (int i=0; i<size_of_vec_dim_0; i++) {
        vector<int> temp_vec = read_bin_vec_int(reader);
        return_vec.push_back(temp_vec);
    }
    
    return return_vec;
}
void export_for_resume(string resumeFilePath,
                       vector<int> &icl,
                       vector<int> &Number_Nt_on_lattice_sites,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       vector<int> &monomer_tree_position,
                       vector<int> &lattice_sites_nodes,
                       vector<int> &functionality_tree_node,
                       vector<int> &list_available_index,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       vector<int> &monomer_belong_polymer,
                       vector<int> &vector_functionalities,
                       vector<long int> &vector_attempt_move_tree_with_functionality,
                       vector<long int> &vector_accept_move_tree_with_functionality,
                       vector<vector<int>> &sites_info,
                       vector<vector<int>> &tree_info,
                       vector<vector<int> > &old_position,
                       vector<vector<int> > &current_position,
                       vector<vector<int> > &pbc_shift,
                       double &time_phase_dyn,
                       double &delta_E,
                       long int &iteration,
                       int &counter_step,
                       long int &accept_tree_node_dy,
                       long int &accept_pair_move_dy,
                       long int &accept_one_mon_dy,
                       mt19937_64 &RNG
                       )
{
    
    //    cout<<resumeFilePath<<endl;
    //    string buffResumeFilePath = resumeFilePath + "_buff";
    ofstream writebuff(resumeFilePath.c_str(), ios::out|ios::binary);
    
    write_bin_vec_int(writebuff, icl);
    write_bin_vec_int(writebuff, Number_Nt_on_lattice_sites);
    write_bin_vec_int(writebuff, monomer_lattice_pos);
    write_bin_vec_int(writebuff, previous_mon);
    write_bin_vec_int(writebuff, next_mon);
    write_bin_vec_int(writebuff, monomer_bond);
    write_bin_vec_int(writebuff, monomer_tree_position);
    write_bin_vec_int(writebuff, lattice_sites_nodes);
    write_bin_vec_int(writebuff, functionality_tree_node);
    write_bin_vec_int(writebuff, list_available_index);
    write_bin_vec_int(writebuff, sum_f_1);
    write_bin_vec_int(writebuff, sum_N_t);
    write_bin_vec_int(writebuff, monomer_belong_polymer);
    write_bin_vec_int(writebuff, vector_functionalities);
    write_bin_vec_long_int(writebuff, vector_attempt_move_tree_with_functionality);
    write_bin_vec_long_int(writebuff, vector_accept_move_tree_with_functionality);
    
    write_bin_vec_vec_int(writebuff, sites_info);
    write_bin_vec_vec_int(writebuff, tree_info);
    write_bin_vec_vec_int(writebuff, old_position);
    write_bin_vec_vec_int(writebuff, current_position);
    write_bin_vec_vec_int(writebuff, pbc_shift);
    
    writebuff.write(reinterpret_cast<const char*>(&time_phase_dyn), sizeof(time_phase_dyn));
    writebuff.write(reinterpret_cast<const char*>(&delta_E), sizeof(delta_E));
    
    writebuff.write(reinterpret_cast<const char*>(&iteration), sizeof(long int));
    writebuff.write(reinterpret_cast<const char*>(&counter_step), sizeof(int));
    writebuff.write(reinterpret_cast<const char*>(&accept_tree_node_dy), sizeof(long int));
    writebuff.write(reinterpret_cast<const char*>(&accept_pair_move_dy), sizeof(long int));
    writebuff.write(reinterpret_cast<const char*>(&accept_one_mon_dy), sizeof(long int));
    writebuff.close();
    
    
    
    string RNGpath = resumeFilePath;
    string first_backup = resumeFilePath;
    first_backup.erase(first_backup.begin(), first_backup.end()-6);
    if (first_backup == "backup") {
        RNGpath.erase(RNGpath.end()-(4+7), RNGpath.end());
        RNGpath+="RNGstate_backup";
    } else {
        RNGpath.erase(RNGpath.end()-4, RNGpath.end());
        RNGpath+="RNGstate";
    }
    
    
    
    ofstream writerng(RNGpath.c_str(), ios::out|ios::binary);
    writerng<<RNG;
    writerng.close();
    
    
    
}

void import_for_resume(string resumeFilePath,
                       vector<int> &icl,
                       vector<int> &Number_Nt_on_lattice_sites,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       
                       vector<int> &monomer_tree_position,
                       vector <int> &lattice_sites_nodes,
                       
                       vector<int> &functionality_tree_node,
                       vector<int> &list_available_index,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       vector<int> &monomer_belong_polymer,
                       
                       vector<int> &vector_functionalities,
                       vector<long int> &vector_attempt_move_tree_with_functionality,
                       vector<long int> &vector_accept_move_tree_with_functionality,
                       vector<vector<int>> &sites_info,
                       vector<vector<int>> &tree_info,
                       vector<vector<int> > &old_position,
                       vector<vector<int> > &current_position,
                       vector<vector<int> > &pbc_shift,
                       double &time_phase_dyn,
                       double &delta_E,
                       long int &iteration,
                       int &counter_step,
                       long int &accept_tree_node_dy,
                       long int &accept_pair_move_dy,
                       long int &accept_one_mon_dy,
                       mt19937_64 &RNG
                       
                       )
{
    string correctPath;
    ifstream temp_reader(resumeFilePath.c_str(), ios::in|ios::binary);
    if (!temp_reader.is_open()) {
        string warning_message = "Warning: No resume binary buffer file available to read.\nWill try the backup.\n";
        cout<<warning_message<<endl;
        string backup_path = resumeFilePath + "_backup";
        ifstream reader_backup(backup_path.c_str(), ios::in|ios::binary);
        if (!reader_backup.is_open()) {
            string error_message = "Error: Both resume binary and its backup file are not available.\n";
            throw std::runtime_error(error_message);
        } else {
            correctPath = backup_path;
        }
        
    } else{
        correctPath = resumeFilePath;
    }
    
    
    ifstream reader(correctPath.c_str(), ios::in|ios::binary);
    
    icl.clear();
    icl = read_bin_vec_int(reader);
    Number_Nt_on_lattice_sites.clear();
    Number_Nt_on_lattice_sites = read_bin_vec_int(reader);
    monomer_lattice_pos.clear();
    monomer_lattice_pos = read_bin_vec_int(reader);
    
    previous_mon.clear();
    previous_mon = read_bin_vec_int(reader);
    next_mon.clear();
    next_mon = read_bin_vec_int(reader);
    monomer_bond.clear();
    monomer_bond = read_bin_vec_int(reader);
    monomer_tree_position.clear();
    monomer_tree_position = read_bin_vec_int(reader);
    lattice_sites_nodes.clear();
    lattice_sites_nodes = read_bin_vec_int(reader);
    functionality_tree_node.clear();
    functionality_tree_node = read_bin_vec_int(reader);
    list_available_index.clear();
    list_available_index = read_bin_vec_int(reader);
    sum_f_1.clear();
    sum_f_1 = read_bin_vec_int(reader);
    sum_N_t.clear();
    sum_N_t = read_bin_vec_int(reader);
    monomer_belong_polymer.clear();
    monomer_belong_polymer = read_bin_vec_int(reader);
    vector_functionalities.clear();
    vector_functionalities = read_bin_vec_int(reader);
    vector_attempt_move_tree_with_functionality.clear();
    vector_attempt_move_tree_with_functionality = read_bin_vec_long_int(reader);
    vector_accept_move_tree_with_functionality.clear();
    vector_accept_move_tree_with_functionality = read_bin_vec_long_int(reader);
    
    sites_info.clear();
    sites_info = read_bin_vec_vec_int(reader);
    tree_info.clear();
    tree_info = read_bin_vec_vec_int(reader);
    old_position.clear();
    old_position = read_bin_vec_vec_int(reader);
    current_position.clear();
    current_position = read_bin_vec_vec_int(reader);
    pbc_shift.clear();
    pbc_shift = read_bin_vec_vec_int(reader);
    
    reader.read(reinterpret_cast<char*>(&time_phase_dyn), sizeof(double));
    reader.read(reinterpret_cast<char*>(&delta_E), sizeof(double));
    
    reader.read(reinterpret_cast<char*>(&iteration), sizeof(long int));
    reader.read(reinterpret_cast<char*>(&counter_step), sizeof(int));
    reader.read(reinterpret_cast<char*>(&accept_tree_node_dy), sizeof(long int));
    reader.read(reinterpret_cast<char*>(&accept_tree_node_dy), sizeof(long int));
    reader.read(reinterpret_cast<char*>(&accept_pair_move_dy), sizeof(long int));
    reader.read(reinterpret_cast<char*>(&accept_one_mon_dy), sizeof(long int));
    
    reader.close();
    
    
    
    correctPath = resumeFilePath;
    correctPath.erase(correctPath.end()-4,correctPath.end());
    correctPath += "RNGstate";
    ifstream temp_RNGreader(correctPath.c_str(), ios::in|ios::binary);
    if (!temp_reader.is_open()) {
        string warning_message = "Warning: No resume RNG binary buffer file available to read.\nWill try the backup.\n";
        cout<<warning_message<<endl;
        string backup_path = correctPath + "_backup";
        ifstream reader_backup(backup_path.c_str(), ios::in|ios::binary);
        if (!reader_backup.is_open()) {
            string error_message = "Error: Both resume RNG binary and its backup file are not available.\n";
            throw std::runtime_error(error_message);
        } else {
            correctPath = backup_path;
        }
        
    }
    
    
    ifstream readerRNG(correctPath.c_str(), ios::in|ios::binary);
    readerRNG>>RNG;
    readerRNG.close();
    
    
    
}



void copy_over_resume_backup(string resumeFilePath
                             ){
    string read_path = resumeFilePath;
    string write_path = resumeFilePath + "_backup";
    
    ofstream writer(write_path.c_str());
    ifstream reader(read_path.c_str());
    
    if (!reader.is_open()) {
        string error_message = "No resume binary buffer file available to read.\n";
        throw std::runtime_error(error_message);
    }
    writer<<reader.rdbuf();
    
    writer.close();
    reader.close();
}

void copy_over_RNGstate_backup(string resumeFilePath
                               ){
    string read_path = resumeFilePath;
    read_path.erase(read_path.end()-4,read_path.end());
    read_path+="RNGstate";
    string write_path = read_path + "_backup";
    
    ofstream writer(write_path.c_str());
    ifstream reader(read_path.c_str());
    
    if (!reader.is_open()) {
        string error_message = "No resume binary buffer file available to read.\n";
        throw std::runtime_error(error_message);
    }
    writer<<reader.rdbuf();
    
    writer.close();
    reader.close();
}




string check_if_file_exists(string filename){
    ifstream infile(filename);
    bool loop=infile.is_open();
    string entree = filename;
    while(!loop){
        cout<<"'"+ filename+". does not exist. Please try again:\n>> ";
        cin>>entree;
        ifstream infiletemp(entree.c_str());
        loop=infiletemp.is_open();
    }
    return entree;
}



string get_line_from_init_file(string init_file_path){
    ifstream read_init_file;
    read_init_file.open(init_file_path.c_str());
    string line;
    getline(read_init_file, line);
    return line;
}

int get_argc_from_command_line_params(string command_line_params){
    const char delim = ' ';
    std::vector<std::string> out;
    std::stringstream ss(command_line_params);
    std::string s;
    while (std::getline(ss, s, delim)) {
        out.push_back(s);
    }
    return out.size();
}

char** get_argv_from_command_line_params(string command_line_params, int argc){
    //    vector<char *> args;
    std::istringstream iss(command_line_params);
    
    
    //    int numColumns=32;//8*sizeof(char);
    int numColumns=128;//32*sizeof(char);
    //    cout<<numColumns<<endl;
    //    char **charPtrPtr;
    //    charPtrPtr = (char**) malloc(sizeof(char*) * argc);
    //    *charPtrPtr = (char*) malloc(sizeof(char) * numColumns);
    //    for(int i = 0; i < argc; i++){
    //       (charPtrPtr)[i] = ( (*charPtrPtr) + (i * numColumns) );
    //        charPtrPtr[i] = args[i];
    //    }
    
    char** charPtrPtr = new char*[argc];
    charPtrPtr[0]=new char[numColumns];
    charPtrPtr[0][0]='.';
    charPtrPtr[0][1]='/';
    charPtrPtr[0][2]='F';
    charPtrPtr[0][3]='C';
    charPtrPtr[0][4]='C';
    for (int j=5; j<numColumns; j++) {
        charPtrPtr[0][j]='\0';
    }
    
    string token;
    int i=1;
    while(iss >> token) {
        //        char *arg = new char[token.size() + 1];
        //        (charPtrPtr)[i] = ( (*charPtrPtr) + (i * numColumns) );
        //        copy(token.begin(), token.end(), charPtrPtr[i]);
        //        charPtrPtr[i][token.size()] = '\0';
        //        i++;
        //        copy(token.begin(), token.end(), arg);
        //        arg[token.size()] = '\0';
        //        args.push_back(arg);
        
        charPtrPtr[i] = new char[numColumns];
        copy(token.begin(), token.end(), charPtrPtr[i]);
        for (int j=token.size(); j<numColumns; j++) {
            charPtrPtr[i][j]='\0';
        }
        i++;
    }
    //    args.push_back(0);
    
    
    return charPtrPtr;
}

General_Parameters import_params_for_resume(General_Parameters &param)
{
    
    param.resume_file_path = check_if_file_exists(param.resume_file_path);
    string command_line_params = get_line_from_init_file(param.resume_file_path);
    
    int argc = get_argc_from_command_line_params (command_line_params);
    char** argv = get_argv_from_command_line_params (command_line_params, argc);
    
    return argument_parser(argc,argv);
}
