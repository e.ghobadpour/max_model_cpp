//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "output_functions.hpp"
#include <fstream> //For streaming data to the output file
#include <cmath> //For math calculation
#include <unordered_set>
#include <algorithm>
//save output coordinates as .xyz



vector<long int> logarithmicIntervals(double start, double end, int step) {
    vector<long int> result;
    if (start <= 0.0 || end <= 0.0 || step <= 0) {
        cout << "Invalid input. start, end, and steps must be positive numbers.\n";
        return result;
    }
    unordered_set<int> uniqueValues;
    double logA = log(start);
    double logB = log(end);
    
    double interval = (logB - logA) / (step - 1);
    //     cout<<start<<" "<<end<<" "<<logA<<" "<<logB<<endl;
    //     cout<<"interval "<<interval<<endl;
    for (int i = 0; i < step; i++) {
        double value = exp(logA + i * interval);
        int intValue = static_cast<int>(value);
        
        // Ensure uniqueness of the integer values
        while (uniqueValues.find(intValue) != uniqueValues.end()) {
            value += 1.0; // Increment the value to make it unique
            intValue = static_cast<int>(value);
        }
        
        uniqueValues.insert(intValue);
        result.push_back(intValue);
    }
    
    result.insert(result.begin(),0);//Inserting 0 to the time vector
    return result;
}

vector<long int> logspace(double start, double end, int step)
{
    
    vector<long int> logspace;
//    double logstart = log10(start);
    double logend = log10(end);
    
    for (int i = 0; i < step; i++)
    {
        double value = std::pow(10.0, (i / static_cast<double>(step - 1)) * logend) - 1.0;
        logspace.push_back(int(value));
    }
//    logspace.push_back(pow(10, i * (logend - logstart ) / (step - 1)));
    
    auto unique_end = unique(logspace.begin(), logspace.end());

    // Erase the duplicate elements from the vector
    logspace.erase(unique_end, logspace.end());
    logspace.push_back(int(end));
//    for ( int num=0; num<logspace.size();num++) {
//        cout << logspace[num] << " ";
//    }
//    cout << std::endl;
    return logspace;
}

vector<long int> MultiplyVectorByScalar(vector<double> &time, int sweep)
{
    vector<long int> time_integer;
    for(int i=0;i<time.size();++i)
        time[i] = time[i] * sweep;
    
    for (auto &item : time)
    {
        time_integer.push_back(lround(item));
    }
    //    for (int i=0;i<time_integer.size();i++)
    //    {
    //        cout<<time_integer[i]<<endl;
    //    }
    return time_integer;
}



//vector<double> initialise_time_measurment(int mc_sweep,
//                                       int ntim){
//    
//    int ntlg=10;
//    vector <double> time_steps(ntim+1,0);
//    double nlg;
//    nlg=ntlg*log10(mc_sweep);
//    time_steps[0]=0.0;
//    time_steps[ntim]=mc_sweep;
////    cout<<0<<" "<<time_steps[0]<<endl;
//    for (int i=1; i<ntim; i++){
//        time_steps[i]=pow(10,((nlg+i-ntim)/ntlg));
////        cout<<i<<" "<<time_steps[i]<<endl;
//    }
////    cout<<ntim<<" "<<time_steps[ntim]<<endl;
//    return time_steps;
//}
