//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "OneMonMove_functions.hpp"
#include "lattice_functions.hpp"
#include "model_functions.hpp"
#include <stdlib.h>
#include <random>

bool Check_Hamiltonian(string interaction,
                       double potenitial_branch,
                       double potential_interaction,
                       double tree_stiffness,
                       int equib_N_tree,
                       int &bond1,
                       int &bond2,
                       int random_monomer,
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       int new_position_random_monomer,
                       int lattice_pos_random_monomer,
                       vector<int> &N_tree_node_on_lattice_sites,
                       vector<int> &previous_mon,
                       vector<int> &functionality_tree_node,
                       vector<int> &monomer_tree_position,
                       double &delta_E,
                       vector<int> &monomer_belong_polymer,
                       mt19937_64 &RNG
                       )
{
    // in case functionality_tree_node[monomer_tree_position[random_monomer]] ==1
    // This is the accepted value H_id = 0.0 ;
    double H_id = 0.0 ;
    double H_int = 0.0;
    double H_cl = 0.0;
    int sum_f_new ;
    int sum_N_new ;
    int n;
//    int delta =0;
//    int delta_int =0;
//    int delta_N =0;
    int n_site =0;
    int n_site_old =0;
    double  Hamiltonian =0.0;
    int polymer;
    
    if (interaction == "Soft_Excluded")
    {
        polymer = monomer_belong_polymer[random_monomer];
        // in case functionality_tree_node[monomer_tree_position[random_monomer]] ==1
        // This is the accepted value sum_f_new = sum_f_1[polymer];
        sum_f_new = sum_f_1[polymer];
        sum_N_new = sum_N_t[polymer];
        if (bond1!=0 && bond2!=0)
        {
            n = monomer_tree_position[previous_mon[random_monomer]];
            n_site_old = N_tree_node_on_lattice_sites[lattice_pos_random_monomer];
            
            if(n_site_old != 1)
            {
                
//                H_int =k * [(N-1)(N-2) - (N-1)(N)] /2 = (-N+1)

                H_int = potential_interaction * (-1*n_site_old + 1);

            }
            
            if (functionality_tree_node[n]==2)
            {
                H_id =0.0;
            }
            else if (functionality_tree_node[n]!=2)
            {
                if (functionality_tree_node[n] != 1){
                    H_id = -potenitial_branch;
                }

            }
//
//            else
//            {
//                string ErrorMessage="There is an error in functionality of the tree node";
//                throw runtime_error(ErrorMessage);
//            }
            
//            (double(sum_f_1)/(sum_f_new))*
            
//            delta_N = pow((sum_N_new-equib_N_tree), 2) - pow((sum_N_new+1-equib_N_tree), 2);
//            H_cl = tree_stiffness *delta_N;
//            H_cl = k * [ (Nt-1-N0)^2 - (Nt-N0)^2 ] = k [2(N0-Nt)+1]
            H_cl = + tree_stiffness * (2*(equib_N_tree-sum_N_new)+1);
            
            Hamiltonian =  H_id+H_cl+H_int;
            return accept_move(RNG,Hamiltonian);
            
        }
        
        else if (bond1 == 0 && bond2 == 0)
        {
            n_site = N_tree_node_on_lattice_sites[new_position_random_monomer];
            if(n_site !=0)
            {
                
//                H_int = k*[ (N+1)(N)- N(N-1)]/2 = k*[2N-1];
                H_int = potential_interaction * (n_site);
            }
            if (functionality_tree_node[monomer_tree_position[random_monomer]] !=1)
            {
                H_id = potenitial_branch;
            }

            
            else if (functionality_tree_node[monomer_tree_position[random_monomer]] <0)
            {
                string ErrorMessage="There is an error in functionality of the tree node";
                throw runtime_error(ErrorMessage);
            }
//            (double(sum_f_1)/(sum_f_new))*
//            delta_N = pow((sum_N_new+1-equib_N_tree), 2) - pow((sum_N_new-equib_N_tree), 2);
//            H_cl = tree_stiffness *delta_N;
//            or
//            H_cl = k * [(Nt +1 - N0)^2 - (Nt-N0)^2 ] = k [ 2(Nt-N0)+1]
            H_cl = tree_stiffness *(2*(sum_N_new-equib_N_tree)+1);
            
            Hamiltonian = H_id+H_int+H_cl;
            return accept_move(RNG,Hamiltonian);
            
        }
        else
        {
            return true;
        }
        
    }
    else
    {
        return true;
    }
    
}

bool Check_Interaction_Condition_Reptation(string interaction,
                                           int ncell,
                                           int max_branch,
                                           int lattice_pos_random_monomer,
                                           int new_position_random_monomer,
                                           int random_monomer,
                                           int &bond1,
                                           int &bond2,
                                           vector<int> &icl,
                                           vector<int> &previous_mon,
                                           vector<int> &next_mon,
                                           vector<int> &monomer_lattice_pos,
                                           vector <int> & monomer_tree_position,
                                           vector<int> &functionality_tree_node
                                           )
{
    
    if (interaction == "Soft_Excluded")
    {
        if (icl[new_position_random_monomer]>= 0 && icl[new_position_random_monomer]<ncell)
        {
            if (bond1 !=0 && bond2 !=0)
            {
                if (monomer_tree_position[previous_mon[random_monomer]] == monomer_tree_position[next_mon[random_monomer]])
                {
                    return true;
                    
                }
                else
                {
//                    cout<<"tree nodes have a problem"<<endl;
                    return false;
                }
            }
            else if (bond1 ==0 && bond2 ==0)
            {
                if( functionality_tree_node[monomer_tree_position[random_monomer]]<max_branch)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (monomer_lattice_pos[previous_mon[random_monomer]] == new_position_random_monomer || monomer_lattice_pos[next_mon[random_monomer]] == new_position_random_monomer)
                    
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
        
        
    }
    
    
    else if (interaction == "SAT")
    {
        if (icl[new_position_random_monomer] == 0 &&  functionality_tree_node[monomer_tree_position[random_monomer]]<max_branch)
        {
            return true;
        }
        else if(icl[new_position_random_monomer] > 0 && icl[new_position_random_monomer]<ncell )
        {
            if(bond1 !=0 && bond2 !=0)
            {
                if (monomer_tree_position[previous_mon[random_monomer]] == monomer_tree_position[next_mon[random_monomer]])
                {
                    return true;
                }
                else
                {
                    return false;
                    
                }
            }
            
            else
            {
                if (monomer_lattice_pos[previous_mon[random_monomer]] == new_position_random_monomer || monomer_lattice_pos[next_mon[random_monomer]] == new_position_random_monomer)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }
    
    
    else if (interaction == "Ideal")
    {
        if (icl[new_position_random_monomer] >= 0 && icl[new_position_random_monomer]<ncell)
        {
            if (bond1 !=0 && bond2 !=0)
            {
//                if (monomer_tree_position[previous_mon[random_monomer]] == monomer_tree_position[next_mon[random_monomer]])
//                {
                    return true;
//                }
//                else
//                {
                    return false;
//                }
            }
            else if (bond1 ==0 && bond2 ==0 )
            {
                if( functionality_tree_node[monomer_tree_position[random_monomer]]<max_branch)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (monomer_lattice_pos[previous_mon[random_monomer]] == new_position_random_monomer || monomer_lattice_pos[next_mon[random_monomer]] == new_position_random_monomer)
                    
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        string ErrorMessage="Input interaction is not defined. Available interactions: SAT or Ideal or Soft_Excluded";
        throw runtime_error(ErrorMessage);
    }
}


void Update_Monomer_Move(int random_monomer,
                         int lattice_pos_random_monomer,
                         int random_neighbor,
                         int new_position_random_monomer,
                         int &bond1,
                         int &bond2,
                         int &bond1_new,
                         int &bond2_new,
                         vector<int> &icl,
                         vector<int> &monomer_lattice_pos,
                         vector<int> &previous_mon,
                         vector< vector <int> > &forward_bonds,
                         vector< vector <int> > &backward_bonds,
                         vector<int> &monomer_bond,
                         vector<vector<int>> &sites_info,
                         vector <int> &opposite_primitive_vectors
                         )
{
    icl[lattice_pos_random_monomer] -= 1;
    icl[new_position_random_monomer] += 1;
    monomer_lattice_pos[random_monomer] = new_position_random_monomer;
    bond2_new = forward_bonds[bond2][random_neighbor];
    monomer_bond[random_monomer] = forward_bonds[bond2][random_neighbor];
    bond1_new = backward_bonds[bond1][random_neighbor];
    
    if (bond2_new==-1 ||bond1_new==-1)
    {
        string ErrorMessage="One monomer move, there is an error in bonds, bond1_new";
        throw runtime_error(ErrorMessage);
        
    }
    if(bond1_new!=0 && bond2_new!=0 &&  bond2_new!= opposite_primitive_vectors[bond1_new])
    {

        string ErrorMessage="One monomer move, there is an error in bonds, bond1_new";
        throw runtime_error(ErrorMessage);
    }
    monomer_bond[previous_mon[random_monomer]] = backward_bonds[bond1][random_neighbor];
    sites_info[new_position_random_monomer].push_back(random_monomer);
    sites_info[lattice_pos_random_monomer].erase(remove(sites_info[lattice_pos_random_monomer].begin(), sites_info[lattice_pos_random_monomer].end(), random_monomer), sites_info[lattice_pos_random_monomer].end());
}



void Update_Functionality(int new_position_random_monomer,
                          int lattice_pos_random_monomer,
                          int &bond_prev,
                          int &bond_mon,
                          int &bond_prev_new,
                          int &bond_mon_new,
                          vector<int> &functionality
                          )
{
    if (bond_prev ==0 && bond_mon==0 )
    {
        functionality[new_position_random_monomer]+= 1;
        functionality[lattice_pos_random_monomer]+= 1;
        
    }
    if (bond_prev_new ==0 && bond_mon_new ==0)
    {
        functionality[new_position_random_monomer]-= 1;
        functionality[lattice_pos_random_monomer]-= 1;
        
    }
    
}

void update_tree_nodes(int random_monomer,
                       int lattice_pos_random_monomer,
                       int new_position_random_monomer,                       
                       vector<int> &sum_f_1,
                       vector<int> &sum_N_t,
                       vector<int> &N_tree_node_on_lattice_sites,
                       vector<int> &monomer_lattice_pos,
                       vector<int> &previous_mon,
                       vector<int> &next_mon,
                       vector<int> &monomer_bond,
                       vector <int> & monomer_tree_position,
                       vector <int>   &lattice_sites_nodes,
                       vector<vector<int>> &tree_node,
                       vector<int> &functionality_tree_node,
                       vector<int> &list_available_index,
                       vector<int> &monomer_belong_polymer
                       )
{
    int t;
    int n;
    int polymer;
    polymer = monomer_belong_polymer[random_monomer];
    t=monomer_tree_position[random_monomer];
    tree_node[t].erase(remove(tree_node[t].begin(), tree_node[t].end(), random_monomer), tree_node[t].end());
    
    if(new_position_random_monomer != monomer_lattice_pos[previous_mon[random_monomer]] &&
       new_position_random_monomer == monomer_lattice_pos[next_mon[random_monomer]] )
    {
        
        n = monomer_tree_position[next_mon[random_monomer]];
        tree_node[n].push_back(random_monomer);
        monomer_tree_position[random_monomer] = n;
        
    }
    
    if(new_position_random_monomer == monomer_lattice_pos[previous_mon[random_monomer]] &&
       new_position_random_monomer != monomer_lattice_pos[next_mon[random_monomer]] )
    {
        
        n = monomer_tree_position[previous_mon[random_monomer]];
        tree_node[n].push_back(random_monomer);
        monomer_tree_position[random_monomer] = n;
        
    }
    
    if(new_position_random_monomer != monomer_lattice_pos[previous_mon[random_monomer]] &&
       new_position_random_monomer != monomer_lattice_pos[next_mon[random_monomer]] )
    {
        if (list_available_index.size() == 0)
        {
            n =  tree_node.size();
            vector<int> empty;
            tree_node.push_back(empty);
            
            functionality_tree_node.resize(n+1,0);
            lattice_sites_nodes.resize(n+1,0);
        }
        else
        {
            n=list_available_index.back();
            list_available_index.pop_back();
        }
        
        tree_node[n].push_back(random_monomer);
        monomer_tree_position[random_monomer] = n;
        lattice_sites_nodes[n] = new_position_random_monomer;
        if(functionality_tree_node[t]!=1)
        {
            sum_f_1[polymer] +=1;
        }
        
        functionality_tree_node[t] +=1;
        functionality_tree_node[n] +=1;
        N_tree_node_on_lattice_sites[new_position_random_monomer]+=1;
        sum_N_t[polymer] +=1;
        
    }
    
    if(new_position_random_monomer == monomer_lattice_pos[previous_mon[random_monomer]] &&
       new_position_random_monomer == monomer_lattice_pos[next_mon[random_monomer]] )
    {
        //        cout<<monomer_bond[random_monomer]<<" "<<monomer_bond[previous_mon[random_monomer]]<<endl;
        n = monomer_tree_position[previous_mon[random_monomer]];
        tree_node[n].push_back(random_monomer);
        monomer_tree_position[random_monomer] = n;
        lattice_sites_nodes[t] = -1;
        functionality_tree_node[t] -= 1;
        functionality_tree_node[n] -= 1;
        if(functionality_tree_node[n]!=1)
        {
            sum_f_1[polymer] -=1;
        }
        N_tree_node_on_lattice_sites[lattice_pos_random_monomer]-=1;
        sum_N_t[polymer] -=1;
        list_available_index.push_back(t);
    }

    
}




void  One_Monomer_Move (string interaction,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_lattice_pos,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector <int> &opposite_primitive_vectors,
                        vector<vector<int>> &sites_info,
                        vector<int> &monomer_tree_position,
                        vector <int>   &lattice_sites_nodes,
                        vector<vector<int>> &tree_node,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        int max_branch,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG
                        )
{
    int random_monomer;
    int lattice_pos_random_monomer;
    int bond1;
    int bond2;
    int bond1_new ;
    int bond2_new ;
    int random_direction;
    int random_neighbor;
    int new_position_random_monomer;
    bond1_new =-1;
    bond2_new =-1;
    // diffusion
    random_monomer = (RNG() % nt0)+1;
    lattice_pos_random_monomer = monomer_lattice_pos[random_monomer];
    bond1 = monomer_bond[previous_mon[random_monomer]];
    bond2 = monomer_bond[random_monomer];
//    cout<<bond1<<" "<<bond2<<endl;
    random_direction = (RNG() % 12) + 1;
    if(bond1 == -1||bond2 == -1)
    {
        
        cout<<"bond1 "<<bond1<<" bond2 "<<bond2<<" random_monomer "<<random_monomer<<endl;
        string ErrorMessage="In one monomer move, one or two bonds are not correctly defined";
        throw runtime_error(ErrorMessage);
    }
    random_neighbor = move_table[bond1][random_direction][bond2];
        
        
        if (random_neighbor > 0)
        {
            new_position_random_monomer = list_nn[lattice_pos_random_monomer][random_neighbor];
            
            if ( new_position_random_monomer >= 0)
            {
                
                if (Check_Interaction_Condition_Reptation(interaction,
                                                          ncell,
                                                          max_branch,
                                                          lattice_pos_random_monomer,
                                                          new_position_random_monomer,
                                                          random_monomer,
                                                          bond1,
                                                          bond2,
                                                          icl,
                                                          previous_mon,
                                                          next_mon,
                                                          monomer_lattice_pos,
                                                          monomer_tree_position,
                                                          functionality_tree_node)
                    )
                {
                    if (Check_Hamiltonian(interaction,
                                          potenitial_branch,
                                          potential_interaction,
                                          tree_stiffness,
                                          equib_N_tree,
                                          bond1,
                                          bond2,
                                          random_monomer,
                                          sum_f_1,
                                          sum_N_t,
                                          new_position_random_monomer,
                                          lattice_pos_random_monomer,
                                          N_tree_node_on_lattice_sites,
                                          previous_mon,
                                          functionality_tree_node,
                                          monomer_tree_position,
                                          delta_E,
                                          monomer_belong_polymer,
                                          RNG
                                          )
                        )
                    {

                        update_tree_nodes(random_monomer,
                                          lattice_pos_random_monomer,
                                          new_position_random_monomer,
                                          sum_f_1,
                                          sum_N_t,
                                          N_tree_node_on_lattice_sites,
                                          monomer_lattice_pos,
                                          previous_mon,
                                          next_mon,
                                          monomer_bond,
                                          monomer_tree_position,
                                          lattice_sites_nodes,
                                          tree_node,
                                          functionality_tree_node,
                                          list_available_index,
                                          monomer_belong_polymer
                                          );
                       
                        Update_Monomer_Move(random_monomer,
                                            lattice_pos_random_monomer,
                                            random_neighbor,
                                            new_position_random_monomer,
                                            bond1,
                                            bond2,
                                            bond1_new,
                                            bond2_new,
                                            icl,
                                            monomer_lattice_pos,
                                            previous_mon,
                                            forward_bonds,
                                            backward_bonds,
                                            monomer_bond,
                                            sites_info,
                                            opposite_primitive_vectors
                                            );
                        
                        acceptance_rate +=1;
                    }
                    
                }
            }
        }
        

}

bool Check_Interaction_Condition_Reptation_repton(string interaction,
                                           int ncell,
                                           int max_branch,
                                           int lattice_pos_random_monomer,
                                           int new_position_random_monomer,
                                           int random_monomer,
                                           int &bond1,
                                           int &bond2,
                                           vector<int> &icl,
                                           vector<int> &previous_mon,
                                           vector<int> &next_mon,
                                           vector<int> &monomer_lattice_pos,
                                           vector <int> & monomer_tree_position,
                                           vector<int> &functionality_tree_node,
                                           double repton_mixing,
                                           int nt0,
                                           mt19937_64 &RNG
                                           )
{
    double random_repton;
    std::uniform_real_distribution<double> dis(0.0, 1.0);
    random_repton  = dis(RNG);
    if (interaction == "Soft_Excluded")
    {
        if (icl[new_position_random_monomer]>= 0 && icl[new_position_random_monomer]<ncell)
        {
            if (bond1 !=0 && bond2 !=0)
            {
                if (random_repton> (repton_mixing))
                {
                if (monomer_tree_position[previous_mon[random_monomer]] == monomer_tree_position[next_mon[random_monomer]])
                {
                    return true;
                    
                }
                else
                {
//                    cout<<"tree nodes have a problem"<<endl;
                    return false;
                }
                }
                else
                {
                    return false;
                }
                    
            }
            else if (bond1 ==0 && bond2 ==0)
            {
                if (random_repton> (repton_mixing))
                {
                if( functionality_tree_node[monomer_tree_position[random_monomer]]<max_branch)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                }
                else
                {
                    return false;
                }
                
            }
            else
            {
//                random_repton  = (RNG()% nt0);
                
                
                if (random_repton<= (repton_mixing))
                {
//                    cout<<random_repton<<endl;
//                    cout<<random_repton<<" "<<repton_mixing *nt0<<endl;
                if (monomer_lattice_pos[previous_mon[random_monomer]] == new_position_random_monomer || monomer_lattice_pos[next_mon[random_monomer]] == new_position_random_monomer)
                    
                {
                    return true;
                }
                else
                {
                    return false;
                }
                }
                else
                {
                    return false;
                }
                    
            }
        }
        else
        {
            return false;
        }
        
        
    }
    

    
    else
    {
        string ErrorMessage="Input interaction is not defined. Available interactions: SAT or Ideal or Soft_Excluded";
        throw runtime_error(ErrorMessage);
    }
}

void  One_Monomer_Move_repton (string interaction,
                        double repton_mixing,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_lattice_pos,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector <int> &opposite_primitive_vectors,
                        vector<vector<int>> &sites_info,
                        vector<int> &monomer_tree_position,
                        vector <int>   &lattice_sites_nodes,
                        vector<vector<int>> &tree_node,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        int max_branch,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG
                        )
{
    int random_monomer;
    int lattice_pos_random_monomer;
    int bond1;
    int bond2;
    int bond1_new ;
    int bond2_new ;
    int random_direction;
    int random_neighbor;
    double random_repton ;
    int new_position_random_monomer;
    bond1_new =-1;
    bond2_new =-1;
    // diffusion
    random_monomer = (RNG() % nt0)+1;
    lattice_pos_random_monomer = monomer_lattice_pos[random_monomer];
    bond1 = monomer_bond[previous_mon[random_monomer]];
    bond2 = monomer_bond[random_monomer];
//    cout<<bond1<<" "<<bond2<<endl;
    random_direction = (RNG() % 12) + 1;
    if(bond1 == -1||bond2 == -1)
    {
        
        cout<<"bond1 "<<bond1<<" bond2 "<<bond2<<" random_monomer "<<random_monomer<<endl;
        string ErrorMessage="In one monomer move, one or two bonds are not correctly defined";
        throw runtime_error(ErrorMessage);
    }
    random_neighbor = move_table[bond1][random_direction][bond2];
        
        
        if (random_neighbor > 0)
        {
            
            
            new_position_random_monomer = list_nn[lattice_pos_random_monomer][random_neighbor];
            
            
            if ( new_position_random_monomer >= 0)
            {
                
                if (Check_Interaction_Condition_Reptation_repton(interaction,
                                                          ncell,
                                                          max_branch,
                                                          lattice_pos_random_monomer,
                                                          new_position_random_monomer,
                                                          random_monomer,
                                                          bond1,
                                                          bond2,
                                                          icl,
                                                          previous_mon,
                                                          next_mon,
                                                          monomer_lattice_pos,
                                                          monomer_tree_position,
                                                          functionality_tree_node,
                                                          repton_mixing,
                                                          nt0,
                                                          RNG)
                    )
                {
                    if (Check_Hamiltonian(interaction,
                                          potenitial_branch,
                                          potential_interaction,
                                          tree_stiffness,
                                          equib_N_tree,
                                          bond1,
                                          bond2,
                                          random_monomer,
                                          sum_f_1,
                                          sum_N_t,
                                          new_position_random_monomer,
                                          lattice_pos_random_monomer,
                                          N_tree_node_on_lattice_sites,
                                          previous_mon,
                                          functionality_tree_node,
                                          monomer_tree_position,
                                          delta_E,
                                          monomer_belong_polymer,
                                          RNG
                                          )
                        )
                    {

                        update_tree_nodes(random_monomer,
                                          lattice_pos_random_monomer,
                                          new_position_random_monomer,
                                          sum_f_1,
                                          sum_N_t,
                                          N_tree_node_on_lattice_sites,
                                          monomer_lattice_pos,
                                          previous_mon,
                                          next_mon,
                                          monomer_bond,
                                          monomer_tree_position,
                                          lattice_sites_nodes,
                                          tree_node,
                                          functionality_tree_node,
                                          list_available_index,
                                          monomer_belong_polymer
                                          );
                       
                        Update_Monomer_Move(random_monomer,
                                            lattice_pos_random_monomer,
                                            random_neighbor,
                                            new_position_random_monomer,
                                            bond1,
                                            bond2,
                                            bond1_new,
                                            bond2_new,
                                            icl,
                                            monomer_lattice_pos,
                                            previous_mon,
                                            forward_bonds,
                                            backward_bonds,
                                            monomer_bond,
                                            sites_info,
                                            opposite_primitive_vectors
                                            );
                        
                        acceptance_rate +=1;
                    }
                    
                }
            }
        }
        

}



