//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "model_functions.hpp"
#include "TwoMon_functions.hpp"
#include "lattice_functions.hpp"


bool Check_branch_probability_f_2(int zero_bond,
                                  int random_tree_node,
                                  double branch_prob,
                                  vector<int> &functionality_tree_node,
                                  mt19937_64 &RNG
                                  )
{
    
    int condition = 0;
    
    if (abs(branch_prob-1.0) >0.0000001)
    {
        if (zero_bond ==1 )
        {
            if (functionality_tree_node[random_tree_node]==2)
            {
                //                cout<<zero_bond<<endl;
                
                //                cout<<zero_bond<<" "<<branch_prob<<" "<<functionality_tree_node[random_tree_node]<<endl;
                if (((double)RNG() / (double)RNG.max()) > branch_prob)
                {
                    condition =-1;
                    //                    cout<<true;
                    
                }
            }
        }
        
    }
    if ( condition == -1)
    {
        return false;
    }
    else
    {
        return true;
    }
    
}



void  Two_Monomer_Move (string interaction,
                        double potenitial_branch,
                        double potential_interaction,
                        double tree_stiffness,
                        int equib_N_tree,
                        double branch_prob,
                        int max_branch,
                        int chain_size,
                        int mesh_size,
                        int ncell,
                        int nt0,
                        vector<int> &sum_f_1,
                        vector<int> &sum_N_t,
                        vector<int> &previous_mon,
                        vector<int> &next_mon,
                        vector<int> &monomer_lattice_pos,
                        vector <int>   &lattice_sites_nodes,
                        vector<int> &icl,
                        vector<int> &N_tree_node_on_lattice_sites,
                        vector<int> &monomer_bond,
                        vector <vector<vector<int> > > &bending_move_table,
                        vector <vector<int> > &list_nn,
                        vector< vector <int> > &forward_bonds,
                        vector< vector <int> > &backward_bonds,
                        vector<vector<int>> &sites_info,
                        vector<int> &paired_vectors,
                        vector <int> &monomer_tree_position,
                        vector <vector <int> >  &tree_node_info,
                        vector<int> &functionality_tree_node,
                        vector<int> &list_available_index,
                        long int &acceptance_rate,
                        double &delta_E,
                        vector<int> &monomer_belong_polymer,
                        mt19937_64 &RNG
                        )
{
    
    int random_tree_node;
    int random_lattice_site;
    int monomer_1;
    int monomer_2;
    int random_direction;
    int bond1_1,bond1_2,bond2_1,bond2_2;
    int random_neighbor_1,random_neighbor_2;
    int new_position_random_monomers;
    int zero_bond;
    int bond_direction;
    int first_monomer;
    
    //    first_monomer = 0;
    monomer_1 = (RNG() % nt0)+1;
    random_tree_node = monomer_tree_position[monomer_1];
    if (random_tree_node == -1)
    {
        cout<<monomer_1<<" "<<random_tree_node<<endl;
        string ErrorMessage="In paired move, random tree node is equel to -1";
        throw runtime_error(ErrorMessage);
    }
    random_lattice_site = lattice_sites_nodes[random_tree_node];
    
    if (tree_node_info[random_tree_node].size() >= 2)
    {
        bond1_1 = monomer_bond[previous_mon[monomer_1]];
        bond1_2 = monomer_bond[monomer_1];
        if (bond1_1 ==-1 || bond1_2 ==-1)
        {
            cout<<monomer_1<<" "<<bond1_1<<" "<<bond1_2<<endl;
            string ErrorMessage="One or two monomer bonds of the first monomer in pair move are -1";
            throw runtime_error(ErrorMessage);
            
        }
        if (bond1_1 != 0 || bond1_2 != 0)//The monomer should be at the edge
        {
            
            
            monomer_2 = Find_Paired_Monomer (chain_size,
                                             monomer_1,
                                             random_tree_node,
                                             previous_mon,
                                             next_mon,
                                             monomer_lattice_pos,
                                             monomer_bond,
                                             monomer_tree_position,
                                             tree_node_info,
                                             RNG
                                             );
            
            
            if (monomer_2 > 0)
            {
                
                bond2_1 = monomer_bond[previous_mon[monomer_2]];
                bond2_2 = monomer_bond[monomer_2];
                random_direction = (RNG() % 12)+1;
                if (bond2_1 ==-1 || bond2_2 ==-1)
                {
                    cout<<monomer_2<<" "<<bond2_1<<" "<<bond2_2<<endl;
                    string ErrorMessage="One or two monomer bonds of the second monomer in pair move are -1";
                    throw runtime_error(ErrorMessage);
                    exit(1);
                    
                }
                random_neighbor_1 = bending_move_table[bond1_1][random_direction][bond1_2];
                random_neighbor_2 = bending_move_table[bond2_1][random_direction][bond2_2];
                
                zero_bond = 0;
                bond_direction = 0;
                
                if (random_neighbor_1 != -1 && random_neighbor_2 !=-1 && random_neighbor_1==random_neighbor_2)
                {
                    new_position_random_monomers = list_nn[random_lattice_site][random_neighbor_1];
                    
                    if ( new_position_random_monomers >= 0)
                    {
                        
                        for (int num : {bond1_1, bond1_2, bond2_1, bond2_2})
                        {
                            
                            if (num==0)
                            {
                                zero_bond+=1;
                            }
                        }
                        
                        
                        first_monomer = 0;
                        if (zero_bond==1)
                        {
                            
                            bond_direction =  Find_Bond_Direction(bond1_1,
                                                                  bond1_2,
                                                                  bond2_1,
                                                                  bond2_2,
                                                                  paired_vectors
                                                                  );
                            if (bond1_1 !=0 && bond1_2 !=0)
                            {
                                first_monomer = monomer_1;
                            }
                            else
                            {
                                first_monomer = monomer_2;
                            }
                        }
                        
                        if (Check_Accept_Condition_Two_Mons_Move(interaction,
                                                                 potenitial_branch,
                                                                 potential_interaction,
                                                                 tree_stiffness,
                                                                 equib_N_tree,
                                                                 max_branch,
                                                                 zero_bond,
                                                                 monomer_1,
                                                                 monomer_2,
                                                                 new_position_random_monomers,
                                                                 random_lattice_site,
                                                                 random_tree_node,
                                                                 random_neighbor_1,
                                                                 bond_direction,
                                                                 first_monomer,
                                                                 sum_f_1,
                                                                 sum_N_t,
                                                                 icl,
                                                                 N_tree_node_on_lattice_sites,
                                                                 previous_mon,
                                                                 next_mon,
                                                                 monomer_bond,
                                                                 monomer_lattice_pos,
                                                                 paired_vectors,
                                                                 monomer_tree_position,
                                                                 lattice_sites_nodes,
                                                                 functionality_tree_node,
                                                                 tree_node_info,
                                                                 delta_E,
                                                                 monomer_belong_polymer,
                                                                 RNG
                                                                 )
                            )
                        {
                            Update_Sites_Two_Monomer_Move(monomer_1,
                                                         monomer_2,
                                                         random_lattice_site,
                                                         new_position_random_monomers,
                                                         zero_bond,
                                                         previous_mon,
                                                         next_mon,
                                                         monomer_lattice_pos,
                                                         icl,
                                                         sites_info);
                            Update_Tree_Nodes_Two_Monomer_Move(monomer_1,
                                                              monomer_2,
                                                              random_lattice_site,
                                                              new_position_random_monomers,
                                                              zero_bond,
                                                              random_tree_node,
                                                              sum_f_1,
                                                              sum_N_t,
                                                              icl,
                                                              N_tree_node_on_lattice_sites,
                                                              previous_mon,
                                                              next_mon,
                                                              monomer_lattice_pos,
                                                              monomer_bond,
                                                              lattice_sites_nodes,
                                                              monomer_tree_position,
                                                              functionality_tree_node,
                                                              list_available_index,
                                                              tree_node_info,
                                                              monomer_belong_polymer
                                                              );
                            monomer_bond[previous_mon[monomer_1]] = backward_bonds[bond1_1][random_neighbor_1];
                            monomer_bond[monomer_1] = forward_bonds[bond1_2][random_neighbor_1];
                            
                            monomer_bond[previous_mon[monomer_2]] = backward_bonds[bond2_1][random_neighbor_1];
                            monomer_bond[monomer_2] = forward_bonds[bond2_2][random_neighbor_1];
                            monomer_lattice_pos[monomer_1] = new_position_random_monomers;
                            monomer_lattice_pos[monomer_2] = new_position_random_monomers;
                            acceptance_rate +=1;
                        }// if (Check_Accept_Condition_Two_Mons_Move
                        
                        for (int i=0; i<tree_node_info.size();i++ )
                        {
                            if (lattice_sites_nodes[i]==-1 && tree_node_info[i].size()!=0 )
                            {
                                cout<<"Two monomer move "<<endl;
                                for (int j =0; j<tree_node_info[i].size();j++)
                                {
                                    cout<<tree_node_info[i][j]<<" ";
                                }
                                cout<<endl;
                                string ErrorMessage="lattice_site node =-1 and tree_node-info!=0";
                                throw runtime_error(ErrorMessage);
                                //            exit(1);
                                
                            }
                        }
                    }// if ( new_position_random_monomers >= 0)
                    
                    
                    
                }//if (random_neighbor_1 != -1 && random_neighbor_2 !=-1 && random_neighbor_1==random_neighbor_2)
                
                
                
            }
            
        }
        
        
    }
    
    
}



int Find_Paired_Monomer (int chain_size,
                         int monomer_1,
                         int random_tree_node,
                         vector<int> &previous_mon,
                         vector<int> &next_mon,
                         vector<int> &monomer_lattice_pos,
                         vector<int> &monomer_bond,
                         vector <int> &monomer_tree_position,
                         vector <vector <int> >  &tree_node_info,
                         mt19937_64 &RNG
                         )
{
    int monomer_2;
    int prev_monomer_1 ,prev_monomer_2;
    int next_monomer_1 ,next_monomer_2;
    int paired_monomer;
    int rand_mon;
    vector<int> paired_mons;
    monomer_2 = -1;
    prev_monomer_1 = previous_mon[monomer_1];
    next_monomer_1 = next_mon[monomer_1];
    for (int i = 0; i<tree_node_info[random_tree_node].size(); i++)
    {
        paired_monomer = tree_node_info[random_tree_node][i];
        if(paired_monomer != monomer_1)
        {
            if (monomer_bond[paired_monomer] != 0 || monomer_bond[previous_mon[paired_monomer]] != 0)
            {
                if(abs(monomer_1-paired_monomer)%(chain_size-2) != 1)
                {
                    prev_monomer_2 = previous_mon[paired_monomer];
                    next_monomer_2 = next_mon[paired_monomer];
                    
                    if((monomer_tree_position[prev_monomer_2] == monomer_tree_position[next_monomer_1] && monomer_tree_position[prev_monomer_2] != monomer_tree_position[monomer_1] ) || (monomer_tree_position[next_monomer_2] == monomer_tree_position[prev_monomer_1] && monomer_tree_position[next_monomer_2] != monomer_tree_position[monomer_1] ))
                    {
                        paired_mons.push_back(paired_monomer);
                    }
                }
            }
        }
    }
    if (paired_mons.size()>= 2)
    {
        rand_mon = RNG() % paired_mons.size();
        monomer_2 = paired_mons[rand_mon];
    }
    else if (paired_mons.size() == 1 )
    {
        
        monomer_2 = paired_mons[0];
    }
    return monomer_2;
}



int Find_Bond_Direction( int bond1_1,
                        int bond1_2,
                        int bond2_1,
                        int bond2_2,
                        vector<int> &paired_vectors)

{
    int bond_direction=0;
    
    if (bond1_1==paired_vectors[bond2_2])
    {
        
        if (bond1_2!=0)
        {
            bond_direction=bond1_2;
        }
        else
        {
            bond_direction=bond2_1;
        }
    }
    if (bond1_2==paired_vectors[bond2_1])
    {
        
        if (bond1_1!=0)
        {
            bond_direction=bond1_1;
        }
        else
        {
            bond_direction=bond2_2;
        }
    }
    return bond_direction;
}


bool Check_Accept_Condition_Two_Mons_Move(string interaction,
                                          double potenitial_branch,
                                          double potential_interaction,
                                          double tree_stiffness,
                                          int equib_N_tree,
                                          int max_branch,
                                          int zero_bond,
                                          int monomer_1,
                                          int monomer_2,
                                          int new_position_random_monomers,
                                          int random_lattice_site,
                                          int random_tree_node,
                                          int random_neighbor_1,
                                          int bond_direction,
                                          int monomer_first,
                                          vector<int> sum_f_1,
                                          vector<int> sum_N_t,
                                          vector<int> &icl,
                                          vector<int> &N_tree_node_on_lattice_sites,
                                          vector<int> &previous_mon,
                                          vector<int> &next_mon,
                                          vector<int> &monomer_bond,
                                          vector<int> &monomer_lattice_pos,
                                          vector<int> &paired_vectors,
                                          vector <int> &monomer_tree_position,
                                          vector <int>   &lattice_sites_nodes,
                                          vector<int> &functionality_tree_node,
                                          vector <vector <int> >  &tree_node_info,
                                          double &delta_E,
                                          vector<int> &monomer_belong_polymer,
                                          mt19937_64 &RNG
                                          )
{
    int sum_f_new=0,sum_f_old=0,sum_N_total=0;
    int n_site_new=0;
    int n_site_old=0;
    int delta_initial=0;
    int delta_final=0;
    int delta_cl =0;
    int polymer;
    double H_int=0;
    double H_cl=0;
    double Hamiltonian = 0.0;
    double E_tot =0;
    //    int H_id =0;
    if (interaction == "Soft_Excluded")
    {
        polymer = monomer_belong_polymer[monomer_1];
//        cout<<"monomer_first "<<monomer_1<<endl;
//        cout<<endl;
        sum_f_old = sum_f_1[polymer];
        sum_f_new = sum_f_1[polymer];
        sum_N_total = sum_N_t[polymer];
        if (zero_bond == 1)
        {
            if (random_neighbor_1 == bond_direction || random_neighbor_1 == paired_vectors[bond_direction])
            {
                if (lattice_sites_nodes[monomer_tree_position[previous_mon[monomer_first]]] != lattice_sites_nodes[monomer_tree_position[next_mon[monomer_first]]])
                {
                    if((new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_first]]) && functionality_tree_node[monomer_tree_position[next_mon[monomer_first]]]<max_branch)
                    {
                        if (functionality_tree_node[random_tree_node]==2)
                        {
                            sum_f_new++;
                        }
                        if (functionality_tree_node[monomer_tree_position[next_mon[monomer_first]]]==1)
                        {
                            sum_f_new--;
                        }
                        //                        if (functionality_tree_node[monomer_tree_position[next_mon[monomer_first]]]==1 &&functionality_tree_node[random_tree_node]==2)
                        //                        {
                        //                            sum_f_new = sum_f_old;
                        //                        }
                        //                        (double(sum_f_1)/(sum_f_new))*
                        
                        
                        
                        Hamiltonian =  potenitial_branch*(sum_f_new-sum_f_old);

                        return accept_move(RNG,Hamiltonian);
//                        if (Hamiltonian <=0)
//                        {
//                            //                            delta_E +=potenitial_branch*(sum_f_new-sum_f_1);
//                            return true;
//                        }
//                        else
//                        {
//
//                            if (((double)RNG() / (double)RNG.max()) < exp(-Hamiltonian))
//                            {
//
//                                //                                delta_E +=potenitial_branch*(sum_f_new-sum_f_1);
//                                return true;
//                            }
//                            else
//                            {
//                                return false;
//                            }
//
//                        }
                        
                    }
                    else if((new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_first]]) && functionality_tree_node[monomer_tree_position[previous_mon[monomer_first]]]<max_branch)
                    {
                        if (functionality_tree_node[random_tree_node]==2)
                        {
                            sum_f_new ++;
                            
                        }
                        if (functionality_tree_node[monomer_tree_position[previous_mon[monomer_first]]]==1)
                        {
                            sum_f_new --;
                            
                        }
                        //                        if (functionality_tree_node[monomer_tree_position[previous_mon[monomer_first]]]==1 &&functionality_tree_node[random_tree_node]==2)
                        //                        {
                        //                            sum_f_new = sum_f_old;
                        //
                        //                        }
                        
                        //                        (double(sum_f_1)/(sum_f_new))*
                        
                        Hamiltonian =  potenitial_branch*(sum_f_new-sum_f_old);
                        if (Hamiltonian <=0)
                        {
                            //                            delta_E += potenitial_branch*(sum_f_new-sum_f_1);
                            return true;
                        }
                        else
                        {
                            
                            if (((double)RNG() / (double)RNG.max()) < exp(-Hamiltonian))
                            {
                                //                                delta_E += potenitial_branch*(sum_f_new-sum_f_1);
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                            
                        }
                    }
                    else
                    {
                        return false;
                    }
                    
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else if (zero_bond == 2)
        {
            if ((new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]]) || (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]]))
            {
                return true;
            }
            else
            {
                n_site_new = N_tree_node_on_lattice_sites[new_position_random_monomers];
                n_site_old = N_tree_node_on_lattice_sites[random_lattice_site];
                
                //                delta_initial=0;
                if (n_site_new != 0)
                {
                    delta_initial = (n_site_new-1)*(n_site_new-1); //+(n_site_old-1)*(n_site_old-1);
                    delta_final = n_site_new*n_site_new; //+ (n_site_old-1)*(n_site_old-1);
                    H_int = potential_interaction *(delta_final - delta_initial);
                }
                
                
                
                //                delta_cl = pow((sum_N_total+1-equib_N_tree), 2) - pow((sum_N_total-equib_N_tree), 2);
                //                H_cl = tree_stiffness *delta_cl;
                //                H_cl = k *[ (Nt+1-N0)^2 - (Nt-N0)^2] = k * [2(Nt-N0)+1]
                
                H_cl = tree_stiffness *(2*(sum_N_total-equib_N_tree)+1);
                
                Hamiltonian =  H_int +H_cl;
              
                return accept_move(RNG,Hamiltonian);
//                if(Hamiltonian<=0)
//                {
//                    //                    cout<<"H_int2 "<<H_int<<endl;
//                    //                    delta_E += H_int +H_cl;
//                    return true;
//                }
//                else
//                {
//                    if (((double)RNG() / (double)RNG.max()) < exp(-Hamiltonian))
//                    {
//                        //                        cout<<"H_int2 "<<H_int<<endl;
//                        //                        delta_E += H_int +H_cl;
//                        return true;
//                    }
//                    else
//                    {
//
//                        return false;
//                    }
//                }
                //                }
            }
            
        }
        else if(zero_bond == 0)
        {
            
            if (tree_node_info[random_tree_node].size() == 2)
            {
                if (lattice_sites_nodes[monomer_tree_position[previous_mon[monomer_1]]] != lattice_sites_nodes[monomer_tree_position[next_mon[monomer_1]]])
                {
                    if((new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]]) || (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]]))
                    {
                        n_site_new = N_tree_node_on_lattice_sites[new_position_random_monomers];
                        n_site_old = N_tree_node_on_lattice_sites[random_lattice_site];
                        
                        delta_final =0;
                        if (n_site_old-1 != 0)
                            
                        {
                            delta_final = (n_site_old-2)*(n_site_old-2);
                        }
                        delta_initial = (n_site_old-1)*(n_site_old-1);
                        H_int = potential_interaction *(delta_final - delta_initial);
                        
                        //                        delta_cl = pow((sum_N_total-1-equib_N_tree), 2) - pow((sum_N_total-equib_N_tree), 2);
                        //                        H_cl = tree_stiffness *delta_cl;
                        //                        H_cl = k * [(Nt-1-N0)^2-(Nt-N0)^2] = k * [-2(Nt-N0)+1]
                        H_cl = tree_stiffness *(-2*(sum_N_total-equib_N_tree)+1);
                        Hamiltonian = H_int + H_cl;
//                        if (H_cl>=0)
//                        {
//                        cout<<sum_N_t[polymer]<<" ";
//                        }

                        return accept_move(RNG,Hamiltonian);
//                        if (Hamiltonian <=0)
//                        {
//
//                            return true;
//                        }
//                        else
//                        {
//
//                            if (((double)RNG() / (double)RNG.max()) < exp(-Hamiltonian))
//                            {
//
//                                return true;
//                            }
//                            else
//                            {
//
//                                return false;
//                            }
//                        }
                        
                    }
                    else
                    {
                        return false;
                        
                        
                    }
                    
                    
                    
                    
                }
                //                    }
                //            }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            
        }
        else
        {
            string ErrorMessage="Paired Monomers has a problem of zero bonds";
            throw runtime_error(ErrorMessage);
        }
        
    }
    
    else if (interaction == "SAT")
    {
        if(zero_bond == 0)
        {
            
            if (tree_node_info[random_tree_node].size() == 2)
            {
                if ((icl[new_position_random_monomers] == 0) || (new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]]) || (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]]))
                {
                    if (lattice_sites_nodes[monomer_tree_position[previous_mon[monomer_1]]] != lattice_sites_nodes[monomer_tree_position[next_mon[monomer_1]]])
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        
        else if (zero_bond == 2)
        {
            if ((icl[new_position_random_monomers] == 0) || (new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]]) || (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]]))
            {
                return true;
            }
            else
            {
                return false;
            }
            
            
        }
        else if (zero_bond == 1)
        {
            //            cout<<monomer_first<<endl;
            
            if (random_neighbor_1 == bond_direction || random_neighbor_1 == paired_vectors[bond_direction])
            {
                //                if((new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_first]]) && (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_first]]))
                
                //                if (monomer_bond[monomer_first]!= paired_vectors[previous_mon[monomer_first]])
                if (lattice_sites_nodes[monomer_tree_position[previous_mon[monomer_first]]] != lattice_sites_nodes[monomer_tree_position[next_mon[monomer_first]]])
                {
                    
                    if((new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_first]]) && functionality_tree_node[monomer_tree_position[next_mon[monomer_first]]]<max_branch)
                    {
                        return true;
                    }
                    else if((new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_first]]) && functionality_tree_node[monomer_tree_position[previous_mon[monomer_first]]]<max_branch)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    //                    return true;
                    
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            string ErrorMessage="Paired Monomers has a problem of zero bonds";
            throw runtime_error(ErrorMessage);
        }
        
        
        
    }
    else if (interaction == "Ideal")
    {
        if(zero_bond == 0)
        {
            if (tree_node_info[random_tree_node].size() == 2)
            {
                if ((icl[new_position_random_monomers] >=0) || (new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]]) || (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]]))
                {
                    if (lattice_sites_nodes[monomer_tree_position[previous_mon[monomer_1]]] != lattice_sites_nodes[monomer_tree_position[next_mon[monomer_1]]])
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            
        }
        if(zero_bond == 2)
        {
            if (icl[new_position_random_monomers] >=0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (zero_bond == 1)
        {
            if (random_neighbor_1 == bond_direction || random_neighbor_1 == paired_vectors[bond_direction])
            {
                if (lattice_sites_nodes[monomer_tree_position[previous_mon[monomer_first]]] != lattice_sites_nodes[monomer_tree_position[next_mon[monomer_first]]])
                {
                    
                    if((new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_first]]) && functionality_tree_node[monomer_tree_position[next_mon[monomer_first]]]<max_branch)
                    {
                        return true;
                    }
                    else if((new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_first]]) && functionality_tree_node[monomer_tree_position[previous_mon[monomer_first]]]<max_branch)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    
                    
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            string ErrorMessage="Paired Monomers has a problem of zero bonds";
            throw runtime_error(ErrorMessage);
        }
        
    }
    else
    {
        string ErrorMessage="Input interaction for two monomers move is not defined. Available interactions: SAT or Ideal or Soft_Excluded";
        throw runtime_error(ErrorMessage);
    }
}




void Update_Sites_Two_Monomer_Move(int monomer_1,
                                  int monomer_2,
                                  int random_lattice_site,
                                  int new_position_random_monomers,
                                  int zero_bond,
                                  vector<int> &previous_mon,
                                  vector<int> &next_mon,
                                  vector<int> &monomer_lattice_pos,
                                  vector<int> &icl,
                                  vector<vector<int>> &sites_info)
{
    sites_info[new_position_random_monomers].push_back(monomer_1);
    sites_info[new_position_random_monomers].push_back(monomer_2);
    sites_info[random_lattice_site].erase(remove(sites_info[random_lattice_site].begin(), sites_info[random_lattice_site].end(), monomer_1), sites_info[random_lattice_site].end());
    
    sites_info[random_lattice_site].erase(remove(sites_info[random_lattice_site].begin(), sites_info[random_lattice_site].end(), monomer_2), sites_info[random_lattice_site].end());
    
    icl[new_position_random_monomers] += 2;
    icl[random_lattice_site] -= 2;
    //    if (zero_bond == 0)
    //    {
    //        //        if (icl[new_position_random_monomers]==0)
    //        if(new_position_random_monomers != monomer_lattice_pos[previous_mon[monomer_1]] &&
    //           new_position_random_monomers != monomer_lattice_pos[next_mon[monomer_1]] )
    //        {
    //            functionality[new_position_random_monomers] += 2;
    //            functionality[random_lattice_site] -= 2;
    //        }
    //        else
    //        {
    //            functionality[new_position_random_monomers] += 0;
    //            functionality[random_lattice_site] -= 2;
    //        }
    //    }
    //    if (zero_bond == 1)
    //    {
    //        functionality[new_position_random_monomers] += 1;
    //        functionality[random_lattice_site] -= 1;
    //    }
    //    if (zero_bond == 2)
    //    {
    //        if(new_position_random_monomers != monomer_lattice_pos[previous_mon[monomer_1]] &&
    //           new_position_random_monomers != monomer_lattice_pos[next_mon[monomer_1]] )
    //        {
    //            functionality[new_position_random_monomers]+= 2;
    //            functionality[random_lattice_site]-= 0;
    //        }
    //    }
    
}

void Update_Tree_Nodes_Two_Monomer_Move(int monomer_1,
                                       int monomer_2,
                                       int random_lattice_site,
                                       int new_position_random_monomers,
                                       int zero_bond,
                                       int random_tree_node,
                                       vector<int> &sum_f_1,
                                       vector<int> &sum_N_t,
                                       vector<int> &icl,
                                       vector<int> &N_tree_node_on_lattice_sites,
                                       vector<int> &previous_mon,
                                       vector<int> &next_mon,
                                       vector<int> &monomer_lattice_pos,
                                       vector<int> &monomer_bond,
                                       vector <int>   &lattice_sites_nodes,
                                       vector <int> &monomer_tree_position,
                                       vector<int> &functionality_tree_node,
                                       vector<int> &list_available_index,
                                       vector <vector <int> >  &tree_node_info,
                                       vector<int> &monomer_belong_polymer
                                       )
{
    int n = -1;
    int order_monomer;
    int polymer;
    
    polymer = monomer_belong_polymer[monomer_1];
    
    tree_node_info[random_tree_node].erase(remove(tree_node_info[random_tree_node].begin(), tree_node_info[random_tree_node].end(), monomer_1), tree_node_info[random_tree_node].end());
    
    tree_node_info[random_tree_node].erase(remove(tree_node_info[random_tree_node].begin(), tree_node_info[random_tree_node].end(), monomer_2), tree_node_info[random_tree_node].end());
    
    
    if (zero_bond ==0 )
    {
        if(new_position_random_monomers != monomer_lattice_pos[previous_mon[monomer_1]] &&
           new_position_random_monomers != monomer_lattice_pos[next_mon[monomer_1]] )
            
        {
            tree_node_info[random_tree_node].push_back(monomer_1);
            tree_node_info[random_tree_node].push_back(monomer_2);
            lattice_sites_nodes[random_tree_node] = new_position_random_monomers;
            N_tree_node_on_lattice_sites[new_position_random_monomers] += 1;
            N_tree_node_on_lattice_sites[random_lattice_site] -= 1;
        }
        if (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]] ||
            new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]] )
        {
            if (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]])
            {
                n = monomer_tree_position[previous_mon[monomer_1]];
            }
            if (new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]])
            {
                n = monomer_tree_position[next_mon[monomer_1]];
            }
            
            tree_node_info[n].push_back(monomer_1);
            tree_node_info[n].push_back(monomer_2);
            monomer_tree_position[monomer_1] = n;
            monomer_tree_position[monomer_2] = n;
            functionality_tree_node[random_tree_node] -= 2;
            list_available_index.push_back(random_tree_node);
            lattice_sites_nodes[random_tree_node] = -1;
            N_tree_node_on_lattice_sites[random_lattice_site] -= 1;
            sum_N_t[polymer] -=1;
            //            cout<<"tree_node_info[n].size() "<<tree_node_info[random_tree_node].size()<<endl;
        }
        
    }
    
    
    if (zero_bond ==2 )
        
    {
        if(new_position_random_monomers != monomer_lattice_pos[previous_mon[monomer_1]] &&
           new_position_random_monomers != monomer_lattice_pos[next_mon[monomer_1]] )
        {
            if (list_available_index.size() == 0)
            {
                n =  tree_node_info.size();
                vector<int> empty;
                tree_node_info.push_back(empty);
                
                functionality_tree_node.resize(n+1,0);
                lattice_sites_nodes.resize(n+1,0);
            }
            else
            {
                n=list_available_index.back();
                list_available_index.pop_back();
            }
            
            tree_node_info[n].push_back(monomer_1);
            tree_node_info[n].push_back(monomer_2);
            monomer_tree_position[monomer_1] = n;
            monomer_tree_position[monomer_2] = n;
            lattice_sites_nodes[n] = new_position_random_monomers;
            functionality_tree_node[n] +=2;
            N_tree_node_on_lattice_sites[new_position_random_monomers] += 1;
            sum_N_t[polymer] +=1;
            //            cout<<n<<endl;
            
        }
        
        if (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]] ||
            new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]] )
        {
            if (new_position_random_monomers == monomer_lattice_pos[previous_mon[monomer_1]])
            {
                n = monomer_tree_position[previous_mon[monomer_1]];
            }
            if (new_position_random_monomers == monomer_lattice_pos[next_mon[monomer_1]])
            {
                n = monomer_tree_position[next_mon[monomer_1]];
            }
            
            tree_node_info[n].push_back(monomer_1);
            tree_node_info[n].push_back(monomer_2);
            monomer_tree_position[monomer_1] = n;
            monomer_tree_position[monomer_2] = n;
            
        }
    }
    
    if (zero_bond == 1 )
    {
        if (monomer_bond[monomer_1]!= 0 && monomer_bond[previous_mon[monomer_1]]!= 0)
        {
            order_monomer = monomer_1;
        }
        else
        {
            order_monomer = monomer_2;
        }
        //        cout<<order_monomer<<endl;
        if (new_position_random_monomers == monomer_lattice_pos[previous_mon[order_monomer]])
        {
            
            n = monomer_tree_position[previous_mon[order_monomer]];
            
        }
        if (new_position_random_monomers == monomer_lattice_pos[next_mon[order_monomer]])
        {
            
            n = monomer_tree_position[next_mon[order_monomer]];
        }
        if (n==-1)
        {
            cout<<"there is a problem in n"<<endl;
        }
        tree_node_info[n].push_back(monomer_1);
        tree_node_info[n].push_back(monomer_2);
        monomer_tree_position[monomer_1] = n;
        monomer_tree_position[monomer_2] = n;
        if (functionality_tree_node[n] ==1)
        {
            sum_f_1[polymer] -=1;
        }
        if (functionality_tree_node[random_tree_node] ==2)
        {
            sum_f_1[polymer] +=1;
        }
        
        functionality_tree_node[n] +=1;
        functionality_tree_node[random_tree_node] -=1;
        
        
    }
    
}

