//
//  geometry.cpp
//  FCC_Max
//
//  Created by ghobadel on 28/07/2021.
//
#include "cxxopts.hpp"
#include "arg_parse_functions.hpp"
#include "parameters.hpp"
#include <cmath>
void check_params(General_Parameters &param);

General_Parameters argument_parser(int argc, char** argv)

{
    General_Parameters param;
    cxxopts::Options options("CGBD", "Coarse-Grain Bacterial DNA Simulator: Monte Carlo simulation of double-folded elastic lattice polymer model.");
    
    options.add_options()
    
//all the parameteres are in the unit of lattice bond (2), I need to update them into biology language 
    ("c,constant", "Param lattice_const", cxxopts::value<int>()->default_value("2"), "int")
    ("x,lx", "Param lx", cxxopts::value<int>()->default_value("10"), "int")
    ("y,ly", "Param ly", cxxopts::value<int>()->default_value("10"), "int")
    ("z,lz", "Param lz", cxxopts::value<int>()->default_value("10"), "int")
    ("b,boundary", "Param boundary, pbc or fixed", cxxopts::value<std::string>()->default_value("pbc"), "string")
    ("f,functionality", "Param Max functionality tree nodes,between 2 and 12", cxxopts::value<int>()->default_value("100"), "int")
    ("n,ncell", "Param max. number of monomer per cell", cxxopts::value<int>()->default_value("10000"), "int")
    //**************************************************
    ("i,interactionType", "Param interaction for diffusion,SAT or Ideal or Soft_Excluded,for now just use Soft_Excluded", cxxopts::value<std::string>()->default_value("Soft_Excluded"), "string")
    ("u,branchpotenitial", "Param chemical potenitial branching " , cxxopts::value<double>()->default_value("0.0"), "double")
    ("k,interactionpotenitial", "Param chemical potenitial interaction " , cxxopts::value<double>()->default_value("0.0"), "double")
    ("v,stiffnesslengthtree", "Param chemical potenitial tree node length " , cxxopts::value<double>()->default_value("0.0"), "double")
    ("q,densitytreenode", "Param density tree nodes " , cxxopts::value<double>()->default_value("3.0"), "double")
    ("w,MCmove", "Param MC move,one_mon, tree_node, or mixed", cxxopts::value<std::string>()->default_value("mixed"), "string")
    ("j,mixing", "Param fraction mixing the moves", cxxopts::value<double>()->default_value("0.5"), "double")
    ("d,MixingRepton", "Param fraction mixing reptation in the monomer move", cxxopts::value<double>()->default_value("1.0"), "double")
    //***************************************************
    ("m,monomer", "Param number of monomers", cxxopts::value<int>()->default_value("64"), "int")
    ("p,polymer", "Param number of polymers", cxxopts::value<int>()->default_value("1"), "int")
    ("g,timgrow", "Param MC sweep grow phase", cxxopts::value<int>()->default_value("1000"), "int")
    ("e,timequib", "Param MC sweep equilibrium phase", cxxopts::value<int>()->default_value("1000000"), "int")
    ("t,timdyn", "Param MC sweep dynamics phase", cxxopts::value<long int>()->default_value("1000000"), "long int")
    ("R,resumefreq", "number of times to save for resuming per simulation", cxxopts::value<int>()->default_value("0"), "int")
    ("s,sample", "number of independent samples", cxxopts::value<int>()->default_value("1600"), "int")
    ("a,seed", "seed number" , cxxopts::value<int>()->default_value("1"), "int")
    ("projectName", "Project name" , cxxopts::value<std::string>()->default_value("NewProject"), "string")
    ("resume", "path to interupted project init file", cxxopts::value<std::string>()->default_value("path/to/init/file"))
    ("h,help", "Print usage")

    ;
    auto result = options.parse(argc, argv);
    
    if (result.count("help"))
    {
        std::cout << options.help() << std::endl;
        exit(0);
    }

    param.LATTICE_CONSTANT = result["constant"].as<int>();
    param.Lx = result["lx"].as<int>();
    param.Ly = result["ly"].as<int>();
    param.Lz = result["lz"].as<int>();
    param.boundary = result["boundary"].as<std::string>();
    param.FUNCTIONALITY  = result["functionality"].as<int>();
    param.N_CELL  = result["ncell"].as<int>();
    param.N_MONOMERS = result["monomer"].as<int>();
    param.N_POLYMERS = result["polymer"].as<int>();
    
    
    param.interaction_type = result["interactionType"].as<std::string>();
//    param.interaction_reptation = result["interactionReptation"].as<std::string>();
    param.BRANCH_POTENITIAL = result["branchpotenitial"].as<double>();
    param.INTERACTION_POTENITIAL = result["interactionpotenitial"].as<double>();
    param.DENSITY_T = result["densitytreenode"].as<double>();
    param.TREE_LENGTH_STIFFNESS = result["stiffnesslengthtree"].as<double>();
    param.move = result["MCmove"].as<std::string>();


    param.MC_GROW = result["timgrow"].as<int>();
    param.MC_EQUIB = result["timequib"].as<int>();
    param.MC_DYN = result["timdyn"].as<long int>();
    param.nsamp = result["sample"].as<int>();
    param.seed_sample = result["seed"].as<int>();
    param.project_name = result["projectName"].as<std::string>();
    param.resume_save_freq = result["resumefreq"].as<int>();
    param.mixing =  result["mixing"].as<double>();
    param.mixingRepton =  result["MixingRepton"].as<double>();
    if (result["resume"].as<std::string>()=="path/to/init/file") {
        param.resume = false;
    }
    else
    {
        param.resume = true;
    }
    param.resume_file_path = result["resume"].as<std::string>();
//    cout<<"param.resume_file_path "<<param.resume_file_path<<endl;
    
    check_params(param);
    
    return param;
}


void check_params(General_Parameters &param)
{
    param.TOTAL_N_MON = param.N_MONOMERS * param.N_POLYMERS;
    param.N_MESH_POINTS = 4*param.Lx* param.Ly* param.Lz;
//    param.end_time_output = log10(param.MC_DYN);
    param.end_time_output_eq = param.MC_EQUIB;
    param.end_time_output = param.MC_DYN;
    param.N_T = int(param.N_MONOMERS/param.DENSITY_T) +1;
    cout<<"param.N_T "<<param.N_T<<endl;
}
