#!/bin/bash

# you have to change time limit and memory
# SBATCH -t 48:00:00
# SBATCH --mem=5G
# SBATCH --mail-user=<your email>
# SBATCH --mail-type=END,FAIL
#SBATCH --job-name=test
# SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --partition=s92,c61,c82

module purge
# source /usr/share/lmod/init/bash
# ml use /applis/PSMN/debian11/E5/modules/all/
# ml load GCC/11.2.0

echo ${ARG1};
# echo ${ARG2};
# echo ${ARG3};
# echo ${ARG4};


./CGBD -a ${ARG1} -s 1  -f 3 -w "mixed" -p 12 -m 64 -k 30.0 -x 4 -y 4 -z 4 -g 1000 -e 10000000 -t 10000000 -R 0;

# ARG1, ARG2, ARG3 ... are command line arguments.
# you have to use --export to tell sbatch to use command line arguments.
# you can use nested loop to enumerate all the possible combinations
# and run one job per combination:
# for a in ...; do for b in ...; do for c in ...; do sbatch -J <name of the job> -o <name of the output file> -e <name of the error file> --export=ARG1=${a},ARG2=${b},ARG3=${c} sbatch_multiprams_template.sh; sleep 1; done; done; done
