TARGET=CGBD
CXXFLAGS=-std=c++11 -O3

OSNAME := $(shell uname -s)

Boost_LIB_Mac_Dir = /usr/local/lib
Boost_LIB_Lin_Dir = /usr/lib/x86_64-linux-gnu

MacLIBS= -lboost_filesystem
LinLIBS= -lboost_filesystem -lboost_system

ifeq ($(OSNAME),Darwin)
  Boost_LIB_Dir = $(Boost_LIB_Mac_Dir)
  LIBS = $(MacLIBS)
  CXX = g++  
else
  Boost_LIB_Dir = $(Boost_LIB_Mac_Dir)
  LIBS = $(LinLIBS)
  CXX = clang++
endif

BINDIR=bin
SRCDIR=source
INCDIR=include
OBJDIR=objects

INCDIRS=-I$(INCDIR) #-I$(OpenMM_INSTALL_DIR)/include 
LIB_DIR=-L$(Boost_LIB_Dir) #-L$(OpenMM_INSTALL_DIR)/lib -L$(Boost_LIB_Dir) 

SRCFILES=$(wildcard $(SRCDIR)/*.cpp) 
OBJFILES=$(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SRCFILES)) 
DEPFILES=$(wildcard $(INCDIR)/*.hpp) #$(wildcard $(INCDIR)/*.h)


INC=-I$(DEPFILES)

all: $(BINDIR)/$(TARGET)
	@echo Finished!

$(BINDIR)/$(TARGET): $(OBJFILES)
	@$(CXX) $(CXXFLAGS) $(LIB_DIR) $(LIBS) $? -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@mkdir -p $(@D)	
	@$(CXX) $(CXXFLAGS) $(INCDIRS) -c $< -o $@

SUBDIR_ROOTS := objects 
DIRS := . $(shell find $(SUBDIR_ROOTS) -type d)
GARBAGE_PATTERNS := *.o
GARBAGE := $(foreach DIR,$(DIRS),$(addprefix $(DIR)/,$(GARBAGE_PATTERNS)))

clean: 
	rm -rf $(GARBAGE)
	rm bin/$(TARGET)
