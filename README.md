# Coarse-Grain Bacterial DNA Simulator (CGBD)

Welcome to the **Coarse-Grain Bacterial DNA Simulator (CGBD)**, a Monte Carlo simulator for the double-folded elastic lattice polymer model of bacterial DNA. This README provides an overview of how to get started, what you need, and the key information about the project.

### Publications and Background

The following publications describe the model used by CGBD:

1. [Monte Carlo simulation of a lattice model for the dynamics of randomly branching double-folded ring polymers, Phys. Rev. E 104, 014501 (2021)](https://doi.org/10.1103/PhysRevE.104.014501)
2. My Ph.D. thesis
3. **In preparation**: Multi-scale polymer model of bacterial chromosomes

### Citing CGBD

If you use CGBD for your research, please cite:

1. [Monte Carlo simulation of a lattice model for the dynamics of randomly branching double-folded ring polymers, Phys. Rev. E 104, 014501 (2021)](https://doi.org/10.1103/PhysRevE.104.014501)
2. **In preparation**

### System Requirements

- Linux or MacOS
- [Boost Library](https://www.boost.org/)
- [Clang++ (version 9 or newer)](https://clang.llvm.org/)

### Cloning the Repository

To get started, first clone the repository:

```sh
git clone https://github.com/yourusername/CGBD.git
cd CGBD
```

### Installation

To install CGBD, follow these steps:

1. Ensure that you have the Boost Library and Clang++ installed. You can find installation instructions for Boost [here](https://www.boost.org/doc/libs/1_81_0/more/getting_started/index.html) and for Clang++ [here](https://clang.llvm.org/get_started.html).
2. Use the provided Makefile to compile the code:

```sh
make clean && make
```

### Usage

To view all available options and their default values, run:

```sh
./CGBD -h
```

### Example Command

To simulate two self-avoiding trees (p=2) with 64 monomers in a cubic box of dimensions (10, 10, 10) with periodic boundary conditions:

```sh
./CGBD -b "fixed" -p 2 -m 64 -f 3 -x 10 -y 10 -z 10 -g 1000 -e 1000000 -t 1 -R 0
```

**Explanation of Important Parameters:**

- `-f 2`: Simulates a linear double-folded chain, while `f > 2` will simulate a tree.
- `-k`: Controls interaction potential. `k == 0.0` for ideal chains (no interaction), while `k == 30 k_BT` gives a hard excluded volume effect. For softer excluded volume interactions, use values like `k=0.25, 0.5, 1.0`.
- `-u`: Chemical potential to control branch length. `u = 0.0` corresponds to highly branching chains, while `u >> 0.0` gives more linear chains, depending on the chain length.
- `-e`: Number of Monte Carlo (MC) sweeps required for equilibrium. Longer chains require longer equilibration.
- `-t 1`: For purely statistical results without trajectory data, use `t=1` to save only the final snapshot in `eq_trajectory.xyz`.
- `-R 0`: Number of times to save simulation state for possible resumption. Useful for long simulations.

### Code Structure Overview

The **main.cpp** file is structured as follows:

1. **Lattice Initialization**

   - Creates the lattice and initializes necessary tables.

2. **Sample Initialization**

   - Initializes vectors for each sample. Specify the number of independent samples with `-s`. Use `-s 1` for single samples or a higher number (e.g., `-s 100`) for multiple independent samples.

3. **Growth Phase**

   - Begins by inserting a trimer into the system and adding monomers incrementally until the desired length is achieved.

4. **Equilibrium Phase**

   - Achieves equilibrium using two types of moves: monomer moves and tree node moves.

5. **Dynamics Phase**

   - After equilibrium, simulates monomer trajectories to analyze properties like Mean-Square Displacement (MSD) and relaxation time.

---
Thank you for using **CGBD**! If you encounter issues or have questions, please reach out to our community.

Happy simulating!
